#+TITLE: About the Delta Jointer that used to be in the farmhouse basement (WSITEM-200967), and its blades

:PROPERTIES:
:serial_number: J9708
:model_number: 37-190
:manual_url: https://www.mikestools.com/download/Delta-Jointer-Owners-manuals/37-190%20Type%203.pdf
:END:

** Adjusting

There are probably lots of ways to adjust that I have not figured out.

The height of the right end, opposite of 'depth of cut', can be easily adjusted
by loosening a lever-handled screw on the front-right and then pulling a lever
under the right side of the table up or down.

Turning the knob under the left end of the table clockwise tightens it,
*lowering* the left end of the table relative to the blade
(think of it as tightening the knob pulls the table down).
It can be very hard to turn.
This might be because I need to loosen something else.  Hmm.

#+BEGIN_QUOTE
For most jointing operations the outfeed table must be
exactly level with the knives at their highest point of
revolution.

...

To move the outfeed table up or down, loosen lockscrew (A) Fig. 38,
and turn hand knob (B). When the outfeed table is exactly level with
the knives at their highest point of revolution, tighten lockscrew (A).
#+END_QUOTE
(the manual, p16)

The lockscrew is the small metal lever-headed screw on the back.
The knob is the big knob under the left side of the table.


** Blades

Blades are 5/8" wide, just over 6" long ('6+1/8", online products will say),
and somewhat less than 1/8" thick.  They have NO HOLE!Sho

A 5/16" open-ended wrench is perfect to loosen (turn away from you) the bolts
that hold the blade in.

The small set screws can be turned to lift the blade up (un-stick the hex bolts first);
clockwise to lift, counterclockwise to lower.

A guy on the internet suggested to get the right and left blades to the exact same height
and then set the blades so that it matches that same exact height at its middle point.
In practice, I have the blade go a smidgen higher, so that passing a straightedge
(I used the fat edge of a small metal speed square) across the gap just barely
knocks into the blade and moves it slightly.
