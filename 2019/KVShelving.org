#+TITLE: KV 255 Series Shelving

'KV 255 Series Standard' seems to refer to these metal strips
with 1/2"-spaced slots that I like.

** Sources

*** Amazon

On 2019-03-06 I ordered some zinc KV shelving strips
for about 1$/foot from these guys: https://www.amazon.com/gp/product/B001003C9U

- The strips are 0.61in or 15.8mm wide and 0.16in or 4.2mm thick,
  or a little under 5/8-wide and a little over 5/32 deep
  (to simplify when routing, maybe I should shoot for 6/32"=3/16" depth? -- [2019-11-15])
- Each 6-inch segment has a screw hole 1/2in from the bottom
  and a staple hole 1/2in from the top.
- Additionally, the top section has a screw hole 1in from the top
  and the bottom section has a staple hole 1in from the bottom.

Notes and diagrams in breen grid notebook WSITEM-3402, p35.

The [[https://smile.amazon.com/gp/product/B075G3Q9XG][clips I bought from Amazon]] are slightly wider than the strips
at 16.1mm wide, or just under 5/8in.
So when installing, the surace of the strip needs to either be flush
with or protrude from the wall, or the slot should be wide enough to accomodate the clip.

*** ReStore

I have found them there.
And sometimes clips.
