# Why isn't my WeMos D1 mini programming???

Errors uploading
(```warning: espcomm_send_command: wrong direction/command: 0x01 0x03, expected 0x01 0x04```),
or upload gets to 100% but program doesn't run.

- Bricked WeMos?
- Bad USB cable?
- Borked USB port?
- RPi too bogged down with other things?

### Fresh everything!

- Reboot
- different USB port
- new USB cable
- fresh WeMos
- upload speed 115200
- writing 'Blink' program:

It works!

### Switch to possibly b0rked WeMos, write 'Blkink' program

Works!

### Try writing more complex program

Works!

### Plug it into the assembly with the MOSFETs and stuff

Seems to continue working!

### Can I talk to it via serial?

Hmm, maybe not.  'help' don't do nothing.

### Should'be checked that before plugging it into the board, maybe

Yeah.

### Okay unplug everything and back in

Still not seeing anything in Serial,
even though the program's obviously running
(built-in LED is pulsing).

Oh no serial port keeps switching.

### Hold down reset

Oh hey, when it comes back I can see my serial output,
and commands work!

### Make some changes, recompile

Oh no it doesn't work again.

warning: espcomm_send_command: didn't receive command response

### Try ploading 'Blink'

1. fail to upload
2. upload to 100%, then it doesn't run
3. fail to upload
4. fail to upload

### Unplug from USB, plug back in, try uploading 'Blink' again

```
warning: espcomm_send_command: didn't receive command response
warning: espcomm_send_command(FLASH_DOWNLOAD_DATA) failed
warning: espcomm_send_command: wrong direction/command: 0x01 0x03, expected 0x01 0x04
error: espcomm_upload_mem failed
An error occurred while uploading the sketch
```

Grr.

### Try lower upload speed, 230400

Same error.


### Try programming from bottom-left USB port

Blink successfully programmed!

### Repeat

Blink works!

### Try writing ArduinoLightController

I've added a couple of 500ms delays at the beginning of setup().

Programmed okay!  Light pulsing.  Serial I/O works as expected.

### Make some changes, program it again

Oh no, some trouble, but

### Hold down reset until IDE's nearly ready to program, then let up

Programs okay.


### Hypothesis

My program messes with some of the I/O pins
which screws up the USB port so that programming it doesn't work.
Adding a delay at the beginning gives me a chance to program it
before screwing up voltage levels on I/O pins.


### Plug back into assembly

TODO: Try programming with old cable, diff USB ports
