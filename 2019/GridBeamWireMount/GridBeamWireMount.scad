// v1.0 - 2019-07-04

mm = 1;
inch = 25.4 * mm;
extrusion_height = 0.3 * mm;
approximate_sixteenth = extrusion_height * round(1/16 * inch / extrusion_height);
$fn = 100;

echo(str(
  "approximate_sixteenth = ", approximate_sixteenth / extrusion_height, " layers, or ",
  approximate_sixteenth / inch, " inches"
));

unit_width = (1+1/2) * inch;
base_thickness = 2 * approximate_sixteenth;
base_margin = 1/16 * inch;
base_width = unit_width - base_margin*2;

bolt_hole_diameter = 3/8 * inch; // Big enough for my joint connector nuts!
jc_head_diameter = 0.65 * inch; // Diameter of the heads of my joint connector bolts
bolt_countersink_diameter = jc_head_diameter + 1/16 * inch;
bolt_countersink_depth = 1 * approximate_sixteenth;

cage_margin = 3/16 * inch;
cage_bar_thickness = 2 * approximate_sixteenth;
cage_basement_depth = bolt_countersink_depth;
cage_basement_margin = 0.5 * mm;
cage_height = cage_bar_thickness + cage_bar_thickness + cage_basement_depth;
cage_basement_width = cage_bar_thickness + cage_basement_margin*2;

// Distance in X or Y to from center of part to cage post centers:
post_distance = unit_width/2 - cage_margin - cage_bar_thickness/2;

base_color = [0.5,0.5,0.6];
rail_color = [0.6,0.6,0.7];
pocket_color = [0.4,0.4,0.5];

module each_corner() {
	for( ang = [0 : 90 : 270] ) rotate( [0, 0, ang] ) children();
}

module base() {
	translate([0, 0, base_thickness/2]) difference() {
		color(base_color) cube([base_width, base_width, base_thickness], true);
		color(pocket_color) {
			cylinder(d = bolt_hole_diameter, h=1 * inch, center=true);
			translate([0, 0, base_thickness/2])
				cylinder(d = bolt_countersink_diameter, h=bolt_countersink_depth*2, center=true);
			each_corner() translate([post_distance, post_distance, base_thickness/2])
				cube([cage_basement_width, cage_basement_width, cage_basement_depth*2], true);
		}
	}
}

module rails() {
	color(rail_color) each_corner() {
		translate([post_distance, 0, cage_bar_thickness/2])
			cube([cage_bar_thickness, unit_width - cage_margin*2, cage_bar_thickness], true);
		translate([post_distance, -post_distance, cage_height/2])
			cube([cage_bar_thickness, cage_bar_thickness, cage_height], true);
	}
}

module assembled() {
	base();
	translate([0, 0, cage_height + base_thickness - cage_basement_depth]) rotate([180,0,0])
		rails();
}

module apart() {
	translate([-unit_width/2, 0, 0]) base();
	translate([+unit_width/2, 0, 0]) rails();
}

//assembled();
apart();