#+TITLE: Headphone Jack / Isolator panel

I designed it with sub-panels that can be switched out
because I have to over-engineer everything.
Also because I don't 100% know what will need
isolation or not, so wanted to keep things flexible
rather than soldering everything together.

This same design could also be used for 2.1mm DC power jacks,
whose barrel and flange are about the same size as those on the
3.5mm headphone jacks that I have.

** Electronic Equipment

I bought (back on [[https://smile.amazon.com/gp/your-account/order-details/ref=dp_iou_view_this_order?ie=UTF8&orderID=113-4886605-0469813][2021-04-04]]):

- These 3.5mm (1/8") headphone audio jacks: [[https://smile.amazon.com/gp/product/B089222S84]]
- These isolation transformers: [[https://smile.amazon.com/gp/product/B07Q6RSWYC]]

*** Audio jacks

**** Mounting on panel

Need a very thin panel.
The threaded part fits nicely through a 5/16" hole.
The ridge needs slightly larger than 3/4" to fit.

On [2021-11-21] I succuessfully attached one to a 3/8" thick board
by drilling *almost* all the way through with a...1/2", I think, bit,
and finishing the hole from the other side with a 5/16" bit.

:aside_regarding_power_barrel_jacks:
Based on some info StainTest.tef#2018-12-10:

#+BEGIN_QUOTE
| Use                      | Size             | Notes                                                                |
|--------------------------+------------------+----------------------------------------------------------------------|
| 2.1mm power jack (inner) | 9/32"            | Space for the power jack's shaft                                     |
| 2.1mm power jack (outer) | 7/16"            | Space for the power jack's flange                                    |
#+END_QUOTE

These same holes should also work for my DC barrel connectors.  Cool, cool.  Not yet tested.
:END:

**** Pinout

There are 4 connectors  on the back.  What do they mean?
Pin numbers are tiny, on the end of the connector,
but they are visible.  If we look at the back side, where the connectors are,
and call pin '1' the bottom (pins 1 and 3 have notches on the outside of the barrel),
then count clockwise: Pins 1, 2, 3, 4 are bottom, left, top, right, respectively.

:omtp_pinout:
According to [[https://smile.amazon.com/gp/product/B089222S84][the product page]]:
- Pin 1 :: Ground
- Pin 2 :: Microphone
- Pin 3 :: Left channel
- Pin 4 :: Right channel

According to comments *THIS IS WRONG* if you are interested in the TRRS standard.
:END:

You might want to cross-reference [[https://images-na.ssl-images-amazon.com/images/I/71iU51D0STL._SL1600_.jpg][this pic uploaded by Mike G]].  He says

#+BEGIN_QUOTE
Their pin diagram references the seldom used OMTP standard
#+END_QUOTE

His 2, 3, 4, 1 (tip to base of plug) matches the schematic the product page provides,
and correspond to left, right, ground mic.

Or, even better, use the multimeter with a plugged-in cable.

#+BEGIN_QUOTE
Good quality jack socket. DO check with a multimeter, where is
ground, left, right and mic, as the diagram shown is incorrect. It
works very well though, and the nut makes it very easy to install.
#+END_QUOTE

According to [[https://en.wikipedia.org/wiki/Phone_connector_(audio)#TRRS_standards][the wikipedia page]], 'Most Android devices' use the CTIA/AHJ standard,
which is, from tip to base: left, right, ground mic.
Which matches what Mike G said.

[2021-11-23T14]  I have used the multimeter to check the connection with one of my 3-conductor TRS cables.

| pin | TRS part | role   | matches  |
|     |          |        | Mike P's |
|-----+----------+--------+----------|
|   1 | sleeve   | common | mic?     |
|   2 | tip      | left   | left     |
|   3 | ring     | right  | right    |
|   4 | sleeve   | common | gnd?     |

I like [[http://picture-files.nuke24.net/uri-res/raw/urn:sha1:KOPCKJF7FNPCUMIBY67BNA4FULXWZ76W/TRRS-3.5mm.png][this pic]] (found on [[https://www.facebook.com/techiemanthan1/posts/all-about-35mm-jack-ts-trs-trrs-trrrs-jack-information-in-hindihttpsyoutubetg4ma/427421501444257/][Techie Manthan's facebook page]])
showing the different number of rings they cram onto these connectors.

More good pix on this [[https://appmaker.se/making-a-long-sound-cable/][making a long sound cable]] page.

**** Orientation on my panels

How do I want to orient the connectors?

Clockwise order is: 1, 2, 3, 4, which correspond to mic, left, right, ground.

If I have mic at the top and ground on the side, they would be close to their respective screws.
So let's say 1 goes up.  It doesn't really matter, since I'll have to twist the wires around
anyway since everything will be so close together.

Wire colors?  I don't know if there's a standard for the wires inside stereo cables,
but for RCA plugs, red is for right (yay), white for left.
So I could go that route.  What about mic?  Yellow for video, but is
the TRRS usage for camcorders the same as headphones/mic?
According to the wikipedia page, it is for 'CTIA-style AV' - they just swap video for mic.

So yeah, this color scheme should be fine:

| pin | TRRS part | role  | color  |
|-----+-----------+-------+--------|
|   1 | sleeve    | mic   | yellow |
|   2 | tip       | left  | red    |
|   3 | ring 1    | right | white  |
|   4 | ring 2    | gnd   | black  |

And we shall call this...

:aside_about_usbc:
Interestingly,

#+BEGIN_QUOTE
The USB Type-C Cable and Connector Specification Revision 1.1
specifies a mapping from a USB-C jack to a 4-pole TRRS jack, for the
use of headsets, and supports both CTIA and OMTP (YD/T 1885–2009)
modes. See Audio Adapter Accessory Mode (Appendix A). Some devices
transparently handle many jack standards,[60][61] and there are
hardware implementations of this available as components.[62]
#+END_QUOTE

USB-C does everything.  Neat.
:END:

*** Isolator

The block across the middle separates the isolated circuits.
I know this because I attached a multimeter and after a short delay
(expected for an inductor, I suppose)
it indicates the two legs on the same side are connected.
Simple enough!


** [2021-11-21]

Cut, drilled, sanded, and started soaking the panels in BLO.
It took all afternoon.
Let's call these WSBATCH-100623.

** [2021-11-22], regarding WSBATCH-100623

It may have mde more sens to put the threaded holes on the main panel.

Advangtages of threaded holes in the main panel:
- Would allow stacking sub-panels
- Gives more flexibility for sub-panel orientation

Advantages of threading the holes on the sub-panels:
- screws can be semi-permanently attached to the sub-panels
  so you don't need to worry about messing up the connectors

A reasonable compromise might be to leave the holes on the
headphone jack panels threaded, screw through from the back,
and then the connector panels could be placed over the front.

I want front-access to the wiring so I can reconfigure things easily.

[2021-11-22T12:40]
Oh no that means the screws won't be attached to the audio panel anyway!
Well maybe if I superglue the terminals down it'll be fine.

I could just drill through all the holes to un-thread them.
But I kind of like the threaded holes.
I'll see what happens.
And probably, in the future, try to remember to thread the holes
on the rack, not on the panels!
