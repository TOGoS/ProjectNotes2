#+TITLE: Vacuum Cleaner Hose Dimensions

See also: notebook WSITEM-3404, p29.

So there's 4" ducting, which is common for dryer vents and bathroom fans.
Then there's 'universal' 1+1/4" for vacuum cleaner attachments,
and 2+1/2" for shop-vacs.

What sized connectors do mine have?

Small blue shop vac:
- Vacuum to hose: ~35mm, with hose on the inside
- Hose to attachment: ~32mm, with hose on the inside (fits into Kreg Jig)

Red shop vac (WSITEM-200354):
- Vacuum to hose: ~2+1/4", with hose on the inside.
- Hose to attachment: ~32mm, with hose on the inside (fits into Kreg Jig)

Table saw:
- 2+1/4" ID hose port

Performax router table:
- 2+1/4" ID hose port


However, when you search online for adapters, they all talk about "2-1/2" things!
This might actually mean the 2+1/4" ones like I have:

From https://woodworking.stackexchange.com/questions/3284/how-are-vacuum-hose-sizes-measured:

#+BEGIN_QUOTE
For example: on my shop vac, the end of the hose measures 2-1/8"ID x
2-1/4"OD, and the end of the attachments measure 2-1/4"ID x
2-1/2"OD. What is the correct way to refer to the size of this hose?
#+END_QUOTE

#+BEGIN_QUOTE
Ridgid's Shopvac accessories come in 1-7/8" and 2-1/2" sizes. So from
this we can deduce that Ridgid, at least, uses the outer diameter of
the attachments. The sizes you describe match what Ridgid refers to as
2-1/2". That's just Ridgid, of course, but mostly I see this
consistently. YMMV.

...

2. Take a ruler with you when shopping.
#+END_QUOTE

That's my plan.

#+BEGIN_QUOTE
There are also 1.5" fitting and 1.25" fittings (diameter where the
male and female parts actually touch). 1.25" has been around for a
long time, and is used on many different older vacuums.

2.25" and 2.5" are their larger cousins. If you buy an extension hose,
often it will come with adapters for both sizes. Again, the 2.25" is
the outer diameter of the male fitting where it inserts into the
female hole of the same size.
#+END_QUOTE

[2020-08-06T14:12:48-05:00] I bought a Rigid
"[[https://www.homedepot.com/p/RIDGID-Hose-and-Accessory-Adapter-Kit-for-RIDGID-Wet-Dry-Shop-Vacuums-VT1755/202077239][3-piece Adapter Kit]]" from Home Depot
and every part is completely uselss.  Or backwards.
There's a 1+1/4" OD piece, and two 2+1/4" ID pieces.
The opposite of what I need!
I guess they expect that you'll be adapting a 2+1/4" OD hose to a 2+1/4" ID tool.
I'm trying to adapt a 1+1/4" OD hose to a 2+1/4" ID tool (the saw or router table).
