$fn = 30; // Make curves look better and stuff

/*
	Unistrut dimensions according to http://www.unistrut.us/DB/PDF_Archive/No_12.pdf
	- Total width/height: 1+5/8"
	- Channel mouth width: 7/8"
	- Channel mouth depth: 9/32"

	So with that I could design a panel that's designed to be bolted to unistrut
	with a single bolt but could also be bolted to a flat surface
	by having grooves for the 'lips' slightly less than 9/32" deep
	(since the spring nut sticks up into the mouth a little bit) to prevent rotation.
*/

//// Global variables

big = 24;
// 7/64 according to the internet, but I've found that 1/8 works okay:
number_6_tap_diameter = 1/8;
standard_panel_margin = 1/32;
standard_panel_chamfer = 1/8 - standard_panel_margin;

//// Utils

module chamfered_rect(width, height, chamfer) {
	xn = width/2 - chamfer;
	xf = width/2;
	yn = height/2 - chamfer;
	yf = height/2;
	polygon([
		[+xf,+yn],[+xn,+yf],
		[-xn,+yf],[-xf,+yn],
		[-xf,-yn],[-xn,-yf],
		[+xn,-yf],[+xf,-yn]
	]);
}

module chamfered_panel(width, height, chamfer, thickness) {
	linear_extrude(height=thickness, center=true) {
		chamfered_rect(width, height, chamfer);
	}
}

module togpanel_screw_holes(panel_units) {
	for( y=[-panel_units/2+0.5:0.5:+panel_units/2-0.5] )
		for( x=[-1.5, 1.5] ) translate([x, y, 0])
			cylinder(d=5/32, h=2, center=true);
}


//// Mesh panel, v1.0

mesh_panel_thickness = 1/8;

module diamond_mesh(width, height, spacing, thickness) {
	lang = (height+width)/2;
	intersection() {
		square([width, height], true);
		for( y=[-lang:spacing:+lang] ) {
			translate([0,y,0]) {
				polygon([
					[-width/2,-width/2-thickness*1.4/2],
					[+width/2,+width/2-thickness*1.4/2],
					[+width/2,+width/2+thickness*1.4/2],
					[-width/2,-width/2+thickness*1.4/2],
				]);
				polygon([
					[+width/2,-width/2-thickness*1.4/2],
					[-width/2,+width/2-thickness*1.4/2],
					[-width/2,+width/2+thickness*1.4/2],
					[+width/2,-width/2+thickness*1.4/2],
				]);
			}
		}
	}
}

module mesh_panel(panel_units) {
	difference() {
		chamfered_panel(3.5, panel_units, standard_panel_chamfer, mesh_panel_thickness);
		chamfered_panel(2.5, panel_units-1/4, 1/8, mesh_panel_thickness*4);
		togpanel_screw_holes(panel_units);
	}
	linear_extrude(height=mesh_panel_thickness, center=true) {
		diamond_mesh(2.75, panel_units, 1/4, 1/16);
	}
}

//// Unistrut TOGRail Adapter Panel, v1.0

strut_width = 1+5/8;
strut_mouth_width = 7/8;
strut_groove_width = (strut_width - strut_mouth_width) / 2;
strut_groove_distance_from_center = (strut_mouth_width + strut_groove_width)/2;
uta_panel_strut_groove_depth = 4/32; // something less than 9/32 to give the spring nut some room
uta_panel_thickness = 1/4;
uta_panel_color = [0.4,0.3,0.2];
uta_panel_margin = standard_panel_margin;

module uta_panel(panel_units) {
	difference() {
		chamfered_panel(
			3.5 - uta_panel_margin*2,
			panel_units - uta_panel_margin*2,
			standard_panel_chamfer,
			uta_panel_thickness
		);
		translate([0,0,uta_panel_thickness/2]) {
			for( x=[-1:2:1] ) {
				translate([x*strut_groove_distance_from_center,0,0])
					cube([strut_groove_width,big,uta_panel_strut_groove_depth*2], true);
				translate([x*1.5,0,0])
					cube([5/8,big,uta_panel_strut_groove_depth*2], true);				
			}
			//for( y=[-1:2:1] ) {
			//	translate([0,y*strut_groove_distance_from_center,0])
			//		cube([big,strut_groove_width,strut_groove_depth*2], true);
			//}
		}
		for( y=[-panel_units/2+1 : 1 : +panel_units/2-1] )
			translate([0,y,0])
				cylinder(d=3/8, h=2, center=true);
		togpanel_screw_holes(panel_units);
	}
}

//// TOG Rail, v2.1

// Small indent to use as drill guide in case you need extra holes.
// Center on point to put the divit.
// By default, pointy end is down.  Rotate accordingly.
module tograil_divit() {
	cylinder(h=3/32, d1=0, d2=3/16, center=true);
}

module tograil(length,thickness,groove_depth=5/32) {
	difference() {
		cube([1/2,length,thickness], true);
		
		for(y=[-length/2+0.5:0.5:length/2-0.5]) {
			translate([0,y,0]) {
				cylinder(h=thickness*2, d=number_6_tap_diameter, center=true);
			}
		}
		for(y=[-length/2+1:1:length/2-1]) {
			translate([0,y,thickness/2]) {
				cube([1, 0.5, groove_depth*2], true);
			}
		}
		
		// Always put divits at the ends because grooves won't go there
		for(y=[-length/2+0.25, length/2-0.25]) {
			translate([0,y,+thickness/2]) rotate([0,0,0]) tograil_divit();
		}
		for(y=[-length/2+0.25:0.25:length/2-0.25]) {
			if( groove_depth == 0 ) translate([0,y,+thickness/2]) rotate([0,0,0]) tograil_divit();
			translate([0,y,-thickness/2]) rotate([0,180,0]) tograil_divit();
			translate([-0.25,y,0]) rotate([0,-90,0]) tograil_divit();
			translate([+0.25,y,0]) rotate([0,+90,0]) tograil_divit();
		}
		translate([0,-length/2,0]) rotate([+90,0,0]) tograil_divit();
		translate([0,+length/2,0]) rotate([-90,0,0]) tograil_divit();
	}
}

//// Screws and washers

washer_thickness = 1/32;
bolt_color = [1/2,1/2,1/2];

module washer(innerdiam,thickness,outerdiam) {
	color(bolt_color) difference () {
		cylinder(h=thickness,d=outerdiam,center=true);
		cylinder(h=big,d=innerdiam,center=true);
	}
}

module screwrot() {
	rotate([0,0,rands(0,360,1)[0]]) children();
}

module normal_washer() {
	washer(10/64, washer_thickness, 24/64);
}

module screw(diam,height,headdiam) {
	headheight=headdiam/3;
	/*intersection() {
		translate([-big/2,-big/2,0]) cube([big,big,big]);
		sphere(headdiam);
	}*/
	color(bolt_color) {
		difference() {
			translate([0,0,headheight/2])
				scale([1,1,headheight/headdiam])
					sphere(headdiam/2);
			translate([0,0,headheight]) {
				cube([headdiam*1/8, headdiam*2/3, headheight], true);
				cube([headdiam*2/3, headdiam*1/8, headheight], true);
			}
		}
		translate([0,0,-height]) cylinder(h=height+headheight/2,d=diam);
	}
}

module washerandscrew() {
	screw(9/64, 1, 9/32);
	translate([0,0,1/64]) normal_washer();
}

module panel_screws(panel_units, panel_thickness) {
	for( y=[-panel_units/2+0.5, +panel_units/2-0.5] ) {
		for( x=[-3/2:3:+3/2] ) {
			translate([x,y,panel_thickness]) {
				screwrot() washerandscrew();
			}
		}
	}
}

module panels_on_rails() {
	panel_units = 4;
	color([0.1,0.4,0.2]) {
		translate([-3/2,0,-3/8]) rotate([0,180,0]) tograil(panel_units*3, 3/4);
		translate([+3/2,0,-3/8]) rotate([0,180,0]) tograil(panel_units*3, 3/4);
	}
	
	translate([0,0,0]) {
		translate([0,0,uta_panel_thickness/2])
			color(uta_panel_color)
				uta_panel(panel_units);
		panel_screws(panel_units, uta_panel_thickness - uta_panel_strut_groove_depth);
	}
	
	translate([0,-panel_units,0]) {
		translate([0,0,mesh_panel_thickness/2])
			color(uta_panel_color) mesh_panel(panel_units);
		panel_screws(panel_units, mesh_panel_thickness);
	}
}

scale(25.4) { // All my units are inches
	//tograil(4, 3/4);
	//panels_on_rails();
	uta_panel(4);
	//mesh_panel(4);
}
