(defun cost-per-inch (cost itemcount mmperitem)
  (/ cost (/ (* itemcount mmperitem) 25.4)))


## OpenBeam

Size: 15x15mm
$/inch: <0.47
Standard fastener: M3 screws
Commonly used in: 3D printers
Common lengths: 300mm (11.8"), 600mm (23.6")
Ease of getting accessories: All over Amazon

### This kit: https://smile.amazon.com/gp/product/B0195NEDS2

8x300mm, 4x270mm, 4x240mm, 4x 210mm, 4x 180mm, 6x 150mm, 4x 120mm, 4x90mm, 4x60mm, 4x30mm
Plus a bunch of stuff

So that's
(+ (* 8 300) (* 4 270) (* 4 240) (* 4 210) (* 4 180) (* 6 160) (* 4 120) (* 4 90) (* 4 60) (* 4 30)) = 8160mm for 150$,
(cost-per-inch 150 1 8160) = 0.47$/inch (but actually less because other parts in set)


### Beams: https://smile.amazon.com/OpenBeam-1515-meter-Clear-Anodized/dp/B0196FGT0Y

6 meters for ???  not available darn

### https://smile.amazon.com/OpenBeam-1515-600mm-Clear-Anodized/dp/B0196FHMZ0

(cost-per-inch 52 8 600) = 0.28$/inch

### https://smile.amazon.com/OpenBeam-1515-210mm-Clear-Anodized/dp/B0196FH7Y6

(cost-per-inch 23 8 210) = 0.35$/inch

### https://smile.amazon.com/OpenBeam-1515-meter-Black-Anodized/dp/B0196FGS7S

(cost-per-inch 69 6 1000) = 0.30$/inch



## 20mm

Standard fastener: M5,M4, or M3 screws
Where to buy: Adafruit, inventables.com

### Joh's rig

Uses 20mm beams, probably M5 (but possibly 10-32) screws.

I fit a 10-32 screw into a nut that Joh brought over,
but now that I have my own I realize that a 10-32 screw will sort of thread into a M5 nut
(but an M5 screw will more obviously not fit into a 10-32 nut, which is good).

I couldn't find any reference to 10-32 nuts to go with 2020 beams,
so I suspect that my initial 'hey it's 10-32s' was wrong and that
his rig is made using M5 screws and nuts.

There are 'fractional' versions of these things ('1515 fractional' is
1.5" instead of 15mm beams, etc), but they mostly use #8-32 and
1/4-20s.



https://eriknilssonblog.blogspot.com/2013/09/oh-sae-can-you-see.html

### https://smile.amazon.com/80-20-20-2020-T-Slotted-Extrusion/dp/B00CBGUPGK

(cost-per-inch 11 1 305) = 0.92$/inch

### https://www.inventables.com/technologies/aluminum-extrusion-20mm-x-20mm-black

(cost-per-inch 14 1 1000) = 0.35$/inch PLUS SHIPPING

Apparently can amortize shipping by buying a bunch.

### https://www.adafruit.com/product/1221

(cost-per-inch 7.50 1 610) = 0.31$/inch PLUS SOME SHIPPING

### https://www.ebay.com/itm/2020-20mm-Aluminum-Framing-Extrusion-400mm-600mm-800mm-1000mm-1200mm-Lengths/182631490680

(cost-per-inch 5.20 1 400) = 0.33$ each
(cost-per-inch 7.80 1 600) = 0.33$ each
(cost-per-inch 10.10 1 800) = 0.32$ each
(cost-per-inch 15.70 1 1000) = 0.40$ each PLUS SHIPPING



## MakerBeam, OpenBeam

Comparison of OpenBeam (15mm) vs MakerBeam (10mm):
https://www.makerbeam.com/blogs/makerbeam/specifications-makerbeam-and-openbeam/

MakerBeam also has MakerBeamXL, which maybe means they realized that 15mm was necessary for a lot of use cases.

MakerBeam stiffness area: 48,16mm²
MakerBeamXL stiffness area: 113,52mm²
OpenBeam stiffness area: 113,52mm²

So OpenBeam and MakerBeam are both more than twice as sturdyas the 10mm stuff, which makes sense.

They all use M3 screws.

## 20mm stuff

20mm is a little over 0.75", so that looks better in my mind for building a rig than 15mm stuff.

"20mm is probably fine too tbh. Most of our newer machines have a lot
of 2020 or 1515, but we're talking conveyors, motors, etc etc"

-- Nick

## Lengths needed

(/ 15 25.4)
(/ 20 25.4)
(/ 958 25.4)
(/ 708 25.4)
(/ 750 25.4)

My rig is:
11" (* 11 25.4) = 280mm interior height
14" (* 14 25.4) = 356mm wide
20" (* 20 25.4) = 508mm deep

If I got 800mm segments:
(+ 356 280 280)
(+ 508 280)
(+ 508 280)



## Bought

Some of these longbois: https://www.ebay.com/itm/182631490680

Some of these t-slotbois: https://www.ebay.com/itm/182511748557
'Fit to 20-series metric T-slot profiles commonly used in small CNC machines and custom industrial enclosures'

Assuming those nuts go with those beams (!!!)

So I've bought into t-slot.  There's also V-slot.  But that's like a subset of T-slot.
https://www.makerstore.com.au/blog/t-slot-v-slot-profiles/

I ordered some things from Amazon, too.  We'll see if they work with the beams.
