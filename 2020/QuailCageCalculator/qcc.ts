// Let's calculate quail cage parts

type PartName = string;

type PartsManifest = {[partName:string]: number};

interface Recipe {
	outputParts: PartsManifest;
	inputParts?: PartsManifest;
	instructions?: string;
}

// Bolt lengths refer to how much material the bolt must pass through.
// Exact length depends on what kind of nuts you use
// (will need a longer bolt if using hex nuts than T-nuts or weld nuts)

let recipes : Recipe[] = [
	{
		outputParts: {"quail-cage": 1},
		inputParts: {
			"quail-cage-floor": 1,
			"45x9in-back-panel": 2,
			"18x15in-frame": 2,
			"18x15in-screen-panel": 5,
			"45x18in-front-frame": 1,
			"18in-gridbeam": 4,
			"18x15in-top-panel": 3,
			"2.25in-bolt": 16, // Back and ends
			"2.63in-bolt": 4, // Front corners
			"1.13in-bolt": 8, // Front panels to frame
		},
	},
	{
		outputParts: {"18x15in-frame": 1},
		inputParts: {
			"15x1.5x0.38in-frame-rail": 4,
		},
		instructions: "Staple/glue rails into a rectangle.\n"
	},
	{
		outputParts: {"18x15in-screen-panel": 1},
		inputParts: {
			"12x1.5x0.38in-frame-rail": 2,
			"18x1.5x0.38in-frame-rail": 2,
			"18x15in-screen": 1,
		},
		instructions: "" +
			"Staple/glue rails into a rectangle.\n" +
			"Paint as desired.\n" +
			"Hot glue screen over frame, covering mounting holes.\n" +
			"Drill through mounting holes.\n"
	},
	{
		outputParts: {"45x18in-front-frame": 1},
		inputParts: {
			"45x1.5x0.75in-frame-rail": 2,
			"15x1.5x0.75in-frame-rail": 2,
			"15x3x0.75in-frame-rail": 2,
		}
	},
	{
		outputParts: {"quail-cage-floor": 1},
		inputParts: {
			"48x18in-plywood": 1,
			"48x0.5x0.5-wood-strip": 1,
			"17.5x0.5x0.5in-wood-strip": 2,
		}
	},
	{
		outputParts: {"18x15in-top-panel": 1},
		inputParts: {
			"18x1.5x0.38in-frame-rail": 2,
			"12x3x0.38in-frame-rail": 2,
			"18x15in-screen": 1,
			"top-panel-bracer": 2,
			"1.13in-bolt": 4,
		}
	},
];

class RecipeDatabase {
	protected recipesByOutputPartName:{[partName:string]: Recipe[]} = {};

	constructor(recipes:Recipe[]) {
		for( let r in recipes ) {
			let recipe = recipes[r];
			for( let opn in recipe.outputParts ) {
				(this.recipesByOutputPartName[opn] ||= []).push(recipe);
			}
		}
	}
	
	public getRecipesFor(partName:PartName): Recipe[] {
		return this.recipesByOutputPartName[partName] || [];
	}
}

let recipeDatabase = new RecipeDatabase(recipes);

interface AggregatedRecipe {
	inputParts: PartsManifest;
	// TODO: outputParts from bucket
}

function collectInputs(partName:PartName, count:number, bucket:PartsManifest, into:PartsManifest) {
	let recipes = recipeDatabase.getRecipesFor(partName);
	// TODO: Put extras in bucket and use them

	let isAtomic = true;
	
	if( recipes.length > 0 ) {
		for( let r in recipes ) {
			let recipe = recipes[r];
			if( recipe.inputParts ) {
				isAtomic = false;
				for( let inputPartName in recipe.inputParts ) {
					collectInputs(inputPartName, count*recipe.inputParts[inputPartName], bucket, into);
				}
				break;
			}
		}
	}

	if( isAtomic ) {
		if( into[partName] == undefined ) into[partName] = 0;
		into[partName] += count;
	}
}

function getAggregatedRecipeFor(partName:PartName, count:number):AggregatedRecipe {
	let bucket:PartsManifest = {};
	let inputParts:PartsManifest = {};
	collectInputs(partName, count, bucket, inputParts);
	return {
		inputParts
	};
}

function printPartsManifest(manifest: PartsManifest) {
	for( let partName in manifest ) {
		console.log(manifest[partName] + " x " + partName);
	}
}

let ar = getAggregatedRecipeFor("quail-cage", 2);
printPartsManifest(ar.inputParts);
