// WSTYPE-100391: Design for a small
// 3D-printable TOGRack box

// Global control for curve detail; 1-4 = rough-full; higher is overkill
fn_mult = 4; // [0.5 : 0.5 : 5]
fn_min = 15;
fn_max = 90;

mm   =  1;
inch = 25.4;

item_outer_dims = [4.5*inch, 4.5*inch, 1.5*inch];
panel_thickness = 1/8*inch;
panel_outer_dims = [item_outer_dims[0], item_outer_dims[1], panel_thickness];
item_body_outer_dims = [item_outer_dims[0], item_outer_dims[1], item_outer_dims[2] - panel_thickness*2];
item_outer_corner_radius = 1/16*inch;
item_h_corner_radius = 1/4*inch;

// The bulkhead-mount 2.1mm barrel connectors of which I have several,
// and that I have measured many times, but measuring them again
// seems easier than finding where I wrote the measurements down,
// are about like this:
// - head diameter: 10.1mm
// - head height:    4.5mm
// - shaft diameter; 7.8mm
// So, to make a hole for them...
barrel_conn_counterbore_diameter = 15 * mm;
barrel_conn_counterbore_depth    =  5 * mm;
barrel_conn_bore_diameter        =  8 * mm;

// Distance from base of flange to end of electrical-male GX16 connector
gx16_port_height = 7*mm;
gh16_shaft_diameter = 15.6*mm;
gh16_flange_diameter = 18.1*mm; // Bore needs to be smaller than this!
gh16_bore_diameter = 16*mm;
// Maximum distance between bottom of flange and washer
gx16_shaft_max_bore_length = 4.4*mm;
gx16_plug_nut_diameter = 18*mm;
gx16_counterbore_diameter = 1*inch;

gx12_port_height = 6*mm;
gh12_shaft_diameter = 11.7*mm;
gh12_flange_diameter = 14.8*mm; // Bore needs to be smaller than this!
gh12_bore_diameter = 12*mm;
// Maximum distance between bottom of flange and washer
gx12_shaft_max_bore_length = 4.4*mm;
gx12_plug_nut_diameter = 15.4*mm;
gx12_counterbore_diameter = 1*inch;

// For #6 screws, this works fine
number6_to_be_threaded_hole_diameter = 3/32 * inch;
number6_flathead_screw_top_diameter = 0.262*inch;
number6_hole_diameter = 5/32*inch;

function fn_for_diam(d) = max(fn_min, min(fn_max,
	fn_mult * sqrt(d * 8)
));
function fn_for_rad(r) = fn_for_diam(2*r);

// Default $fn
$fn = min(fn_max, 10*fn_mult);

// Use at surface, with -Z being into the to-be-drilled area
module counterbored_barrel_conn_hole(depth) {
	cylinder(d=barrel_conn_counterbore_diameter, h=barrel_conn_counterbore_depth*2, center=true, $fn=fn_for_diam(barrel_conn_counterbore_diameter) );
	cylinder(d=barrel_conn_bore_diameter, h=depth*3, center=true, $fn=fn_for_diam(barrel_conn_bore_diameter));
}

module x_extended_counterbore(d, h) {
	cylinder(d=d, h=h, center=true, $fn=fn_for_diam(d));
	translate([20, 0, 0]) cube([40,d,h], center=true);
}

module counterbored_gx16_port_hole() {
	x_extended_counterbore(d=gx16_counterbore_diameter, h=gx16_port_height * 2);
	cylinder(d=gh16_bore_diameter, h=100, center=true, $fn=fn_for_diam(gh16_bore_diameter));
	translate([0,0,0-gx16_port_height-gx16_shaft_max_bore_length-20]) {
		//cylinder(d=gx16_counterbore_diameter-2, h=40, center=true);
		//xy_rounded_cube([gx16_counterbore_diameter
		x_extended_counterbore(d=gx16_counterbore_diameter, h=40);
	}
}

module counterbored_gx12_port_hole() {
	x_extended_counterbore(d=gx12_counterbore_diameter, h=gx12_port_height * 2);
	cylinder(d=gh12_bore_diameter, h=100, center=true, $fn=fn_for_diam(gh12_bore_diameter));
	translate([0,0,0-gx12_port_height-gx12_shaft_max_bore_length-20]) {
		x_extended_counterbore(d=gx12_counterbore_diameter, h=40);
	}
}

module item_hull(outer_dims) {
	$fn = fn_for_rad(item_h_corner_radius);
	minkowski() {
		linear_extrude(outer_dims[2]-item_outer_corner_radius*2, center=true) {
			hull() {
				for( yf = [-1,1] ) for( xf = [-1,1] ) {
					translate([
						xf*(outer_dims[0]/2 - item_h_corner_radius),
						yf*(outer_dims[1]/2 - item_h_corner_radius),
						0
					]) circle(r=item_h_corner_radius - item_outer_corner_radius);
				}
			}
		}
		sphere(r=item_outer_corner_radius);
	}
}

module item_lid_hull(outer_dims) {
	intersection() {
		item_hull([outer_dims[0], outer_dims[1], outer_dims[2]*2]);
		translate([0,0,outer_dims[2]]) {
			cube([outer_dims[0]*2, outer_dims[1]*2, outer_dims[2]*2], center=true);
		}
	}
}

module countersunk_number6_hole(depth) {
	$fn = fn_for_diam(number6_flathead_screw_top_diameter);
	translate([0,0,-depth/2]) {
		cylinder(d = number6_hole_diameter, h = depth*2, center=true);
	}
	cylinder(
		d1 = 0,
		d2 = number6_flathead_screw_top_diameter*2,
		h  = number6_flathead_screw_top_diameter,
		center = true
	);
}

module item_lid_edge_holes(outer_dims) {
	for( y = [-outer_dims[1]/2+0.25*inch, +outer_dims[1]/2-0.25*inch] ) {
		for( x = [-outer_dims[0]/2+0.25*inch : 0.5*inch : +outer_dims[0]/2-0.25*inch] ) {
			translate([x,y,outer_dims[2]]) countersunk_number6_hole(outer_dims[2]*2);
		}
	}
	for( x = [-outer_dims[0]/2+0.25*inch, +outer_dims[0]/2-0.25*inch] ) {
		for( y = [-outer_dims[1]/2+0.25*inch : 0.5*inch : +outer_dims[1]/2-0.25*inch] ) {
			translate([x,y,outer_dims[2]]) countersunk_number6_hole(outer_dims[2]*2);
		}
	}
}

module item_lid_with_diamond_grating(outer_dims) {
	difference() {
		item_lid_hull(outer_dims);
		item_lid_edge_holes(outer_dims);
		for( x = [-outer_dims[0]/2+1*inch : 0.5*inch : +outer_dims[0]/2-1*inch] ) {
			for( y = [-outer_dims[1]/2+1*inch : 0.5*inch : +outer_dims[1]/2-1*inch] ) {
				translate([x,y,outer_dims[2]/2]) rotate([0,0,45]) cube([0.25*inch, 0.25*inch, outer_dims[2]*2], center=true);
			}
		}
		for( x = [-outer_dims[0]/2+0.75*inch : 0.5*inch : +outer_dims[0]/2-0.75*inch] ) {
			for( y = [-outer_dims[1]/2+0.75*inch : 0.5*inch : +outer_dims[1]/2-0.75*inch] ) {
				translate([x,y,outer_dims[2]/2]) rotate([0,0,45]) cube([0.25*inch, 0.25*inch, outer_dims[2]*2], center=true);
			}
		}
	}
}

module item_lid_with_gridbeam_holes(outer_dims) {
	difference() {
		item_lid_hull(outer_dims);
		item_lid_edge_holes(outer_dims);
		translate([0,0,outer_dims[2]/2]) cylinder(d=5/16*inch, h=outer_dims[2]*2, center=true);
		for( x = [-0.75*inch : 1.5*inch : +0.75*inch] ) {
			for( y = [-0.75*inch : 1.5*inch : +0.75*inch] ) {
				d = 5/16*inch;
				translate([x,y,outer_dims[2]/2]) cylinder(d=d, h=outer_dims[2]*2, center=true, $fn=fn_for_diam(d));
			}
		}
		for( x = [-1.5*inch : 1.5*inch : +1.5*inch] ) {
			for( y = [-1.5*inch : 1.5*inch : +1.5*inch] ) {
				d = 5/32*inch;
				translate([x,y,outer_dims[2]/2]) cylinder(d=d, h=outer_dims[2]*2, center=true, $fn=fn_for_diam(d));
			}
		}
	}
}

module rounded_rectangle(outer_dims, r, center) {
	hull() {
		for( yf = [-1,1] ) for( xf = [-1,1] ) {
			translate([
				xf*(outer_dims[0]/2 - r),
				yf*(outer_dims[1]/2 - r),
				0
			]) circle(r=r);
		}
	}
}

module xy_rounded_cube(outer_dims, r, center) {
	linear_extrude(outer_dims[2], center=center) {
		rounded_rectangle(outer_dims, r=r, center=center);
	}
}

module item_body_hull(outer_dims, center) {
	xy_rounded_cube(outer_dims, r=item_h_corner_radius, center=center, $fn=fn_for_rad(item_h_corner_radius));
}

module tograck_hole(depth) {
	cylinder(h=depth*3, d=number6_to_be_threaded_hole_diameter, center=true);
}

module base_item_body(outer_dims) {
	difference() {
		item_body_hull(outer_dims);
		translate([0,0,3/4*inch + outer_dims[2]/2]) {
			xy_rounded_cube([outer_dims[0]-1*inch, outer_dims[1]-1*inch, outer_dims[2]], r=1/8*inch, center=true);
		}
		xy_rounded_cube([outer_dims[0]-1*inch, 2.5*inch, outer_dims[2]*2], r=1/16*inch, center=true);
		for( yf = [-1,1] ) {
			for( x = [-outer_dims[0]/2+0.25*inch : 0.5*inch : +outer_dims[0]/2-0.25*inch] ) {
				translate([x, yf*(outer_dims[1]/2-0.25*inch), 0]) tograck_hole(outer_dims[2]);
				translate([x, yf*(outer_dims[1]/2-0.75*inch), 0]) tograck_hole(outer_dims[2]);
			}
		}
		for( xf = [-1,1] ) {
			for( y = [-1*inch, 1*inch] ) {
				translate([xf*(outer_dims[1]/2-0.25*inch), y, 0]) tograck_hole(outer_dims[2]);
			}
		}
	}
}

module 4port_item_body(outer_dims) {
	difference() {
		base_item_body(outer_dims);
		for( xf = [-1, 1] ) {
			for( y = [-0.5*inch, +0.5*inch] ) {
				translate([xf * outer_dims[0]/2, y, 1/2*inch]) {
					rotate([0,xf * 90,0]) counterbored_barrel_conn_hole(0.5*inch);
				}
			}
		}
	}
}

module large_end_port_item_body(outer_dims) {
	difference() {
		base_item_body(outer_dims);
		for( xf = [-1, 1] ) {
			for( y = [0] ) {
				translate([xf * outer_dims[0]/2, y, 1/2*inch]) {
					rotate([0,90,90-90*xf]) children();
				}
			}
		}
		for( yf = [-1, 1] ) {
			for( x = [-0.75*inch, +0.75*inch] ) {
				translate([x, yf * outer_dims[0]/2, 1/2*inch]) {
					rotate([yf * 90,0,0]) counterbored_barrel_conn_hole(1.5*inch);
				}
			}
		}
	}
}

module gx12_item_body(outer_dims) {
	large_end_port_item_body(outer_dims) {
		counterbored_gx12_port_hole();
	}
}

translate([-item_outer_dims[0] - 0.5*inch, 0, 0]) {
	item_lid_with_gridbeam_holes(panel_outer_dims);
}

translate([0, 0, 0]) {
	item_lid_with_diamond_grating(panel_outer_dims);
}

translate([item_outer_dims[0] + 0.5*inch, 0, 0]) {
	gx12_item_body(item_body_outer_dims);
}
