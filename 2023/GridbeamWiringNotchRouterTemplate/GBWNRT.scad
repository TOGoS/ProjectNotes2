$fs = 1;

inch = 25.4;

thickness = 1/4*inch;
depth     = 7.5*inch;
width     = 9  *inch;
edge_curve_radius = 1/4*inch;
// Radius of the curves at the end of the 'I' serif
tb_radius = 1/4*inch;

module rounded_rectangle(size, r) {
	hull() {
		translate([-size[0]/2+r, -size[1]/2+r]) circle(r);
		translate([ size[0]/2-r, -size[1]/2+r]) circle(r);
		translate([ size[0]/2-r,  size[1]/2-r]) circle(r);
		translate([-size[0]/2+r,  size[1]/2-r]) circle(r);
	}
}

module cutout_shape() {
	$fn = 70;
	difference() {
		union() {
			hull() {
				translate([-3/8*inch-edge_curve_radius, -3/4*inch-tb_radius, 0]) circle(r=tb_radius);
				translate([ 3/8*inch+edge_curve_radius, -3/4*inch-tb_radius, 0]) circle(r=tb_radius);
			}
			hull() {
				translate([-3/8*inch-edge_curve_radius,  3/4*inch+tb_radius, 0]) circle(r=tb_radius);
				translate([ 3/8*inch+edge_curve_radius,  3/4*inch+tb_radius, 0]) circle(r=tb_radius);
			}
			square([2*(3/8*inch+edge_curve_radius), 2*inch], center=true);
		}
		hull() {
			translate([-3/8*inch-edge_curve_radius, -3/4*inch+edge_curve_radius]) circle(r=edge_curve_radius);
			translate([-3/8*inch-edge_curve_radius,  3/4*inch-edge_curve_radius]) circle(r=edge_curve_radius);
		}
		hull() {
			translate([ 3/8*inch+edge_curve_radius, -3/4*inch+edge_curve_radius]) circle(r=edge_curve_radius);
			translate([ 3/8*inch+edge_curve_radius,  3/4*inch-edge_curve_radius]) circle(r=edge_curve_radius);
		}
	}
}

module gridbeam_hole(hole_depth=2*inch) {
	cylinder(d=5/16*inch, h=depth*2, center=true);
}

module counterbored_gridbeam_hole(hole_depth=2*inch, counterbore_depth=3/32*inch) {
	gridbeam_hole(hole_depth);
	cylinder(d=7/8 *inch, h=counterbore_depth*2, center=true);
}
module countersunk_gridbeam_hole(hole_depth=2*inch) {
	gridbeam_hole(hole_depth);
	translate([0,0,-0.25*inch]) cylinder(h=1*inch, r1=0, r2=1*inch);
}

difference() {
	linear_extrude(thickness, center=true) {
		rounded_rectangle([width, depth], r=3/4*inch);
	}
	//cube([width, depth, thickness], center=true);
	for( gbcy = [-3:1:3] ) {
		for( gbcx = [-3:0.5:3] ) {
			if( abs(gbcx)*1.5*inch < width/2 && abs(gbcy)*1.5*inch < depth/2 ) {
				translate([gbcx*1.5*inch, gbcy*1.5*inch, thickness/2]) {
					if( abs(gbcx) <= 0.5 && abs(gbcy) == 1 ) {
					} else if( abs(gbcx) <= 1 && abs(gbcy) <= 1 ) {
						countersunk_gridbeam_hole();
					} else {
						countersunk_gridbeam_hole();
					}
				}
			}
		}
	}
	linear_extrude(2*thickness, center=true) {
		cutout_shape();
	}
}