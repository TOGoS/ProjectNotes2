inch = 25.4;

size = 4.5*inch;
epsilon = 1;
thickness = 1.5 * inch;
min_wall_thickness = 1/2 * inch; 
counterbore_diameter = 7/8 * inch;
wall_thickness = 3/4 * inch;

slot_start = 1.5*inch;

module the_old_hull() {
	translate([0,0,-thickness/2]) {
		cube([wall_thickness, size, thickness]);
		cube([size, wall_thickness, thickness]);
	}
}

module the_hull() {
	linear_extrude(thickness, center=true) {
		polygon([
			[0,0],
			[0,size],
			[wall_thickness,size],
			[size,wall_thickness],
			[size,0],
		]);
	}
}

module edge_holes() {
	hull() {
		translate([slot_start, 0, -epsilon]) {
			cylinder(d=5/16*inch, h=size+2*epsilon);
		}
		translate([3.75*inch, 0, -epsilon]) {
			cylinder(d=5/16*inch, h=size+2*epsilon);
		}
	}
	hull() {
		translate([slot_start, 0, min_wall_thickness]) {
			cylinder(d=counterbore_diameter, h=size+epsilon-min_wall_thickness);
		}
		translate([3.75*inch, 0, min_wall_thickness]) {
			cylinder(d=counterbore_diameter, h=size+epsilon-min_wall_thickness);
		}
	}
}

difference() {
	the_hull();
	rotate([-90, 0, 0]) edge_holes();
	rotate([0, 0, 90]) rotate([90, 0, 0]) edge_holes();
}
