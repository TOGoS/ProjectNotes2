# Power over Ethernet notes

Or rather notes on how best to do non-standard passive PoE.

I'm considering using 8p8c modular jacks
(probably in an unconventional way that doesn't necessarily involve Ethernet)
for transmitting data and power into simple devices.
e.g. LED lamps with multiple lights,
or maybe some mesh networking experiment using microcontrollers.

### How complicated is standard Power over Ethernet (PoE)?

- More complicated than I want to deal with
- Requires at least 44V
- Devices are supposed to work with mode A or B
- Some stuff about resistors and signalling

### What can I get away with doing simple non-standard PoE?

- To minimize chance of frying something, don't apply voltage between wires of the same pair;
  standard PoE can power over data pins by having base voltage difference between pairs.
- PoE 'mode B' works by putting high voltage on pins 4+5 (blue; pair 1) and low on pins 7+8 (brown; pair 4),
  so if you do that you're kind of following the spirit of the standard.

### Is there a de-facto standard for non-standard \[passive\] PoE?

> The common 100 Mbit/s passive applications use the pinout of 802.3af mode B -
> with DC plus on pins 4 and 5 and DC minus on 7 and 8

-- https://en.wikipedia.org/wiki/Power_over_Ethernet#Passive

But they tend to use 24-56V to compensate for voltage drop

### Would be nice to have a simple power+data port for small devices.

For this this use case probably go with the 'passive' non-standard
and write the expected pin voltages next to the port,
maybe with a warning icon that says "this isn't Ethernet!"

I'd be using the 8P8C connectors purely for their physical convenience.
If I really do want to transmit Ethernet signals, I should have
a converter in front of the device that puts power on the blue and brown pairs.
