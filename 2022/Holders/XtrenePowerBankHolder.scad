// Try 3 for compact, 4.5 for tall with cutout
height_in_inches = 3; // [3, 4.5]

// End of customizable variables
module _() { }

// Holder for the 'Xtreme'
// USB power bank that I must have
// picked up from Menards in June 2021.
// Outer dimensions are
// 3+16/16" long x 3+1/2" wide x 1+3/16" thick

inch = 25.4;
mm = 1;

// Always centered!
module rounded_xy_z_cube(size, xy_rad, z_rad) {
	xy_rad_adjusted = min(min(size[0],size[1])/2-0.1, xy_rad);
	z_rad_adjusted = min(size[2]/2-0.1, xy_rad_adjusted-0.1, z_rad);
	minkowski() {
		sphere(r=z_rad_adjusted);
		linear_extrude(size[2]-z_rad_adjusted*2, center=true) {
			minkowski() {
				circle(r=xy_rad_adjusted-z_rad_adjusted);
				square_margin = xy_rad_adjusted;
				square([size[0]-square_margin*2,size[1]-square_margin*2], center=true);
			}
		}
	}
}
module rounded_xy_cube(size, xy_rad) {
	xy_rad_adjusted = min(min(size[0],size[1])/2-0.1, xy_rad);
	linear_extrude(size[2], center=true) {
		minkowski() {
			circle(r=xy_rad_adjusted);
			square([size[0]-xy_rad_adjusted*2,size[1]-xy_rad_adjusted*2], center=true);
		}
	}
}


module power_brick_holder(size) {
	back_panel([size[0], back_thickness, size[2]]);
	
	for( side=[-1,1] ) {
		translate([
			side * (size[0] - side_thickness)/2,
			size[1]/2, side_height/2
		]) cube([side_thickness, size[1], side_height], center=true);
	}
	
	translate([0, size[1]/2, bottom_thickness/2]) {
		cube([size[0], size[1], bottom_thickness], center=true);
	}
	
	translate([0, size[1]-front_thickness/2, side_height/2]) {
		cube([size[0], front_thickness, side_height], center=true);
	}
}

//power_brick_holder([4.5*inch, 2.25*inch, 6*inch]);

holder_size = [3*inch, 1.5*inch, height_in_inches*inch];
wall_thickness = 3/16*inch;
// I *think* that 1+1/4 will work, based on that
// the power bank fits snugly into a 1+1/2" phone holder with,
// I'm guessing, 1/8" hardboard front/back panels.
cavity_size = [(2+9/16)*inch, (1+1/4)*inch, 4*inch];
panel_thickness = (holder_size[1]-cavity_size[1])/2;
floor_thickness = 1/4*inch;
difference() {
	rounded_xy_z_cube(holder_size, 1/8*inch, 3/32*inch, $fn=30);
	// Top bay
	translate([0, holder_size[1]/2, -holder_size[2]/2+4.5*inch]) {
		rotate([00,0,90]) rotate([90,0,0]) {
			rounded_xy_cube([2*inch, 3*inch, holder_size[0]+10], 1/4*inch, $fn=30);
		}
	}
	// Main cavity
	translate([0,0,floor_thickness]) {
		rounded_xy_z_cube([
			cavity_size[0],
			cavity_size[1],
			holder_size[2]
		], wall_thickness/2, wall_thickness/2, $fn=30);
	}
	// Bottom hole
	translate([0,0,-holder_size[2]/2]) {
		rounded_xy_cube([2*inch, 1/2*inch, 1*inch], 1/8*inch, $fn=30);
	}
	// Front slot
	translate([0,holder_size[1]/2,0]) {
		cube([1/2*inch, holder_size[1], holder_size[2]+10], center=true);
	}
	// Charge indicator window
	translate([7/8*inch, holder_size[1]/2, -holder_size[2]/2+3/4*inch]) {
		rotate([90,0,0]) {
			rounded_xy_cube([3/4*inch, 1*inch, 3/4*inch], 1/8*inch, $fn=30);
		}
	}
	// Mounting holes
	for( x=[-1:1:1] ) {
		for( z=[0:1:2] ) {
			translate([x*3/4*inch, -holder_size[1]/2+panel_thickness+0.1, -holder_size[2]/2+3/4*inch+z*1.5*inch]) {
				rotate([90,0,0]) {
					cylinder(d=17/64*inch, h=panel_thickness+0.2, $fn=30);
					cylinder(d1=1/2*inch+0.2, d2=0, h=1/4*inch+0.1, $fn=30);
				}
			}
		}
	}
}