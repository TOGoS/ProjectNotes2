When tapping for #6-32 screws, use a 7/64 bit.
You'll have a much easier time tapping than you would with a 3/32 and the holes come out the same.


How to tap straight?
https://www.youtube.com/watch?v=WWjaoW7VJxY
Method 1) 'sighting'; i.e. what you already do; just eyeball it from both directions
Method 2) For thicker stuff, bore the OD partway to help guide the tap
Method 3) Using a tap level
Method 4) 'Tapping guide' - maybe I can build one?
Method 5) 'Guide block' method - make a block with an OD hole in it and C-clamp it to your thing
Method 6) Use your drill press to help line up with the hole you just made - C-lamp the object down! - requires a lot of bit swapping

Method one, meh, but method 2 doesn't apply to the thin aluminium bars I work with.


Also note that it's usually possible to tap again to straigten out a slightly crooked hole;
you'll just end up with looser threads.


Drill press method:
Mount the tap in the drill press like a regular bit,
but move the thing manually instead of running the motor!
You need to be able to move it backward every few turns,
just like if you were tapping with your hands.

Keep some downward pressure the whole time so your object doesn't climb up the bit while you tap.
Once you're through, you can spin the tap backwards and the object will eventually fall off.

I've found that as long as you keep that downward pressure,
it's enough to keep the object still, so a clamp is not strictly necessary,
but it may still be a good idea to keep it extra still.
