mm = 1;
inch = 25.4;
gbu = 0.125*inch;

//// Outer hull

box_width = 3*inch;
box_height = 3*inch;
box_thickness = 1.5*inch;
box_corner_radius = 1/8*inch; // Somewhere from 1/8 to 1/4, idk

//// Size of main power brick-holding cavity

power_brick_width = 2.375*inch;      // actually 2.3
power_brick_height = 2.375*inch;     // actually 2.3
power_brick_thickness = 1.125*inch;  // actually 1.06
power_brick_corner_radius = 4*mm;

//// Parameters for different plugs and cords

framework_stock_usb_plug_width = 0.45*inch;
framework_stock_usb_plug_thickness = 0.24*inch;

// https://electronics.stackexchange.com/a/475912
spec_max_usb_c_plug_width = 12.35*mm;
spec_max_usb_c_plug_thickness = 6.5*mm;

// https://electronics.stackexchange.com/a/507141
practical_max_usb_c_plug_width = 16*mm;
practical_max_usb_c_plug_thickness = 10*mm;

framework_stock_ac_plug_width = 0.85*inch;
framework_stock_ac_plug_thickness = 0.67*inch;
framework_stock_ac_cable_thickness = 6*mm;

// Pick something > your cable thickness
usb_front_gap_width = 1/4*inch; // framework USB cable is 3.7mm
// Pick something > your plug thickness (y)
usb_bottom_gap_width = practical_max_usb_c_plug_thickness;
// Pick something > your plug width (z)
usb_bottom_gap_depth = practical_max_usb_c_plug_width;

// Large enough for most cables, #6 screw heads, screwdriver shafts...
universal_front_gap_width = 5/16*inch;

side_wall_thickness = (box_width - power_brick_width) / 2;
bottom_wall_thickness = (box_height - power_brick_height) / 2;
back_wall_thickness = (box_thickness - power_brick_thickness) / 2;
front_wall_thickness = (box_thickness - power_brick_thickness) / 2;

//// Parameters for different mounting holes

togbeam_hole_diameter = 5/32*inch;
togbeam_hole_fn = 20;
// Should be enough for #6 screw heads (0.17") and/or screwdriver shaft:
togbeam_counterbore_diameter = 1/4*inch;
// Heads of my #6 screws are about 2mm, so:
togbeam_counterbore_depth = 2*mm;
//back_togbeam_counterbore_depth = min(1/8*inch, back_wall_thickness/2);
//side_togbeam_counterbore_depth = min(1/8*inch, side_wall_thickness/2);
togbeam_counterbore_fn = 30;

gridbeam_hole_diameter = 5/16*inch;
gridbeam_hole_fn = 40;
gridbeam_counterbore_depth = 1/8*inch;
//back_gridbeam_counterbore_depth = min(1/8*inch, back_wall_thickness/2);
//side_gridbeam_counterbore_depth = min(1/8*inch, side_wall_thickness/2);
gridbeam_counterbore_diameter = 7/8*inch;
gridbeam_counterbore_fn = 60;

function adjusted_counterbore_depth(preferred_depth, wall_thickness) = min(preferred_depth, back_wall_thickness-1/8*inch);

module y_axis_rounded_corner_subtrahend(radius, length, xside, zside) {
	difference() {
		cube([radius*2, length, radius*2], center=true);
		translate([-xside*radius, 0, -zside*radius])
			rotate([90,0,0])
				cylinder(h=length, r=radius, center=true);
	}
}

module minimum_power_brick_cavity() {
	cube([power_brick_width, power_brick_thickness, power_brick_width], center=true);
}

module tall_power_brick_cavity() {
	difference() {
		cube([power_brick_width, power_brick_thickness, box_height], center=true);
		for(corner = [[-1,-1],[1,-1]] ) {
			translate([corner[0]*power_brick_width/2, 0, corner[1]*box_height/2]) {
				y_axis_rounded_corner_subtrahend($fn=40, power_brick_corner_radius, power_brick_thickness*2, corner[0], corner[1]);
			}
		}
	}
}

module box() {
	difference() {
		cube([box_width, box_thickness, box_height], center=true);
		for(corner = [[-1,-1],[1,-1],[-1,1],[1,1]] ) {
			translate([corner[0]*box_width/2, 0, corner[1]*box_height/2]) {
				y_axis_rounded_corner_subtrahend($fn=40, box_corner_radius, box_thickness*2, corner[0], corner[1]);
			}
		}
	}
}

module base_box() difference() {
	box();
	translate([0, 0, side_wall_thickness]) tall_power_brick_cavity();
}

//// Hole positions

x_axis_togbeam_hole_positions = [
	// 1/2" grid
	[0,  10*gbu],
	[0,   6*gbu],
	[0,   2*gbu],
	[0,  -2*gbu],
	[0,  -6*gbu],
	//[0, -10*gbu],
	
	// 3/4" grid
	[-3*gbu,  9*gbu],
	[+3*gbu,  9*gbu],
	//[-3*gbu,  3*gbu],
	//[+3*gbu,  3*gbu],
	//[-3*gbu, -3*gbu],
	//[+3*gbu, -3*gbu],
	//[-3*gbu, -9*gbu],
	//[+3*gbu, -9*gbu],
];

x_axis_intergridbeam_togbeam_hole_positions = [
	// 1/2" grid
	[0,  10*gbu],
	[0,   6*gbu],
	[0,   2*gbu],
	[0,  -2*gbu],
	[0,  -6*gbu],
	//[0, -10*gbu],
	
	// 3/4" grid
	[-3*gbu,  9*gbu],
	[+3*gbu,  9*gbu],
	[-3*gbu,  3*gbu],
	[+3*gbu,  3*gbu],
	[-3*gbu, -3*gbu],
	[+3*gbu, -3*gbu],
	//[-3*gbu, -9*gbu],
	//[+3*gbu, -9*gbu],
];

// 1/2" grid
y_axis_togbeam_hole_positions = [
	[ -6*gbu,+10*gbu],
	[      0,+10*gbu],
	[ +6*gbu,+10*gbu],
	[ -6*gbu, +6*gbu],
	[      0, +6*gbu],
	[ +6*gbu, +6*gbu],
	[ -6*gbu, +2*gbu],
	[      0, +2*gbu],
	[ +6*gbu, +2*gbu],
	[ -6*gbu, -2*gbu],
	[      0, -2*gbu],
	[ +6*gbu, -2*gbu],
	[ -6*gbu, -6*gbu],
	[      0, -6*gbu],
	[ +6*gbu, -6*gbu],
];
	
// 3/4" grid
y_axis_togbeam_3q_hole_positions = [
	//[-9*gbu, +9*gbu],
	[-3*gbu, +9*gbu],
	[+3*gbu, +9*gbu],
	//[+9*gbu, +9*gbu],
	//[-9*gbu, +3*gbu],
	[-3*gbu, +3*gbu],
	[+3*gbu, +3*gbu],
	//[+9*gbu, +3*gbu],
	//[-9*gbu, -3*gbu],
	[-3*gbu, -3*gbu],
	[+3*gbu, -3*gbu],
	//[+9*gbu, -3*gbu],
	// -9 cuts through the bottom, which is ugly
	//[-9*gbu, -9*gbu],
	//[-3*gbu, -9*gbu],
	//[+3*gbu, -9*gbu],
	//[+9*gbu, -9*gbu],
];

y_axis_intergridbeam_togbeam_hole_positions = [
	[ 0*gbu, 10*gbu],
	[ 0*gbu,  6*gbu],
	[ 0*gbu, -6*gbu],
	//[ 0*gbu,-10*gbu],
];

// y = front-to-back, because that's how OpenSCAD shows it
// [x,z]
y_axis_gridbeam_hole_positions = [
	[-6*gbu,+6*gbu],
	[+6*gbu,+6*gbu],
	[ 0*gbu, 0*gbu],
	[-6*gbu,-6*gbu],
	[+6*gbu,-6*gbu],
];

// [y,z]
x_axis_gridbeam_hole_positions = [
	[0,-6*gbu],
	[0,+6*gbu],
];
// [x,y]
z_axis_gridbeam_hole_positions = [
	[-6*gbu,0],
	[+6*gbu,0],
];

module x_axis_hole(hole_diameter, counterbore_diameter, cbdepth, hole_fn=$fn, counterbore_fn=$fn) {
	rotate([0,90,0]) {
		cylinder(
			center=true,
			$fn=hole_fn,
			d=hole_diameter,
			h=box_width+1
		);
		cylinder(
			center=true,
			$fn=counterbore_fn,
			d=counterbore_diameter,
			h=box_width-side_wall_thickness*2+adjusted_counterbore_depth(gridbeam_counterbore_depth, side_wall_thickness)*2
		);
	}
}

module y_axis_hole(holediam, cbdiam, cbdepth, hole_fn=$fn, counterbore_fn=$fn) {
	rotate([90,0,0]) {
		cylinder(
			$fn=hole_fn,
			h=box_thickness+1,
			d=holediam,
			center=true
		);
		translate([0,0,-box_thickness/2+back_wall_thickness-cbdepth]) {
			cylinder(
				$fn=counterbore_fn,
				h=box_thickness+1,
				d=cbdiam,
				center=false);
		}
	}
}

module y_axis_gridbeam_hole() {
	y_axis_hole(
		gridbeam_hole_diameter,
		gridbeam_counterbore_diameter,
		adjusted_counterbore_depth(gridbeam_counterbore_depth, back_wall_thickness),
		hole_fn=gridbeam_hole_fn,
		counterbore_fn=gridbeam_counterbore_fn
	);
}

module x_axis_togbeam_hole() {
	x_axis_hole(
		togbeam_hole_diameter,
		togbeam_counterbore_diameter,
		adjusted_counterbore_depth(togbeam_counterbore_depth, back_wall_thickness),
		hole_fn=togbeam_hole_fn,
		counterbore_fn=togbeam_counterbore_fn
	);
}

module y_axis_togbeam_hole() {
	y_axis_hole(
		togbeam_hole_diameter,
		togbeam_counterbore_diameter,
		adjusted_counterbore_depth(togbeam_counterbore_depth, back_wall_thickness),
		hole_fn=togbeam_hole_fn,
		counterbore_fn=togbeam_counterbore_fn
	);
}


module x_axis_gridbeam_hole() {
	rotate([0,90,0]) {
		cylinder(
			center=true,
			$fn=gridbeam_hole_fn,
			d=gridbeam_hole_diameter,
			h=box_width+1
		);
		cylinder(
			center=true,
			$fn=gridbeam_counterbore_fn,
			d=gridbeam_counterbore_diameter,
			h=box_width-side_wall_thickness*2+adjusted_counterbore_depth(gridbeam_counterbore_depth, side_wall_thickness)*2
		);
	}
}

module z_axis_hole(holediam, cbdiam, cbdepth, hole_fn=$fn, counterbore_fn=$fn) {
	rotate([0,0,0]) {
		cylinder(
			center=true,
			$fn=hole_fn,
			d=holediam,
			h=box_height+1
		);
		
		translate([0,0,bottom_wall_thickness-cbdepth]) {
			cylinder(
				center=true,
				$fn=counterbore_fn,
				d=cbdiam,
				h=box_height
			);
		}
	}
}

module z_axis_gridbeam_hole() {
	z_axis_hole(
		gridbeam_hole_diameter,
		gridbeam_counterbore_diameter,
		adjusted_counterbore_depth(gridbeam_counterbore_depth, side_wall_thickness),
		hole_fn=gridbeam_hole_fn,
		counterbore_fn=gridbeam_counterbore_fn
	);
}

//// Things to be subtracted from the base box

module front_slot(width) {
	translate([0, -box_thickness/2, 0]) {
		cube([width, box_thickness, box_height+1], center=true);
	}
}
module bottom_center_hole(width, depth, r=0.1) {
	translate([0, 0, -box_height/2]) {
		rounded_extruded_rect(width, depth, bottom_wall_thickness*3, r);
		//cube([width, depth, bottom_wall_thickness*3], center=true);
	}	
}

module usb_slots() {
	front_slot(universal_front_gap_width);
	bottom_center_hole(usb_bottom_gap_width, usb_bottom_gap_depth, r=1.5*mm, $fn=20);
}

module ac_cable_slots() {
	front_slot(framework_stock_ac_cable_thickness+1);
	bottom_center_hole(framework_stock_ac_cable_thickness+1, framework_stock_ac_plug_width+1, r=1*mm, $fn=20);
}

module gridbeam_back_holes() {
	for(pos = y_axis_gridbeam_hole_positions) {
		translate([pos[0], 0, pos[1]]) {
			y_axis_gridbeam_hole();
		}
	}
}

module intergridbeam_togbeam_side_holes() {
	for( pos = x_axis_intergridbeam_togbeam_hole_positions) {
		translate([0, pos[0], pos[1]]) {
			x_axis_togbeam_hole();
		}
	}
}

module gridbeam_side_holes() {
	for( pos = x_axis_gridbeam_hole_positions) {
		translate([0, pos[0], pos[1]]) {
			x_axis_gridbeam_hole();
		}
	}
}

module gridbeam_bottom_holes() {
for( pos = z_axis_gridbeam_hole_positions) {
		translate([pos[0], pos[1], 0]) {
			z_axis_gridbeam_hole();
		}
	}
}

module gridbeam_holes() {
	gridbeam_back_holes();
	gridbeam_side_holes();
	gridbeam_bottom_holes();
}

module togbeam_back_holes() {
	for( pos = y_axis_togbeam_hole_positions ) {
		translate([pos[0], 0, pos[1]]) y_axis_togbeam_hole();
	}
}

module togbeam_3q_back_holes() {
	for( pos = y_axis_togbeam_3q_hole_positions ) {
		translate([pos[0], 0, pos[1]]) y_axis_togbeam_hole();
	}
}

module intergridbeam_togbeam_back_holes() {
	for( pos = y_axis_intergridbeam_togbeam_hole_positions ) {
		translate([pos[0], 0, pos[1]]) y_axis_togbeam_hole();
	}
}

module togbeam_side_holes() {
	for( pos = x_axis_togbeam_hole_positions ) {
		translate([0, pos[0], pos[1]]) x_axis_togbeam_hole();
	}
}

module rounded_extruded_rect(w, d, h, r) {
	hull() {
		translate([ (w/2)-r,  (d/2)-r, 0]) cylinder(h=h, r=r, center=true);
		translate([-(w/2)+r,  (d/2)-r, 0]) cylinder(h=h, r=r, center=true);
		translate([ (w/2)-r, -(d/2)+r, 0]) cylinder(h=h, r=r, center=true);
		translate([-(w/2)+r, -(d/2)+r, 0]) cylinder(h=h, r=r, center=true);
	}
}

module side_plug_holes() {	
	rotate([0,90,0]) rounded_extruded_rect($fn=30, w=3/4*inch, d=7/8*inch, h=box_width+1, r=1/16*inch);
	//cube([box_width*2, 7/8*inch, 3/4*inch], center=true);
}

//// Some configurations

module gridbeamed_box() {
	difference() {
		base_box();
		usb_slots();
		gridbeam_holes();
		intergridbeam_togbeam_back_holes();
		intergridbeam_togbeam_side_holes();
	}
}

module togbeamed_box() {
	difference() {
		base_box();
		usb_slots();
		side_plug_holes();
		//gridbeam_back_holes();
		togbeam_side_holes();
		gridbeam_bottom_holes();
		togbeam_back_holes();
		togbeam_3q_back_holes();
	}
}

translate([-2.25*inch, 0, 0]) gridbeamed_box();
translate([ 2.25*inch, 0, 0]) togbeamed_box();