// Unit used for *_bsunit
bsunit = "1/8*inch"; // ["mm", "inch", "1/8*inch", "1/16*inch", "1/32*inch", "1/64*inch", "inval"]
// Size of one small block
sblock_size_bsunits = 4;
// Size of one large block
lblock_size_bsunits = 12;

width_sblocks = 4;
height_sblocks = 6;

thickness = 3.175;
corner_rad = 3.175;

include <PhoneHolderLib.scad>;

bsunit_size = unit_value(bsunit);
sblock_size = bsunit_size * sblock_size_bsunits;
lblock_size = bsunit_size * lblock_size_bsunits;
width  = width_sblocks*sblock_size;
height = height_sblocks*sblock_size;

hole_positions = "edges"; // ["none", "all", "edges", "top+bottom", "sides"]
window_layout = "none"; // ["none", "xtreme"]
hull_shape = "rectangle"; // ["rectangle", "corner-cut"]

// Set x_scale to -1 to flip overall shape horizontally
x_scale = 1; // [1,-1];

module topside(thickness) {
	translate([0,0,thickness/2]) children();
}
module bottomside(thickness) {
	translate([0,0,-thickness/2]) rotate([180,0,0]) children();
}

module rounded_corner_cut_rectangle(size, corner_size, r) {
	y0 = -size[1]/2+r;
	y1 = +size[1]/2-r;
	x0 = -size[0]/2+r;
	x1 = +size[0]/2-r;
	point_positions = [
		[x0, y0],
		[x1, y0],
		[x1, y1-corner_size],
		[x1-corner_size, y1],
		[x0, y1]
	];
	if( r > 0 ) {
		hull() for( pos=point_positions ) translate(pos) circle(r);
	} else {
		assert(false, "0 corner radius not currently implamenented for rounded corner cut rect");
	}
}

module phfp_hull(size, corner_rad=corner_rad, shape) {
	if( shape == "rectangle" ) {
		rounded_rectangle(tovec2(size), r=corner_rad, $fn=20);
	} else if( shape == "corner-cut" ) {
		rounded_corner_cut_rectangle(tovec2(size), r=corner_rad, corner_size=sblock_size*2.25, $fn=20);
	} else {
		assert(false, str("Invalid phone holder front panel hull shape: '", shape, "'"));
	}
}

module phone_holder_front_panel(size, corner_rad=corner_rad, hole_positions="all", window_layout="none", hull_shape="rectangle") {
	thickness = size[2];
	difference() {
		linear_extrude(thickness, center=true) {
			difference() {
				phfp_hull(size, corner_rad=corner_rad, shape=hull_shape);
				if( window_layout == "none" ) {
				} else if( window_layout == "xtreme" ) {
					translate([0,-sblock_size]) rounded_rectangle(tovec2(sblock_size*2), corner_rad, $fn=20);
					if( hull_shape == "rectangle" ) {
						translate([0,sblock_size*1.5]) rounded_rectangle([sblock_size*2, sblock_size], corner_rad, $fn=20);
					} else {
						translate([-sblock_size/2,sblock_size*1.5]) rounded_corner_cut_rectangle(
							[sblock_size, sblock_size],
							r=corner_rad,
							corner_size=sblock_size/2,
							$fn=20);
					}
				}
			}
		}
		
		hole_positions1 = resolve_grid_positions(hole_positions, size, sblock_size);
		
		for( pos=hole_positions1 ) {
			translate(pos) topside(thickness) small_countersunk_hole(thickness*1.5);
		}
	}
}

scale([x_scale, 1]) phone_holder_front_panel(
	size=[width,height,thickness],
	hole_positions=hole_positions,
	window_layout=window_layout,
	hull_shape=hull_shape
);
