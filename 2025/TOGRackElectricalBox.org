#+TITLE: TOGRack enclosure / electrical box unification

Related to [[../2022/Projects.tef::WSPROJECT-200769][WSPROJECT-200769]], 'Prototype TOGRack-mounted outlets'.

[[../2023/3DPrinting/3DPrintingData.tef::p1404][p1404]] showed that 1 2"x4.5"x1.5" 3D-printed box can comfortably hold
a standard outlet (with not much room left over for wires).

[[../2023/3DPrinting/3DPrintingData.tef::p1798][p1798]] is a design for a 4.5" TOGRack2 box (with a 1/16" TOGridPile lip,
making it slightly taller than p1404).

Measurements of an orange 2-gang open-backed wall...non-box:
- Y distance between outlet top/bottom hole :: 3+1/4"
- X distance between holes for adjacent outlets :: 46mm
  - Important if you want to use pre-made wall plates
- Y distance between outer holes :: 96mm (just over 3+3/4")
  - This is an additional pair of holes above/below the ones to which
    the outlet screws, presumably to accomodate screws that poke
    through certain swiches and their face plates
- Y distance across hole :: 72mm
- Y distance across the whole orange wall non-box :: 4+5/16"

*** [2025-03-05T18]: An idea

WSTYPE-4140-4.5, but the rails are a bit inset (maybe by 1/8")
as if you're going to use really thick panels.

To attach TOGRack panels, put spacers between
the panels and the rails (if needed).

To attach outlets, use an adapter.
