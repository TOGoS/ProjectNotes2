$fn = 50;

function mm(millimeters) = millimeters;
function inches(inc) = mm(25.4 * inc);
function pvunits(hole_count) = mm(2.54 * hole_count);
// For 3D printing stuff, Dad says make thickness a multiple of 0.3mm.
// Width is less important but a multiple of 0.42mm might work best.
function extruded_layers(layer_count) = mm(0.3 * layer_count);
function extruded_beads(bead_count) = mm(0.42 * bead_count);

function unit_ceil(unit, value) =
    ceil(value / unit) * unit;

// Let's say wires going along z,
// so connection interface is the x/y plane.

// Using https://www.amazon.com/dp/B01NAB4GT6 as guide,
// a connector is 14mm long, 2.54 thick, and up to 0.1mm longer
// wider than you'd expect (e.g. their 1p is listed as 2.64mm wide,
// while their 2p is exactly 2x2.54mm = 5.08mm).  *shrug*
// The pins themselves are about 0.65mm square

connector_length = mm(14);
bulkhead_thickness = extruded_layers(2);
margin = mm(0.2);

outer_box_height = 0;

module bulkhead(columns, rows) {
    difference() {
        cube([
            pvunits(columns)+margin+0.05,
            pvunits(rows)+margin+0.05,
            bulkhead_thickness
        ], true);
        // Hole big enough for pins to go through, but that blocks
        // connector housing
        cube([
            pvunits(columns - 1) + mm(1.5),
            pvunits(rows - 1) + mm(1.5),
            1,
        ], true);
    }
}

sleeve_length = connector_length + bulkhead_thickness;
pin1_marker_size = mm(1);

function sleeve_cavity_width(columns) = pvunits(columns) + margin*2;
function sleeve_cavity_diagonal(columns,rows) =
    sqrt(pow(pvunits(columns) + margin*2, 2) +
         pow(pvunits(rows) + margin*2, 2));

module sleeve_cavity(columns, rows, sleeve_length=connector_length*2) {
    hole_width = sleeve_cavity_width(columns);
    hole_height = sleeve_cavity_width(rows);

    difference() {
        cube([hole_width,hole_height,sleeve_length + 1], true);
        bulkhead(columns, rows);
    }
}

module rectangular_sleeve(
    columns, rows,
    sleeve_wall_thickness = extruded_beads(3),
    outer_box_width = 0,
    outer_box_height = 0,
    flange_thickness = 0
) {
    hole_width = sleeve_cavity_width(columns);
    hole_height = sleeve_cavity_width(rows);
    inner_box_width = hole_width + sleeve_wall_thickness * 2;
    inner_box_height = hole_height + sleeve_wall_thickness * 2;
        
    flange_width = max(outer_box_width, inner_box_width);
    flange_height = max(outer_box_height, inner_box_height);
    
    //cube([
    difference() {
        union() {
            cube([inner_box_width,inner_box_height,sleeve_length], true);
            
            translate([0, 0, 0 - sleeve_length/2 + flange_thickness/2]) {
                cube([flange_width, flange_height, flange_thickness], true);
            }
            translate([0, 0, 0 + sleeve_length/2 - flange_thickness/2]) {
                cube([inner_box_width, flange_height, flange_thickness], true);
            }
            cube([inner_box_width, flange_height, flange_thickness], true);
        }
        sleeve_cavity(columns, rows, sleeve_length=sleeve_length);
        translate([pvunits(columns-1)/2, pvunits(rows)/2+pin1_marker_size, -sleeve_length/2]) {
            sphere(d=pin1_marker_size);
        }
    };
}

module round_sleeve(
    columns, rows,
    diameter=0,
    flange_diameter=0,
    flange_thickness=extruded_layers(3)
) {
    diameter = unit_ceil(inches(1/16), sleeve_cavity_diagonal(columns, rows)+extruded_beads(2));
    echo(columns, "x", rows, " peg is ", (diameter / inches(1/16)), " sixteenths of an inch in diameter");
    
    difference() {
        union() {
            cylinder(d=diameter, h=sleeve_length, center=true);
            translate([0, 0, 0 - sleeve_length/2 + flange_thickness/2]) {
                cylinder(d=flange_diameter, h=flange_thickness, center=true);
            }
        }
        sleeve_cavity(columns, rows, sleeve_length=sleeve_length);
        translate([pvunits(columns-1)/2, pvunits(rows)/2+pin1_marker_size, -sleeve_length/2]) {
            sphere(d=pin1_marker_size);
        }
    }
}

function sample_spacing(samps) = inches(samps);
function sample_position(sx,sy) = [sample_spacing(sx), sample_spacing(sy), 0];

module variations() {
	 /*
    translate(sample_position(-0.5,-0.5)) {
        rectangular_sleeve(4,1,
            outer_box_width = 0,
            outer_box_height = inches(1/4),
            flange_thickness=extruded_layers(3));
    }
	 */
    translate(sample_position(-0.5,+0.5)) {
        rectangular_sleeve(4,1,
            outer_box_width = inches(3/4),
            outer_box_height = inches(1/4),
            flange_thickness=extruded_layers(3));
    }
	 /*
    translate(sample_position(0.5,-0.5)) {
        round_sleeve(4,1);
    }
	 */
    translate(sample_position(0.5,+0.5)) {
        round_sleeve(4,1,
            flange_diameter=inches(3/4),
            flange_thickness=extruded_layers(3)
        );
    }
}

variations();

//bulkhead(4,1);
