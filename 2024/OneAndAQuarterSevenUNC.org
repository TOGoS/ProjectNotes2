#+TITLE: 1+1/4"-7 UNC threads

This is here because I am considering using 1+1/4"-7 UNC
as a standard thread for panel-mounted components,
like sensorts to attach to [[./Terrarium/Terrarium.org][terrarium]] lids.

** Spec

Source: https://www.machiningdoctor.com/threadinfo/?tid=138

| Nominal Diameter           |   1.25 |  31.75 |
| Pitch (TPI)                |      7 |        |
| Pitch (Distance)           | 0.1429 |   3.63 |
| Pitch Diameter             | 1.1572 | 29.393 |
| Minor Diameter             | 1.0799 | 27.429 |
| Thread Height              | 0.0773 |  1.963 |
| Addendum                   | 0.0464 |  1.179 |
| Crest (External)           |        |        |
| Root (Internal)            | 0.0179 |  0.455 |
| Tap Drill Size (75% depth) |  1.111 |  28.21 |
| Lead Angle                 |        |        |
| (Single Start)             |  2.25° |        |

Effective dimensions for external threads:

| es         | Allowance                    | 0.0021 (Ref)    | 0.053 (Ref)     |
| d          | Major Diameter               | 1.2233 - 1.2479 | 31.072 - 31.697 |
| d2         | Pitch Diameter               | 1.1446 - 1.1551 | 29.073 - 29.34  |
| d3         | Minor Diameter               |                 |                 |
| (UNR Only) | 1.0778 (Ref)                 | 27.376 (Ref)    |                 |
| α          | Varation of 30° anngle (+/-) | 0.75°           |                 |
| P          | Varation of pitch (+/-)      | 0.003 (Ref)     | 0.076 (Ref)     |

For internal threads:

| D1 | Minor Diameter               | 1.095 - 1.123   | 27.813 - 28.524 |
| D2 | Pitch Diameter               | 1.1572 - 1.1708 | 29.393 - 29.738 |
| D  | Major Diameter               | 1.2500 (Min)    | 31.75 (Min)     |
| α  | Varation of 30° anngle (+/-) | 0.75°           |                 |
| P  | Varation of Pitch (+/-)      | 0.0039 (Ref)    | 0.099 (Ref)     |

These are both the looser tolerances which that page calls '1A' and '1B'.
