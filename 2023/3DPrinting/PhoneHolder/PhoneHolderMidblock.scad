thickness = 31.75;
corner_rad = 2;
// 5 (or slightly larger) for heat-set inserts; 3 for sheet metal screws
hole_diameter = 5;

length_blocks = 6;

module __no_more_customization() { }

include <PhoneHolderLib.scad>;

block_size = 1/2*inch;

length = length_blocks * block_size;

difference() {
	linear_extrude(thickness, center=true) {
		rounded_rectangle([length, block_size], r=corner_rad, $fn=20);
	}
	for( x=[-length/2+block_size/2 : block_size : +length/2-block_size/2] ) {
		translate([x, 0, 0]) cylinder(d=hole_diameter, h=thickness*2, center=true, $fn=20);
	}
}
