$fn = 30;

panel_xpitch = 3.5;
panel_ypitch = 3;
panel_margin = 1/16;
panel_width = panel_xpitch - panel_margin*2;
panel_height = panel_ypitch - panel_margin*2;
big = 10;
screw_hole_diameter = 5/32;

module screw_hole() {
	cylinder(d=screw_hole_diameter, h=big, center=true);
}

module panel(panel_height=3, panel_thickness=3/16) {
	chamfer = 1/8;
	xf = panel_width/2;
	xn = xf - chamfer;
	yf = panel_height/2;
	yn = yf - chamfer;
	difference() {
		linear_extrude(height=panel_thickness, center=true) {
			polygon([
				[+xn,+yf],
				[+xf,+yn],
				[+xf,-yn],
				[+xn,-yf],
				[-xn,-yf],
				[-xf,-yn],
				[-xf,+yn],
				[-xn,+yf]
			]);
		}
		
		holecount = ceil(panel_height);
		for( i=[0:holecount-1] ) {
			translate([-3/2, i+0.5-holecount/2, 0]) screw_hole();
			translate([+3/2, i+0.5-holecount/2, 0]) screw_hole();
		}
	}
	
	//square([panel_width,panel_height], true);
}

camera_base_diameter = 2+3/8;
camera_screw_hole_distance = camera_base_diameter/2 - 1/4;

module camera_mount_holes() {
	for( i=[0:2] ) {
		rotate([0,0,120*i]) translate([camera_screw_hole_distance,0,0]) screw_hole();
	}
}

module camera_mount_panel() {
	difference() {
		panel(panel_height=panel_height);
		
		// 30 is good, too, but 15 is nice and asymmetrical.
		rotate([0,0,0]) camera_mount_holes();
		cylinder(d=3/4, h=big, center=true);
	}
}

scale(24.5) {
	projection() {
		camera_mount_panel();
	}
}
