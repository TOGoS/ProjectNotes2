# Bin and Shelf Dimensions

Maxit black plastic shelves from Menards:
- I think mine are the 36" wide x 72" tall x 18" deep ones
- 17+1/2" vertical pitch
- Shelves are 2" thick
- 15+1/2" between shelves
- 31" between legs
- To secure to wall:
  - Placing board behind at 64" works, or subtract any multiple of 17.5".  Or really multiples of 16" work, too.

Black bins from ShopKo?

Tough Box bins (black with yellow lids) from Menards:
??? - but they fit perfectly on the plastic shelves
- Is this the same as Centrex Plastics 27-gallon Tough Box?
  - 27 gallons, 30+15/16" long, 20+5/16" wide, 14+9/16" tall
  - could very well be; the dimensions would fit snugly on said shelves

Sara's purple bins:
- 16" tall
- 22+1/2" long
- 17" wide

Small cat litter bins
- 11" tall
- 12" long
- 10" wide
- When stacked, each bin adds 10+1/4"

Tall cat litter bins
- 14+3/4" tall
- 12" long
- 10" wide
- When stacked, each bin adds 14+1/4"

Clear rectangular bins (from either Wal-Mart or Target):
- Rounded corners, but more nearly rectangular than e.g. 2 of the ones of a similar size that I use as cat litter boxes
- 23" long
- 16+1/2" wide
- 12+3/8" tall

Space between joists in basement ceiling:
- Usually 14.5" wide, but sometimes 14", sometimes 15".
- Just over 7" tall, like about 7+1/8"
