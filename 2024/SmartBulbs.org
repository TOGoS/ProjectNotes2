#+TITLE: Smart Bulbs, Switches, Zigbee Stuff, Etc

#+begin_quote
Zigbee is a lot more reliable than wifi (2.4g band), so I haven't had
any issues yet with just using the [Phillips Hue] hub

You can just connect the hue stuff to zigbee, so hopefully that means
they'll still work if they stop making things?

The internet says that you might lose "advanced" features that are
only available in the hue app like dynamic scenes, gradient strip control,
and motion sensor sensitivity adjustment,
but otherwise they'll continue working fine with generic zigbee

Looks like the motion sensitivity option is actually available through
zigbee. But yeah, I'm not too worried about any future lack of support
causing major issues

Philips makes switches that will work directly with the bulbs and
smart plugs, like this dimmer switch:
https://www.amazon.com/gp/product/B08W8GLPD5
I personally use the lutron remotes because they have 5 buttons that
can be programmed and they're slightly cheaper and I have home
assistant to connect everything, so I might as well

Lutron/philips/home assistant all work together without internet as long as the wifi is still working

Lutron does use a proprietary connection so that requires a hub

It gets a little complicated when you're trying to link multiple
brands together, but I like the extra control it gives me, and then I
can use lutron switches that are wired in which philips hue doesn't
have, and I can use philips hue smart bulbs which lutron doesn't have

I'd say that if you want to do any smart home stuff, try using philips
hue with a generic zigbee stick with home assistant if you want to be
able to maintain full control when there's no wifi connection (the hue
dimmers and such would also work), or go with the hue products + hub
without home assistant if you don't care about needing wifi. Those
would be the 2 simplest, least frustrating ways to do smart home stuff
I think

You can always add home assistant and other products later on if you
want to make better automations and to mix and match products

Or just use lutron if you don't care about the bulb color temperatures
because then you get wired in switches that will always manually work
without needing extra products
#+end_quote
(Renee, [2024-11-25])

#+begin_quote
SONOFF Zigbee 3.0 USB Dongle... https://www.amazon.com/dp/B0B6P22YJC
#+end_quote
(Renee, [2024-11-27])
