#+TITLE: Which side should be the top?

On the WeMos D1s (I think what I have are called "WeMos D1 mini")
the built-in LED is on the side with the chip and the antenna.
So I guess that should be considered the 'top'.

So when I add headers, the male headers should go on the bottom,
and the female headers should go on the top.

This orientation also matches the part this guy made for KiCAD:

https://github.com/rubienr/wemos-d1-mini-kicad
