#+TITLE: Where should I put holes in studs to support rando wall/ceiling shelves?

I want to have standard holes for mounting shelves on walls.
Specifically, above door frames.

Options:
- Macro-TOGRack - holes on 16"/8"/4"/2" grid (usually 16" horizontally)
- GridBeam - holes on a 1.5" grid (but horizontally probably more like 16" if going into studs)

Thoughts:
- When going into studs, it may make less sense to try to do GridBeam
  holes, since I can't control the horizontal span.
  Instead, Macro-TOGRack holes provide a place to mount *something*, to which my shelves could be attached.
  But I don't really like the idea of having to have another board between the walls and the shelves.
- The height of the ceilings in my house vary a little bit.
  So maybe it's better to pick an arbitrary line (such as the tops of the door frames)
  and base heights on that.
- Any shelves I build will probably need to provide some wiggle room horizontally,
  since I can't always control horizontal spacing of holes.
  This could mean holes become horizontal slots in some cases.
  I'd prefer they not have to have also have vertical wiggle room.
- Given that, maybe I should try to keep things as gridbeam-compatible as possible
  (so focus on 1/4" gridbeam holes instead of TOGRack ones)
- This leaves the question open of whether I should measure from the floor, ceiling, or door frame tops.
  For the purposes of these shelves, door frame tops makes the most sense.
  Would having holes 
- Is it more important to me to mash shelves up against the ceiling, or against the tops of door frames?

Since there are different approaches I might take

- V1: Mish-mash of a couple differnt things - ended up being kind of 'worst of all worlds'
  solution based on trying to provide 
- V2: Measure from 'ceiling'
- V3: Measure from door frame?
