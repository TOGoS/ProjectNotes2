# Questions on cryptocoin (primarily Ethereum) mining

## What hardware do we need to mine Ethereum?

### Notes on Joh's video

Joh likes this video in which a guy who probably has a desert full of solar panels in his backyard:
https://www.youtube.com/watch?v=1pFrgg2J_JQ

Looks like he's using some big long server power supply?

Looks like he's plugging the power on top of the cards into something on the riser.

Maybe this can help explain about power connectors to graphics cards on risers:
http://www.gobitgo.com/articles/1001/How-To-Correctly-Use-and-Install-PCI-E-Riser-Cables/

And maybe this can help explain about server power supplies:
https://linustechtips.com/main/topic/571503-what-standards-do-server-power-supplies-use/

Okay it didn't, actually.  So I just went ahead and ordered
[HSTNS-PL18](https://www.ebay.com/itm/HP-HSTNS-PL18-750W-Server-Power-Supply-506821-001-506822-201-511778-001/201650125897) and a
[breakout board](https://www.ebay.com/itm/322631409862)

> I think I figured it out.
> He's got some cable with both a 6-pin connector for the riser and an 8-pin connector for the card.
> Which should be fine because as I understand it the riser itself doesn't use much power
> since the card draws most of it through the 8-pin power port on top.

-- Dan, 2017-01-07

### Graphics cards?

> It looks like the 1070 could potentially be the best for the money
> because you get a hash rate of about 30. It uses about 150w compared
> to 120w for a 1060.  The amd Radeon cards can be bought slightly
> cheaper, but have the same or slightly lower performance and use
> 180w ish

-- Renee

> I would get the ones renee mentioned if we can,
> but maybe they are hard to find and I did like this article about the 1050
> and 1050 ti because it had lots of statistics and numbers:
> - http://1stminingrig.com/nvidia-geforce-gtx-1050-1050ti-mining-performance-review/
> - http://www.tomshardware.com/news/ethereum-effect-graphics-card-prices,34928.html
> - https://themerkle.com/top-6-graphics-cards-to-mine-ethereum-with/
> 
> [Dan's 1050] might be able to mine zcash.
> It could be a good idea to have a small mining rig
> that mines all of the obscure types of coins
> just in case one of them becomes worth something someday.

-- Joh

> For Ethereum we may need graphics cards with over 3GB of memory.
> It is supposedly going to increase to 4GB soon.
> However Zcash looks like it can be mined with pretty much anything.

-- Joh

We're going with GTX 1070s

- 1 x [GTX 1070 TI bought 2018-01-05 from Newegg](https://www.newegg.com/Product/Product.aspx?Item=N82E16814126225) - whoops this order didn't go through!
- 3 x [PNY GTX 1070s from BestBuy via eBay](https://www.ebay.com/itm/PNY-NVIDIA-GeForce-GTX-1070-8GB-GDDR5-PCI-Express-3-0-Graphics-Card-Black/322743184163?hash=item4b24fe2b23:g:~YYAAOSwbiFZQxro)

### Power supply?

> A possibility is to use server power supplies to separately power the graphics cards.
> Doing this does also require some sort of breakout board I think.
> Server power supplies are less expensive, about $90 for a 1200 watt one.
> They cannot power the motherboard and processor, but can power graphics cards.
> This would mean we could use a regular 400-600 watt power supply for the
> motherboard/processor/everything else except the graphics cards and this would save us money.
> It could help us avoid having to pay $500 or more for an expensive higher end consumer grade 1200-1600 watt modular power supply.

-- Joh

https://www.reddit.com/r/EtherMining/comments/6cx30t/stop_wasting_your_money_on_expensive_power/

### Motherboard

One with lots of PCI-e ports that won't fry itself.

### CPU

The work is all done on the GPUs,
so we went with the most inexpensive CPU that would work with the board (AM4 socket).

Ordered an Athlon X4.

### What's the difference between a PCI-e riser vs extender?

There seems to be some overlap in terminology,
but extenders seem to just be minimal wires,
whereas risers give you a board to mount on and
include power ports.

> In my opinion we definitely need risers, not extenders.

-- Joh

### How are risers (and the cards on them) powered?

Seems risers and the cards on them need to be powered separately.
Different risers have different power connectors.
Some have molex, some have SATA, some have 6-pin (same as 6-pin PCI-e power cable?)

[A page about power connectors](http://www.overclock.net/a/gpu-and-cpu-power-connections) and
![A relevant image](PCIePower.png)

It looks like the PCIe power connector pinout is the same at the power supply end
(for modular power supplies) and the graphics card end.

> Nver ever use thos sata to molex cables , thay arnt designed for
> such wattage and current , simply put they are fire hazard. Always
> use direct cables from psu to molex and preferably not more than 2
> raisers on one cable

-- [mo35](https://forum.z.cash/t/puzzled-about-powered-risers/12646)

I guess ideally I'd stick with 6-pin all the way because that's what our modular power supplies have
for both SATA and PCIe, though they often have 8-pin (6+2) ports specifically for video cards that need them, too.

Risers often have 6-pin ports [but come with SATA power adapters](https://smile.amazon.com/gp/product/B01MU51491).
I guess because the riser itself doesn't use much power,
this is fine as long as the connector isn't made shoddily.
But if you don't trust it you can get some proper 6-pin to 6-pin cables and ignore the adapters.



## Mining software

### Operating System

> The main options I have seen are Windows 10, EthOS, and Simple Mining OS. 
>
> Windows 10:
> I believe Windows 10 only supports up to 8 graphics cards
> with up to a max of 4GB of memory on each card.
> (yes you probably can change it if you know what you're doing,
> but i mean without unreasonable amounts of effort and lots of ongoing errors/issues, it only supports 8).
> Windows also seems to stop mining due to updates, etc, even after people try to turn the updates off.
> I am not saying windows 10 is a completely bad option though,
> it's what I'd use, but I don't think it's what Dan would use.
> If we do use Windows, I would want to try the BIOSTAR TB250-BTC+ because of the 8 gpu slots it has.
> I do see that this motherboard has some bad reviews in some places
> and it is probably not as good as the other two motherboards I will mention,
> but it is also more available,
> it doesn't look like it is out of stock everywhere.
> Biostar seems be of a lower quality than asrock or asus,
> but their motherboards are cheaper, more available, and better for windows because of certain bios settings.  
> 
> EthOS: 
> http://ethosdistro.com/  (look at features)
> It appears to support up to 16 cards by default.
> I don't understand the problems people are having with it yet,
> but it is specifically designed to be a mining os,
> so it probably shouldn't tend to constantly stop mining as much as windows sounds like it does.
> With ethos I'd like to try the ASRock H110 Pro BTC+ which can support up to 13 graphics cards.
> That is, if it isn't sold out everywhere and if we can find it.  
> 
> I bet Ethos can be modified to take more than 16 cards,
> but I haven't seen any good explanations of how to do it.
> Also, 13 might be enough if each card is going to cost $300+.  
> 
> Simple Mining OS:
> Simple mining os is newer and/or more untested than windows or ethos,
> but I feel like(haha) it can easily support more graphics cards.
> Though if we run into other problems it will be harder to find answers
> because it seems to be a more obscure and less used/supported os.
> If we use Simple Mining OS, we could get the Asus B250 Mining Expert:
> https://www.newegg.com/Product/Product.aspx?Item=N82E16813119028, which has 19 PCIe card slots.

-- Joh

> Correction: I learned that both EthOS and Simple Mining OS
> are not free and could be expensive.
> If we could get away with mining with an OS we already have or a free one,
> that would probably be a better way to start.
> Yesterday I also ordered a motherboard, the ASRock 81 Pro and I everything I need to make a single GPU mining rig.
> It can take up to 6, but I'm starting with 1.
> It was about $1000 altogether.
> This might not be the best,
> but I wanted to do something instead of just talking about it.
> It doesn't mean someone else can't make a better one.

-- Joh, 2018-01-05

> I'm running Claymore on Ubuntu to dual-mine Ethereum and Sia and that seems to be working fine.

-- Dan, 2018-01-06

### Dual mining

> Claymore has created a miner that allows users to mine both Ethereum and another cryptocurrency
> like Decred/Siacoin/Lbry/Pascal at the same time without losing too much efficiency,
> allowing you to not lose much of the Ethereum hashrate while mining a different coin at the same time.
> This miner has a built-in fee of 1% but the optimization surpasses said fee by far,
> making this the most efficient Ethereum miner out there.
> This miner works both with pool and solo modes and even allows for dual mining.

-- [CryptoCompare Article on dual mining](https://www.cryptocompare.com/mining/guides/how-to-dual-mine-ethereum-and-another-coin/)

### What spare cables do I already have?

> All 6 or 8-pin at the power supply end:
> 
> - 1 x Rando molex
> - 5 x SATA power
> - 2 x 8-pin CPU power
> - 1 x 8-pin 'VGA' (for GPU)
> - 5 x 2 x 6+2-pin 'Type 3' (for GPU)

-- Dan, 2018-01-07




## Wallets

> Trezor or ledger work for some nonBTC coins. I'll grab the list.

-- Renee

### How do hardware wallets work?

- Can you back up the keys
- How do they keep things secure

### What's a good hardware wallet for non-BTC?
