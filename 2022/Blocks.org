#+TITLE: Toy Block Sizes

So I was organizing the Tinker Toys, which were mixed in with a bunch of letter blocks.
I always hated the letter blocks because they were a different size than the normal blocks,
which I suppose were 1+3/8" unit blocks (e.g. the marble maze ones).

|                             | Size              |         Size |
|                             | (nominal, inches) | (actual, mm) |
|-----------------------------+-------------------+--------------|
| Unit Block                  | 1+3/8             |              |
| Large letter blocks         | 1+3/4             |         43.1 |
| Medium letter blocks        | 1+9/32            |         32.8 |
| Small letter blocks         | 1+3/16            |         30   |
|-----------------------------+-------------------+--------------|
| Tinker Toy slab (thickness) | 5/8               |          15+ |
| Tinker Toy slab (width)     | 2                 |         50.8 |

