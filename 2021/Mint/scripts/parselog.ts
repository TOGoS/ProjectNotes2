#!/usr/bin/env -S deno run

import {readLines} from "https://deno.land/std/io/bufio.ts";

class AggregatorValue {
	count : number = 0;
	total : number = 0;
}

interface Aggregated {
	timestamp : string;
	[k:string]: number|string;
}

class Aggregator {
	timestampLength : number|undefined = undefined;
	currentTimestamp? : string = undefined;
	values : {[k:string]: AggregatorValue} = {}
	outputs : Aggregated[] = [];

	timestamp(ts:string) {
		if( this.timestampLength ) {
			ts = ts.substr(0,this.timestampLength);
		}
		if( ts == this.currentTimestamp ) return;
		
		this.flushValues();
		this.currentTimestamp = ts;
	}
	value(name:string, v:number) {
		if( this.values[name] == undefined ) {
			this.values[name] = { count: 1, total: v} ;
		} else {
			++this.values[name].count;
			this.values[name].total += v;
		}
	}
	end() {
		this.flushValues();
	}
	flushValues() : void {
		if( this.currentTimestamp == undefined ) return;
		
		let result: Aggregated = {timestamp: this.currentTimestamp};
		for( let name in this.values ) {
			let value = this.values[name];
			result[name] = value.total / value.count;
		}
		this.values = {};
		this.outputs.push(result);
	}
	takeOutputs() : Aggregated[] {
		let outputs = this.outputs;
		this.outputs = [];
		return outputs;
	}
}

const agg : Aggregator = new Aggregator();

const TS_LENGTHS = {
	YEAR: 4,
	MONTH: 7,
	DAY: 10,
	HOUR: 13,
	MINUTE: 16
};

for (const arg of Deno.args) {
	if( arg == "--daily" ) {
		agg.timestampLength = TS_LENGTHS.DAY;
	} else if( arg == "--hourly" ) {
		agg.timestampLength = TS_LENGTHS.HOUR;
	} else if( arg == "--minutely" ) {
		agg.timestampLength = TS_LENGTHS.MINUTE;
	} else {
		throw new Error("Unrecognized argument: '"+arg+"'");
	}
}

function line(line:string|null) {
	if( line != null ) {
		const trimmed = line.trim();
		if( trimmed == '' || trimmed.startsWith('#') ) {
			return;
		}
		
		const kv = trimmed.split(/\s+/);
		const attrName = kv[0];
		const value = kv[1];
		const numericValue = +value;
		if( attrName == "timekeeper/time/minute" ) {
			agg.timestamp(value);
		} else if( !attrName.startsWith("timekeeper") && !isNaN(numericValue) ) {
			agg.value(attrName, numericValue);
		}
	} else {
		agg.end();
	}

	const aggregated = agg.takeOutputs();
	for( const o in aggregated ) {
		console.log(JSON.stringify(aggregated[o]));
	}
}

for await (const l of readLines(Deno.stdin)) {
	line(l);
}

line(null);
