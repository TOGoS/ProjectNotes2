inch = 25.4;

function unit_value(unit_name) =
	unit_name == "mm"        ? 1 :
	unit_name == "inch"      ? inch :
	unit_name == "1/8*inch"  ? inch/8 :
	unit_name == "1/16*inch" ? inch/16 :
	unit_name == "1/32*inch" ? inch/32 :
	unit_name == "1/64*inch" ? inch/64 :
	("invalid unit size"+unit_name);

function distance_to_2d( this, that ) = sqrt( pow(that[0]-this[0], 2) + pow(that[1]-this[1], 2) );

function distance_to_nearest_2d( this, those, i=0 ) =
	i >= len(those) ? infinity :
	min(distance_to_2d( this, those[i] ), distance_to_nearest_2d(this, those, i+1) );

/*
// This version seems to be a bit finicky and cause CGAL errors
// and/or "Normalized tree is growing past 200000 elements.  Aborting normalization" errors.

module countersunk_hole_2(surface_d, neck_d, head_h, depth, bore_d, headbore_d, headbore_height) {
	cylinder(d=headbore_d, h=headbore_height);
	intersection() {
		translate([0,0,-head_h]) cylinder(d=headbore_d, h=head_h+height, center=false);
		cylinder(d1=neck_d, d2=surface_d+(surface_d-neck_d), h=head_h*2, center=true);
	}
	rotate([180,0,0]) cylinder(d=bore_d, h=depth);
}
*/

module countersunk_hole_2(surface_d, neck_d, head_h, depth, bore_d, overhead_bore_d, overhead_bore_height) {
	rotate_extrude() {
		union() {
			// Bore
			polygon([
				[     0  , -depth],
				[bore_d/2, -depth],
				[bore_d/2, -0.01],
				[     0  , -0.01],
			]);
			// Head and headbore
			polygon([
				[              0  ,               -head_h],
				[         neck_d/2,               -head_h],
				// Try to avoid points exactly at the surface:
				[      surface_d/2,                 0.01 ],
				[overhead_bore_d/2,                 0.01 ],
				[overhead_bore_d/2,  overhead_bore_height],
				[              0  ,  overhead_bore_height],
			]);
		}
	}
}

module countersunk_hole(surface_d, neck_d, head_h, depth, bore_d=-1, overhead_bore_d=0, overhead_bore_height=1) {
	countersunk_hole_2(surface_d, neck_d, head_h, depth, bore_d == -1 ? neck_d : bore_d, max(surface_d, overhead_bore_d), overhead_bore_height);
}

// Suitable for #6 flatheads
module small_countersunk_hole(depth) {
	countersunk_hole(8, 4, 2, depth, 5);
	//render() countersunk_hole(1/4*inch, 5/32*inch, 1/16*inch, depth);
	//cylinder(h=thickness*2, d=4, center=true);
}

// Suitable for 1/4" flatheads
module large_countersunk_hole(depth) {
	render() countersunk_hole(1/2*inch, 1/4*inch, 1/8*inch, depth, 5/16*inch);
}

function tovec2(thing) = is_num(thing) ? [thing, thing] : [thing[0], thing[1]];

module rounded_rectangle(size, r) {
	if( r > 0 ) {
		hull() for( y=[-1,1] ) for( x=[-1,1] ) {
			translate([x*(size[0]/2-r), y*(size[1]/2-r)]) {
				circle(r);
			}
		}
	} else {
		square(size, center=true);
	}
}

// Functions for making grids etc

// TODO: Round area_size to nearest cell_size multiple

function grid_cell_center_positions(area_size, cell_size) = [
	for( y=[ -area_size[1]/2+cell_size[1]/2 : cell_size[1] : area_size[1]/2-cell_size[1]/2 ] )
		for( x=[ -area_size[0]/2+cell_size[0]/2 : cell_size[0] : area_size[0]/2-cell_size[0]/2 ] )
			[x,y]
];
function grid_edge_cell_center_positions(area_size, cell_size) = [
	for( group=["edges","middle"] )
		if( group == "edges" )
			for( y=[ -area_size[1]/2+cell_size[1]/2, area_size[1]/2-cell_size[1]/2 ] )
				for( x=[ -area_size[0]/2+cell_size[0]/2 : cell_size[0] : area_size[0]/2-cell_size[0]/2 ] )
					[x,y]
		else
			for( y=[ -area_size[1]/2+(cell_size[1]*3)/2 : cell_size[1] : area_size[1]/2-(cell_size[1]*3)/2 ] )
				for( x=[ -area_size[0]/2+cell_size[0]/2, area_size[0]/2-cell_size[0]/2 ] )
					[x,y]
];
function grid_top_and_bottom_cell_center_positions(area_size, cell_size) = [
	for( y=[ -area_size[1]/2+cell_size[1]/2, area_size[1]/2-cell_size[1]/2 ] )
		for( x=[ -area_size[0]/2+cell_size[0]/2 : cell_size[0] : area_size[0]/2-cell_size[0]/2 ] )
			[x,y]
];
function grid_side_cell_center_positions(area_size, cell_size) = [
	for( y=[ -area_size[1]/2+cell_size[1]/2 : cell_size[1] : area_size[1]/2-cell_size[1]/2 ] )
		for( x=[ -area_size[0]/2+cell_size[0]/2, area_size[0]/2-cell_size[0]/2 ] )
			[x,y]
];

function resolve_grid_positions__1(positions, area_size, cell_size) =
	positions == "none" ? [] :
	positions == "all" ? grid_cell_center_positions(area_size, cell_size) :
	positions == "edges" ? grid_edge_cell_center_positions(area_size, cell_size) :
	positions == "top+bottom" ? grid_top_and_bottom_cell_center_positions(area_size, cell_size) :
	positions == "sides" ? grid_side_cell_center_positions(area_size, cell_size) :
	positions;

function resolve_grid_positions(positions, area_size, cell_size) =
	resolve_grid_positions__1(positions, tovec2(area_size), tovec2(cell_size));

/**
 * Invoke children at the center point of each cell of the grid
 * covering area_size, centered at 0,0
 *
 * TODO: Center the grid when area_size is not a multiple of cell_size
 */
module bazgrid( area_size, cell_size ) {
	for( pos=resolve_grid_positions("all", area_size, cell_size) ) translate(pos) children();
}

vesa100_hole_positions = [ for( y=[-1,1] ) for( x=[-1,1] ) [x*50,y*50] ];
