// Wheelbarrow / shovel hook, v1.0
// Base unit = mm

bar_width = 25.4 * 3/4;
bar_thickness = 25.4 * 3/4;
y_offset = 1.5 * 25.4;

d1 = 30.4;
d2 = 25.4;
extra_space = 2;

inch = 25.4;

screw_hole_positions = [
	//[0,-12/8*inch],
	[0, -9/8*inch],
	[0, -3/8*inch],
	[0, +3/8*inch],
	[0, +9/8*inch],
	//[0,+12/8*inch],
];

linear_extrude(25.4 * 3/4) {
	difference() {
		union() {
			translate([-bar_thickness/2 - d1/2,  y_offset]) circle(d=d1+bar_thickness*2);
			translate([+bar_thickness/2 + d2/2, -y_offset]) circle(d=d2+bar_thickness*2);
			square([bar_thickness, y_offset*2], center=true);
		}
		translate([-bar_thickness/2 - d1/2 - extra_space/2,  y_offset]) circle(d=d1+extra_space);
		translate([ bar_thickness/2 + d2/2 + extra_space/2, -y_offset]) circle(d=d2+extra_space);
		hull() {
			translate([-bar_thickness/2 - d1/2,  y_offset]) circle(d=d1);
			translate([-bar_thickness/2 - d1/2, -y_offset*2]) circle(d=d1);
		}
		hull() {
			translate([+bar_thickness/2 + d2/2, -y_offset]) circle(d=d2);
			translate([+bar_thickness/2 + d2/2,  y_offset*2]) circle(d=d2);
		}
		// Shave off some unnecessarily pointy corners with fuzzy math.
		translate([-bar_thickness/2 - d1, +y_offset-bar_thickness-d1/2]) rotate([0,0,-15]) circle($fn=4, d=20);
		translate([ bar_thickness/2 + d2, -y_offset+bar_thickness+d2/2]) rotate([0,0,-15]) circle($fn=4, d=20);
		for( hp = screw_hole_positions ) {
			translate(hp) circle(d=5/32*inch);
		}
	}
}