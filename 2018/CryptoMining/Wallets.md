- EthDump0 = ```0x58a64c13cd00d2709b644f6771bac60b1fd463d8```
  - [nanopool stats](https://eth.nanopool.org/account/0x58a64c13cd00d2709b644f6771bac60b1fd463d8)
- SiaDump0 = ```942d792e51b2457a33834ee76bf0652ac58f01b521bcd7ea341458285f6e175be38c4f08bbfc```
  - [nanopool stats](https://sia.nanopool.org/account/942d792e51b2457a33834ee76bf0652ac58f01b521bcd7ea341458285f6e175be38c4f08bbfc)
  - [siaminer stats](https://siamining.com/addresses/942d792e51b2457a33834ee76bf0652ac58f01b521bcd7ea341458285f6e175be38c4f08bbfc)
    (since no Linux/Nvidia Sia miners seem to be able to mine to nanopool)
