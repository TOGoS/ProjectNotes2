//// VMPanelHolder, v2

inch = 25.4;

module tograck_hole($fn = 20) {
	cylinder(h=2*inch, d=3/32*inch, center=true);
}

module rounded_cuboid(size, r=1/8*inch, $fn=20) {
	//cube(size, center=center);
	hull() {
		for(x=[-size[0]/2+r, size[0]/2-r]) {
			for(y=[-size[1]/2+r, size[1]/2-r]) {
				for(z=[-size[2]/2+r, size[2]/2-r]) {
					translate([x, y, z]) sphere(r=r);
				}
			}
		}
	}
}

module xy_rounded_cuboid(size, r=1/8*inch, $fn=20) {
	linear_extrude(size[2], center=true) {
		hull() {
			for(x=[-size[0]/2+r,+size[0]/2-r ]) {
				for(y=[-size[1]/2+r, size[1]/2-r]) {
					translate([x,y]) circle(r);
				}
			}
		}
	}
}

module holedur() {
	difference() {
		rounded_cuboid([4.5*inch, 1.5*inch, 1.5*inch]);
		xy_rounded_cuboid([2.5*inch, 1*inch, 2*inch], r=1/16*inch);
		rotate([0,90,0]) xy_rounded_cuboid([0.5*inch, 1*inch, 5.0*inch], r=1/16*inch);
		for(z=[-0.75*inch,0.75*inch]) {
			translate([0,0,z]) xy_rounded_cuboid([3.5*inch, 1*inch, 1/4*inch], r=1/16*inch);
		}
		for(y=[-1/4*inch,0,1/4*inch]) {
			for(x=[-1.5*inch,+1.5*inch]) {
				translate([x,y,0]) tograck_hole();
			}
		}
		for(y=[-1/4*inch,0,1/4*inch]) {
			for(x=[-2*inch,+2*inch]) {
				translate([x,y,0]) tograck_hole();
			}
		}
		for(x = [-2.0*inch : 0.5*inch : 2.0*inch]) {
			translate([x,0,0]) rotate([90,0,0]) tograck_hole();
		}
		//rotate([0, 90, 0]) cylinder(5*inch, d=0.75*inch, center=true);
	}
}

holedur();