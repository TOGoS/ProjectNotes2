#+TITLE: TOGridPile, the dumping document

** TOGridPile Block Shapes

Compatibility chart; columns are different block bottom ('male') shapes,
base plate or block lip ('female') shapes are rows.

| Base shape    | rounded | beveled | hybrid1 | hybrid2 | hybrid3 | hybrid4-xy | hybrid4 |
|---------------+---------+---------+---------+---------+---------+------------+---------|
| rounded       | X       |         |         |         |         |            |         |
| heveled       |         | X       | X       | X       |         |            |         |
| hybrid1       |         |         | X       |         |         |            |         |
| hybrid1-inner | X       |         | X       | X       |         |            |         |
| hybrid2       |         |         |         | X       |         |            |         |
| hybrid3       |         |         | X       | X       | X       | X          |         |
| hybrid3+4     |         |         | X       | X       | X       | X          | X       |
| hybrid4       |         |         | X       | X       |         | X          | X       |

'rounded' is conceptually simplest but difficult to 3D print the bottom of blocks
due to overhangs.  It also doesn't sit very securely.

'beveled' is the next simplest, but its sharp angles are also difficult to print.

'hybrid2' is similar to 'beveled' except the faces of the cube are rounded squares.

'hybrid1' is the same as 'hybrid2' except that the vertical corners are rounded
by intersecting with an extruded square with r=3/16" rounded corners.
In hindsight, it should be called "hybrid2-rounded" (or that rounding should
just be a separate parameter).

'hybrid1-inner' is the union of hybrid1 with 'rounded', so that 'hybrid1'
blocks can be rotated and still fit.

"-rounded" postfix should only be applied to blocks;
it just rounds corners a bit more to make printing easier.
Assume an 'X-rounded' will fit anywhere an 'X' does.

'hybrid4-xy' will fit anywhere a hybrid4 does; the "-xy"
just means that the tabs are only added on the top and bottom,
not to the sides.

hybrid3 or hybrid3-rounded into hybrid3 seem the most secure so far.
Hybrid4-int-hybrid4 is second place, but can rotate a bit around the Z axis.

More notes in OpenSCADDesigns/2023/togridpile/README.org

** TOGridPile Print Series
:PROPERTIES:
:CUSTOM_ID: togridpile-series
:END:

*** Cups

| Prefix | Base      | label | finger- | Foot style   | Lip   | V6HC | Magnets | Magnets | CCSH     | CESH    | Notes                                           |
|        |           |       |   slide |              | seg.  | sub. | top     | bottom  |          |         |                                                 |
|--------+-----------+-------+---------+--------------+-------+------+---------+---------+----------+---------+-------------------------------------------------|
| [[#tga-series][TGA]]    | TGx9.5.6  |    10 |    12.5 | Chatomic 6.2 | Chunk | v6.0 | yes     | yes     |          |         | Lip chunk subtractions have 3/16" corner radius |
| [[#tgb-series][TGB]]    | TGx9.5.6  |     0 |       0 | Chatomic 6.2 | Block | v6.0 | N/A     | yes     |          |         | No label or fingerslide -> no top magnets       |
| TGC    | TGx9.5.13 |     0 |       0 | Chatomic 6.1 | Block | v6.1 | N/A     | yes     |          |         | Same as TGB, but 'fixed' V6HC                   |
| TGD    | TGx9.5.17 |     0 |       0 | Chatomic 6.1 | Block | v6.1 | N/A     |         | THL-1001 | THL-100 | Same as TGC, but with screw holes               |
| TGE    | TGx9.5.28 |    10 |    12.5 | Chatomic 6.0 | Block | none | yes     | yes     |          |         | 'Practical small parts cup'                     |
| TGP    |           |       |         |              |       |      |         |         |          |         | Reserved for plates models; see below           |

For general-purpose TOGridPile cups, you probably want
(and this is my judgement as of 2025-03-04) TGC (no holes) or TGD (holes).
Note that the margins on these are only 0.075mm, so subtract a little
X/Y offset when slicing if you want 0.1mm margins.

TGE adds a label shelf and a fingerslide, but also trims the front
sublip, which is sort of weird.

Also watch for different margins between the different versions.
TGC and TGD have 0.075, TGE***z was 0.075 but then I switched
to 0.1 for TGE***y (which I considered a 'fix' so it didn't
get a new preset name).

*** Baseplates

| Prefix | Base                           | Foot   | Foot  | V6HC  | Lip   | Lip   | Magnets | Magnets | Notes                    |
|        |                                | seg.   | col.  | style | seg.  | col.  | top     | bottom  |                          |
|        |                                |        | style |       |       | style |         |         |                          |
|--------+--------------------------------+--------+-------+-------+-------+-------+---------+---------+--------------------------|
| TGPA   | TOGridPileMinimalBasePlate-v2  | None   | N/A   |       | Chunk | v6.1  | N/A     | N/A     |                          |
| TGPB   | TBD; need magnet placement fix | None   | N/A   |       | Chunk | v6.1  | yes     | no      | With bowtie cutouts!     |
| TGPC   | TGx9.4                         | Chatom | v6.0  |       | Chunk | v6.1  | yes     | no      | Stackable and mountable! |
| TGPD   | TGx9.4.20                      | Chatom | v6.0  | v6.0  | Chunk | v6.1  | yes     | no      | Sturdier than TGPC?      |

*** TGA
:PROPERTIES:
:CUSTOM_ID: tga-series
:END:

Based on TGx9.5.6, which is some additional tweaks applied to the shape described by [[#p1087]].
Notably, the shape of the block subtracted to create the lips has a--perhaps accidentally 1/8" beveled,
due to oversight--3/16" radius.

(In general, the concave corners should be a sharper radius than the convex ones,
to make sure that different styles can fit together.  I don't think I intended
for TGx9.x's inner/concave/female/lip corners to be beveled at all.
Perhaps I meant to have the call from `tgx9_cup_top` to `tgx9_block_foot` set `$tgx9_force_bevel_rounded_corners = false`.
But this has meant that the block rims and baseplate borders have a nice consistent width,
rather than getting all skinny at the corners,
and it doesn't seem to have caused any compatibility problems,
so maybe it's fine!)

Having printed a few TGAs already, I am reminded that one reason to
use block segmentation for the lip instead of chunk segmentation
is that chunk segmentation prevents multi-chunk cups from fitting in sideways.

[2023-07-28]: Idea: Maybe a letter instead of a number can mean height in 8ths of an inch?

| code | inches |  u | chunk |
|------+--------+----+-------|
| A    | 1/8"   |  2 |       |
| B    | 1/4"   |  4 |       |
| C    | 3/8"   |  6 |       |
| D    | 1/2"   |  8 | 1/3   |
| E    | 5/8"   | 10 |       |
| F    | 3/4"   | 12 | 1/2   |
| G    | 7/8"   | 14 |       |
| H    | 1"     | 16 | 2/3   |
| I    | 1+1/8" | 18 |       |
| J    | 1+1/4" | 20 |       |
| K    | 1+3/8" | 22 |       |
| L    | 1+1/2" | 24 | 1     |

*** TGB
:PROPERTIES:
:CUSTOM_ID: tgb-series
:END:

Same as TGA but no label, no fingerslide, and 'block' segmentation

*** TGC
:PROPERTIES:
:CUSTOM_ID: tgc-series
:END:

Same as TGB but v6.1 chatomic foot style and horizontal column lip subtraction,
rather than v6.2 and v6.0, respectively.

Requires TGx9.5.13 for the 'v6.1' v6hc subtraction.

** Tasks

*** DONE Post link to current, very crappy documentation to Reddit

https://www.reddit.com/r/gridfinity/comments/15nom3k/yet_another_gridfinitylike_system_designed_for/

*** DONE Determine whether I really understand v6.2 foot bevels

See [[./TOGridPileOutlines.dxf]].  It seems that yes, I do.
At least when the inset = 1u.

Foot column bevel size variants; note that these assume 2u outer bevels
and -1u foot offset.

| Variation | Bevel size (u) | Note                                          |
|-----------+----------------+-----------------------------------------------|
| v6.1      |          1.414 | 'Correct'; sqrt(2)*u                          |
| v6.0      |          1.707 | Original, based on bad calculation            |
| v6.2      |          2.000 | Deep enough to fit additional foot diagonally |

*** TODO Make a nice SVG or something idk

Need to define this task better.

*** TODO Make an explanatory video
