thickness = 3.175;

// Radial subtraction from outer dimensions (mm) to give wiggle room and/or account for wide extrusion
margin = 0.25;

// Countersunk in-between holes
// Counterbored hole surface diameter; 1/4" = 6.35
hole2_surface_diameter = 7;
// Counterbored hole shaft diameter; 3/16" = 4.7625
hole2_shaft_diameter = 5;
// Counterbored hole shaft diameter; 1/8" = 3.175
hole2_countersink_depth = 2.54;

$fn = 40;

include <BowtieLib.scad>

module panel_plug() {
	difference() {
		union() {
			cylinder(d=7/16*inch-margin*2, h=thickness);
			cylinder(d=5/16*inch-margin*2, h=thickness*2);
		}
		rotate([0,180,0]) countersunk_hole( hole2_surface_diameter, hole2_countersink_depth, hole2_shaft_diameter, thickness*3 );
	}
}

panel_plug();