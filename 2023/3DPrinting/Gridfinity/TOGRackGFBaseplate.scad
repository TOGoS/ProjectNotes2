// TOGRackGFBaseplate-v1.2
//
// Changes:
// v1.1:
// - Countersunk holes for 1/4"-20s have wider bores than the ideal neck base.
// v1.2:
// - Use outer tograck panel dimensions as hull for thicker outer walls

panel_thickness = 3.175;
width_gfu = 2;
height_gfu = 2;
tograck_panel_margin = 0.5;
tograck_panel_corner_radius = 3.175;

tograck_unit_size = 12.7;

gridfinity_pitch = 42;
gridfinity_zpitch = 7;
gridfinity_corner_radius = 3.75;

// Offset from center of cell to magnet position
gridfinity_magnet_offset = gridfinity_pitch/2-8;
gridfinity_magnet_pocket_depth = 2.4;
gridfinity_magnet_pokehole_diameter = 3.5;
gridfinity_magnet_pocket_diameter = 6.5;

module __end_of_params() { }

width_tru = ceil(width_gfu * gridfinity_pitch / tograck_unit_size);
height_tru = 7;

inch = 25.4;

tograck_mounting_hole_spacing = 3*25.4;
tograck_mounting_hole_diameter = 5/32*inch;
tograck_mounting_hole_counterbore_diameter = 1/4*inch;
tograck_mounting_hole_counterbore_depth = 1/32*inch;

$fn = 40;

module countersunk_hole(surface_d, neck_d, head_h, depth, bore_d="auto", counterbore_d="auto", height=100) {
	counterbore_d = counterbore_d == "auto" ? surface_d : counterbore_d;
	bore_d = bore_d == "auto" ? neck_d : bore_d;
	intersection() {
		union() {
			cylinder(d=counterbore_d, h=height, center=false);
			cylinder(d1=neck_d, d2=surface_d+(surface_d-neck_d), h=head_h*2, center=true);
			rotate([180,0,0]) cylinder(d=bore_d, h=depth);
		}
		cylinder(d=counterbore_d, h=height*2, center=true);
	}
}

module counterbored_hole(shaft_d, shaft_depth, counterbore_d, counterbore_depth, height=100) {
	if(       shaft_d > 0 ) translate([0,0,-      shaft_depth]) cylinder(d=      shaft_d, h=      shaft_depth+height, center=false);
	if( counterbore_d > 0 ) translate([0,0,-counterbore_depth]) cylinder(d=counterbore_d, h=counterbore_depth+height, center=false);
}

function grid_cell_center_positions(area_size, cell_size) = [
	for( y=[ -area_size[1]/2+cell_size[1]/2 : cell_size[1] : area_size[1]/2-cell_size[1]/2 ] )
		for( x=[ -area_size[0]/2+cell_size[0]/2 : cell_size[0] : area_size[0]/2-cell_size[0]/2 ] )
			[x,y]
];

module rounded_square(size, r, center=true) {
	offset = center ? [0,0] : [size[0]/2, size[1]/2];
	if( r > 0 ) hull() for( xm=[-1,1] ) for( ym=[-1,1] ) {
		translate([offset[0] + xm*(size[0]/2-r), offset[1] + ym*(size[1]/2-r)]) circle(r=r);
	} else square(size, center=center);
}

module tograck_panel_hull(size, tograck_unit_size=tograck_unit_size, corner_r=tograck_panel_corner_radius, margin=0.5) {
	linear_extrude(size[2]) rounded_square([size[0]-margin*2, size[1]-margin*2], r=corner_r, center=true);
}

function tograck_hole_positions(panel_size, tograck_unit_size=tograck_unit_size) = [
	for( xt=[0:1:round(panel_size[0]/tograck_unit_size)-1] ) for( yt=[-1,1] ) [
		xt*tograck_unit_size - panel_size[0]/2 + tograck_unit_size/2,
		(panel_size[1]-tograck_unit_size)/2*yt
	]
];

module tograck_panel(size, tograck_unit_size=tograck_unit_size, corner_r=tograck_panel_corner_radius, margin=0.5) {
	linear_extrude(size[2]) difference() {
		rounded_square([size[0]-margin*2, size[1]-margin*2], r=corner_r, center=true);
		for( pos=tograck_hole_positions(size, tograck_unit_size=tograck_unit_size) ) {
			translate( pos ) {
				circle(d=tograck_mounting_hole_diameter);
			}
		}
	}
}

// tog_gridfinity based on functions from https://github.com/vector76/gridfinity_openscad

module tog_gridfinity_cornercopy(r, num_x=1, num_y=1, gridfinity_pitch=gridfinity_pitch, extra_x=0, extra_y=0) {
	for (xx=[-r-extra_x, gridfinity_pitch*(num_x-1)+r+extra_x]) for (yy=[-r-extra_y, gridfinity_pitch*(num_y-1)+r+extra_y]) 
		translate([xx, yy, 0]) children();
}

// make repeated copies of something(s) at the gridfinity spacing of 42mm
module tog_gridfinity_gridcopy(num_x, num_y, gridfinity_pitch=gridfinity_pitch) {
	for (xi=[1:num_x]) for (yi=[1:num_y]) translate([gridfinity_pitch*(xi-1), gridfinity_pitch*(yi-1), 0]) children();
}

// unit pad slightly oversize at the top to be trimmed or joined with other feet or the rest of the model
// also useful as cutouts for stacking
module tog_gridfinity_pad_oversize(num_x=1, num_y=1, margins=0, sharp_corners=false) {
	pad_corner_position = gridfinity_pitch/2 - 4; // must be 17 to be compatible
	bevel1_top = 0.8;     // z of top of bottom-most bevel (bottom of bevel is at z=0)
	bevel2_bottom = 2.6;  // z of bottom of second bevel
	bevel2_top = 5;       // z of top of second bevel
	bonus_ht = 0.2;       // extra height (and radius) on second bevel
	
	// female parts are a bit oversize for a nicer fit
	radialgap = margins ? 0.25 : 0;  // oversize cylinders for a bit of clearance
	axialdown = margins ? 0.1 : 0;   // a tiny bit of axial clearance present in Zack's design
	
	translate([0, 0, -axialdown])
	difference() {
		union() {
			hull() tog_gridfinity_cornercopy(pad_corner_position, num_x, num_y) {
				if (sharp_corners) {
					cylsq(d=1.6+2*radialgap, h=0.1);
					translate([0, 0, bevel1_top]) cylsq(d=3.2+2*radialgap, h=1.9);
				}
				else {
					cylinder(d=1.6+2*radialgap, h=0.1, $fn=24);
					translate([0, 0, bevel1_top]) cylinder(d=3.2+2*radialgap, h=1.9, $fn=32);
				}
			}
			
			hull() tog_gridfinity_cornercopy(pad_corner_position, num_x, num_y) {
				if (sharp_corners) {
					translate([0, 0, bevel2_bottom]) 
					cylsq2(d1=3.2+2*radialgap, d2=7.5+0.5+2*radialgap+2*bonus_ht, h=bevel2_top-bevel2_bottom+bonus_ht);
				}
				else {
					translate([0, 0, bevel2_bottom]) 
					cylinder(d1=3.2+2*radialgap, d2=7.5+0.5+2*radialgap+2*bonus_ht, h=bevel2_top-bevel2_bottom+bonus_ht, $fn=32);
				}
			}
		}
		
		// cut off bottom if we're going to go negative
		if (margins) {
			translate([-gridfinity_pitch/2, -gridfinity_pitch/2, 0])
			cube([gridfinity_pitch*num_x, gridfinity_pitch*num_y, axialdown]);
		}
	}
}

module tog_gridfinity_frame_pockets(num_x, num_y, trim=0, gridfinity_pitch=gridfinity_pitch) {
	translate([-(num_x-1)/2*gridfinity_pitch, -(num_y-1)/2*gridfinity_pitch] ) {
		translate([0, 0, trim ? 0 : -0.01])
			tog_gridfinity_gridcopy(num_x, num_y) render() tog_gridfinity_pad_oversize(margins=1);
	}
}


module tog_gridfinity_frame_plain(num_x, num_y, extra_down=0, trim=0, gridfinity_pitch=gridfinity_pitch, corner_radius=gridfinity_corner_radius, extra_x=0, extra_y=0) {
	difference() {
		translate([-(num_x-1)/2*gridfinity_pitch, -(num_y-1)/2*gridfinity_pitch] ) {
			ht = extra_down > 0 ? 4.4 : 5;
			corner_position = gridfinity_pitch/2-corner_radius-trim;
			hull() tog_gridfinity_cornercopy(corner_position, num_x, num_y, extra_x=extra_x, extra_y=extra_y)
				translate([0, 0, -extra_down]) cylinder(r=corner_radius, h=ht+extra_down, $fn=44);
		}
		tog_gridfinity_frame_pockets(num_x, num_y, trim=trim, gridfinity_pitch=gridfinity_pitch);
	}
}

function distance_from_nearest_fencepost(x, fp_x0, fp_spacing) = abs((x - fp_x0) - fp_spacing * round((x - fp_x0)/fp_spacing));

function tog_gridfinity_magnet_positions(width_gfu, height_gfu, gridfinity_pitch=gridfinity_pitch) = [
	for( ym=[0:1:height_gfu] ) for( xm=[0:1:width_gfu-1] ) for( ycm=[-1,1] ) for( xcm=[-1,1] ) [
		((xm-width_gfu/2)+0.5) * gridfinity_pitch + xcm*gridfinity_magnet_offset,
		((ym-height_gfu/2)+0.5) * gridfinity_pitch + ycm*gridfinity_magnet_offset,
	]
];

difference() {
	tograck_panel_size = [
		width_tru*tograck_unit_size,
		height_tru*tograck_unit_size,
		panel_thickness + 4.4
	];

	tograck_panel_hull(tograck_panel_size, tograck_unit_size=tograck_unit_size, corner_r=tograck_panel_corner_radius, margin=tograck_panel_margin);
	
	translate([0,0,panel_thickness]) tog_gridfinity_frame_pockets(width_gfu, height_gfu);
	for( pos=tog_gridfinity_magnet_positions(width_gfu, height_gfu) ) {
		translate([pos[0], pos[1], panel_thickness]) counterbored_hole(
			gridfinity_magnet_pokehole_diameter, panel_thickness*2,
			gridfinity_magnet_pocket_diameter, gridfinity_magnet_pocket_depth,
			100
		);
	}
	gridbeam_unit_size = 19.05;
	width_gb = round(tograck_panel_size[0] / gridbeam_unit_size);
	height_gb = round(tograck_panel_size[1] / gridbeam_unit_size);
	for( ym=[0:1:height_gb-1] ) for( xm=[0:1:width_gb-1] ) {
		pos = [gridbeam_unit_size * (xm - (width_gb-1)/2), gridbeam_unit_size * (ym - (height_gb-1)/2)];
		if(
			distance_from_nearest_fencepost(pos[0], -width_gfu*gridfinity_pitch/2, gridfinity_pitch) > 12 &&
			distance_from_nearest_fencepost(pos[1], -height_gfu*gridfinity_pitch/2, gridfinity_pitch) > 12
		) {
			translate([pos[0], pos[1], panel_thickness]) render() countersunk_hole(1/2*inch, 1/4*inch, 1/8*inch, panel_thickness*2, bore_d=5/16*inch);
		}
	}
	for( pos=tograck_hole_positions(tograck_panel_size, tograck_unit_size=tograck_unit_size) ) {
		if( distance_from_nearest_fencepost(pos[0], -width_gfu*gridfinity_pitch/2, gridfinity_pitch) > 5 ) {
			translate([pos[0], pos[1], panel_thickness]) {
				render() countersunk_hole(1/4*inch, 5/32*inch, 1/16*inch, panel_thickness+1);
			}
		}
	}
}
