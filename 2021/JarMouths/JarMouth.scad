//// Panel-mount mason jar necks, v9

//// Parameters

/* [Thread Options] */

// Outer diameter of regular-mouth threads (inches)
regular_outer_diameter_inches = 2.75;

// Outer diameter of wide-mouth threads (inches)
wide_outer_diameter_inches = 3.407;

// The 3.407 actually printed as about 3.413 to 3.425
// A jar lid (that I measured) is 3+5/16", or 3.3125.

// Thread radius pre-clip offset (inches)
// Increase to clip off thread edges without changing size.
thread_radius_pre_clip_offset_inches = 0.015; // [ 0 : 0.001 : 0.05 ]

// Angle from vertical of thread surfaces (degrees)
thread_angle_from_vertical = 35; // [ 20 : 1 : 60]

/*
 * I would say that the thread outer diameter should decrease to inner
 * diameter linearly starting at 0.2 inches from the top.  Then I would make
 * the neck 0.15 inches taller.
 * -- Dad
 *
 * Okay! So:
 *  thread_taper_distance_inches = 0.2
 *  neck_height_inches = 0.66 (previosuly 0.535)
 */

// Vertical distance across which to taper the threads
thread_taper_distance_inches = 0.2;

/* [Other Size Parameters] */

// Neck height (inches)
neck_height_inches = 0.66;

// Panel thickness (inches)
base_panel_thickness_inches = 0.125;

// Gridbeam panel margin (inches)
gridbeam_margin_inches = 0.25;

number6_hole_diameter_inches = 0.19625;
gridbeam_hole_diameter_inches = 0.325;

/* [Target] */

// What to make?
target_name = "widemouth-prototype"; // [regular-prototype : Regular Prototype, widemouth-prototype: Widemouth Prototype]
// Base style
base_style = "v9"; // [v7 : Minimal Hexagon, v8 : "4½″ Gridbeam Panel", v9 : Dodecagon]



module end_of_customizable_variables() {}

// Basic unites
mm = 1;
inch = 25.4 * mm;
epsilon = 1/1024;

// Parameter translations
base_panel_thickness = base_panel_thickness_inches * inch;
gridbeam_margin = gridbeam_margin_inches * inch;
thread_radius_pre_clip_offset = thread_radius_pre_clip_offset_inches * inch;
thread_taper_distance = thread_taper_distance_inches * inch;

number6_hole_diameter = number6_hole_diameter_inches * inch;
gridbeam_hole_diameter = gridbeam_hole_diameter_inches * inch;

// Jar dimensions
regular_outer_diameter = regular_outer_diameter_inches * inch; // But see adjustment
regular_inner_diameter = regular_outer_diameter - (1/8) * inch;
regular_throat_diameter = (2 + 5 / 16) * inch; // Measured from a real jar!
regular_thread_pitch = (1/5) * inch;

wide_outer_diameter = wide_outer_diameter_inches * inch;
wide_inner_diameter = wide_outer_diameter - (1/8) * inch;
wide_throat_diameter = (2 + 15/16) * inch;
wide_thread_pitch = (1/4) * inch;

thread_pitch = 1/5 * inch; // Or should it be 5.3mm?  Close.
total_height = base_panel_thickness + neck_height_inches * inch;

function lerp(a, b, ratio) = a * (1-ratio) + b * ratio;

function thread_profile_function_2(inner_radius, outer_radius, notch_radius, pre_clip_offset=0) =
	function (t) max(inner_radius, min(
		outer_radius,
		lerp(notch_radius, outer_radius, t * 2    ) + pre_clip_offset,
		lerp(outer_radius, notch_radius, t * 2 - 1) + pre_clip_offset
	));

function thread_profile_function(inner_radius, outer_radius, angle_from_vertical, pre_clip_offset) =
	thread_profile_function_2(
		inner_radius, outer_radius,
		notch_radius = outer_radius - thread_pitch / 2 * sin(angle_from_vertical)/cos(angle_from_vertical),
		pre_clip_offset = pre_clip_offset
	);

function polar_to_xy(angle, dist) = [cos(angle) * dist, sin(angle) * dist];

module threads(thread_radius_function, pitch, bottom_z, top_z, bottom_scale, top_scale) {
	length = top_z - bottom_z;
	// rotate() is right-handed, but
	// linear_extrude twists left-handed!
	translate([0,0,bottom_z]) scale(bottom_scale) rotate([0,0,360 * bottom_z / pitch]) linear_extrude(
		height = length,
		twist = -360 * length / pitch,
		scale = top_scale / bottom_scale
	) polygon([for (t = [0:1/60:1]) polar_to_xy(t * 360, thread_radius_function(t)) ]);
}

// Creates neck with thread (but does not remove throat) from z=0 to length
module solid_neck_v2(inner_diameter, outer_diameter, thread_pitch, length) {
	//cylinder(d = inner_diameter, h = length);
	$fn = 100;
	tpf = thread_profile_function(
		 inner_diameter / 2,
		 outer_diameter / 2,
		 angle_from_vertical = thread_angle_from_vertical,
		 pre_clip_offset = thread_radius_pre_clip_offset
	);

	echo(str(
		"Thread outer diamter: ", outer_diameter, "mm, ",
		"inner diameter: ", inner_diameter, "mm, ",
		"length: ", length, "mm, ",
		"taper length: ", thread_taper_distance, "mm"));

	taper_start_height = length - thread_taper_distance;
	threads(tpf, thread_pitch, 0, taper_start_height + epsilon, 1, 1);
	threads(tpf, thread_pitch, taper_start_height, length, 1, inner_diameter / outer_diameter);
	cylinder(d = inner_diameter, h = length);
}

module solid_neck(inner_diameter, outer_diameter, thread_pitch, length) {
	solid_neck_v2(inner_diameter, outer_diameter, thread_pitch, length);
}


module base(height = base_panel_thickness) {
	linear_extrude(height = height) {
		union() {
			square(size = [6 * inch - gridbeam_margin * 2, 4.5 * inch], center = true);
			square(size = [4.5 * inch, 6 * inch - gridbeam_margin * 2], center = true);
			translate([-2.25 * inch, -2.25 * inch]) circle(d=1.5 * inch - gridbeam_margin * 2);
			translate([+2.25 * inch, -2.25 * inch]) circle(d=1.5 * inch - gridbeam_margin * 2);
			translate([-2.25 * inch, +2.25 * inch]) circle(d=1.5 * inch - gridbeam_margin * 2);
			translate([+2.25 * inch, +2.25 * inch]) circle(d=1.5 * inch - gridbeam_margin * 2);
		}
	}
}

function regular_polygon_point_angles(point_count, corner_bevel_angle) = [
	for( n = [0:1:point_count*2-1] ) n % 2 == 0 ? (n-0)/2 * 360 / point_count - corner_bevel_angle/2 : (n-1)/2 * 360 / point_count + corner_bevel_angle/2
];

function regular_polygon_points(point_count, point_dist = 1) =	[
	for( angle = regular_polygon_point_angles(point_count, 360 / point_count / 10) ) [
		point_dist * cos(angle),
		point_dist * sin(angle),
	]
];

function arc(x,y,rad,ang0,ang1) = [ for( a = [ang0:5:ang1] ) [x + cos(a)*rad, y + sin(a)*rad] ];

function rounded_rect_points(x0, y0, x1, y1, rad) = concat(arc(x0,y1,rad,90,180), arc(x0,y0,rad,180,270), arc(x1,y0,rad,270,360), arc(x1,y1,rad,360,450));


module tentacle(pos, seed, direction, curl, size) {
	if( size > 1 ) {
		inner_radius = regular_inner_diameter / 2;

		distance_from_center = sqrt(pos[0]*pos[0] + pos[1]*pos[1]);
		randovect = rands(-5,5,1,seed);
		
		tentacle_ring_width = 3 * inch - inner_radius;
		distance_from_neck = distance_from_center - inner_radius;
		extra_curl_t = distance_from_neck / tentacle_ring_width;
		extra_curl_factor = pow(extra_curl_t, 3);
		enforced_curl = extra_curl_factor * 30;

		curl1 = curl + randovect[0];
		curl2 = sign(curl1) * max(abs(curl1) + randovect[0], enforced_curl);

		//curl2 = curl1 + (curl1 / abs(curl1)) * extra_curl_factor * 60; // Curl harder when approaching the edge

		direction1 = direction + curl2;

		translate(pos) sphere(d = size);

		//echo("distance_from_center = ", distance_from_center);
		//downscale = 0.95 + (3 * inch - inner_radius) / (distance_from_center - inner_radius);
		pos1 = [
			pos[0] + size / 4 * cos(direction1),
			pos[1] + size / 4 * sin(direction1),
			pos[2],
		];
		tentacle(pos1, seed + 10 + randovect[0], direction1, curl2, size * 0.98);
	}
}

module tentacle_from_angle(angle, initial_size) {
	$fn = 5;
	inner_radius = regular_inner_diameter / 2;
	
	startpos = [
		inner_radius * cos(angle),
		inner_radius * sin(angle),
		base_panel_thickness,
	];
	randovect = rands(-5,5,1,angle);
	tentacle(startpos, angle, angle, randovect[0], initial_size);
}


module mounting_hole_subtraction(inner_diameter, outer_diameter) {
	$fn = 25;
	union() {
		translate([0,0,-1]) cylinder(d = inner_diameter, h = total_height + 2);
		translate([0,0,base_panel_thickness+epsilon]) cylinder(d = outer_diameter, h = total_height);
	}
}

module number6_mounting_hole_subtraction() {
	mounting_hole_subtraction(number6_hole_diameter, 1/2*inch);
}

module gridbeam_mounting_hole_subtraction() {
	mounting_hole_subtraction(gridbeam_hole_diameter, 7/8*inch);
}


module throat_subtraction(diameter) {
	$fn = 100;
	translate([0,0,-1]) cylinder(d = diameter, h = total_height + 2);
}

// Pass a 2D shape as the child
module panel_pocket_subtraction() {
	$fn = 50;
	translate([0,0,base_panel_thickness/2]) linear_extrude(height = total_height) {
		children();
	}
}

module regular_size_with_tentacle_base() {
	difference() {
		union() {
			solid_neck(
				inner_diameter = regular_inner_diameter,
				outer_diameter = regular_outer_diameter,
				thread_pitch = regular_thread_pitch,
				length = total_height
			);
			base(base_panel_thickness);
			intersection() {
				base(base_panel_thickness + 10);
				union() {
					for( a = [2.5 : 5 : 360] ) tentacle_from_angle(a, 5);
					for( a = [0 : 30 : 360] ) tentacle_from_angle(a, 10);
				}
			}
		};
		throat_subtraction(regular_throat_diameter);
		translate([-2.25*inch, -2.25*inch, 0]) gridbeam_mounting_hole_subtraction();
		translate([+2.25*inch, -2.25*inch, 0]) gridbeam_mounting_hole_subtraction();
		translate([-2.25*inch, +2.25*inch, 0]) gridbeam_mounting_hole_subtraction();
		translate([+2.25*inch, +2.25*inch, 0]) gridbeam_mounting_hole_subtraction();
	};
}


//// Base hulls

module minimal_polygon_base_hull(point_count, height = base_panel_thickness, outer_radius) {
	linear_extrude(height = height) {
		union() {
			points = regular_polygon_points(point_count, outer_radius);
			polygon(points);
		}
	}	
}

module v8_base_hull(height = base_panel_thickness, outer_radius) {
	linear_extrude(height = height) {
		union() {
			points = rounded_rect_points(-2.25*inch, -2.25*inch, +2.25*inch, +2.25*inch, 0.5*inch);
			polygon(points);
		}
	}	
}

module base_hull() {
	if( base_style == "v7" ) {
		minimal_polygon_base_hull(6, base_panel_thickness, hex_base_radius);
	} else if( base_style == "v8" ) {
		v8_base_hull(base_panel_thickness);
	} else if( base_style == "v9" ) {
		minimal_polygon_base_hull(12, base_panel_thickness, hex_base_radius);
	}
}


//// Base holes

module v8_pocket_shape() {
	polygon(rounded_rect_points(2.45*inch,1*inch,2.45*inch,1.75*inch,3/16*inch));
}


hex_base_hole_distance = target_name == "regular-prototype" ? 1.75*inch : 2.25*inch;
hex_base_radius = target_name == "regular-prototype" ? 2*inch : 2.5*inch;

module v7_base_holes() {
	for( ang = [0:60:300] ) {
		rotate(a = ang, v = [0,0,1])
			translate([hex_base_hole_distance,0,0]) number6_mounting_hole_subtraction();
	}
}

module v9_base_holes() {
	for( ang = [0:30:330] ) {
		rotate(a = ang, v = [0,0,1])
			translate([hex_base_hole_distance,0,0]) number6_mounting_hole_subtraction();
	}
}

module v8_base_holes() {
	for( ang = [15:30:345] ) {
		rotate(a = ang, v = [0,0,1])
			translate([hex_base_hole_distance,0,0]) number6_mounting_hole_subtraction();
	}
	for( ang = [0:90:360-1] ) {
		rotate(a=ang, v=[0,0,1]) {
			translate([2.25*inch, 2.25*inch]) gridbeam_mounting_hole_subtraction();
			translate([2.25*inch, 0*inch]) gridbeam_mounting_hole_subtraction();
		}
	}
	for( ang = [0:90:360-1] ) {
		rotate(a=ang, v=[0,0,1]) {
			for( sy = [-1:2:1] ) {
				scale([1,sy,1]) {
					panel_pocket_subtraction() { v8_pocket_shape(); }
				}
			}
		}
	}
}

module base_holes() {
	if( base_style == "v7" ) v7_base_holes();
	else if( base_style == "v8" ) v8_base_holes();
	else if( base_style == "v9" ) v9_base_holes();
}

module regular_prototype() {
	difference() {
		union() {
			solid_neck(
				inner_diameter = regular_inner_diameter,
				outer_diameter = regular_outer_diameter,
				thread_pitch = regular_thread_pitch,
				length = total_height
			);
			base_hull();
		}
		throat_subtraction(regular_throat_diameter);
		base_holes();
	}
}

module wide_prototype() {
	difference() {
		union() {
			solid_neck(
				inner_diameter = wide_inner_diameter,
				outer_diameter = wide_outer_diameter,
				thread_pitch = wide_thread_pitch,
				length = total_height
			);
			base_hull();
		}
		throat_subtraction(wide_throat_diameter);
		base_holes();
	}
}

if (target_name == "regular-prototype") {
	regular_prototype();
} else if (target_name == "widemouth-prototype") {
	wide_prototype();
}
