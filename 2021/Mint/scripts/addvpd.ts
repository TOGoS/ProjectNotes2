#!/usr/bin/env -S deno run

import {readLines} from "https://deno.land/std/io/bufio.ts";

function calculateVpdKpa(tempC:number, rh:number) {
	const es = 0.6108 * Math.E ** (17.27 * tempC / (tempC + 237.3));
	const ae = rh * es;
	return es - ae;
}

function processRecord(rec:any) {
	if( typeof rec == 'object' ) {
		let rewritten:any = {};
		for( let k in rec ) {
			rewritten[k] = processRecord(rec[k]);
		}
		if( rewritten.temperature && rewritten.humidity ) {
			rewritten.vpd = {
				kpa: calculateVpdKpa(rewritten.temperature.c, rewritten.humidity.percent/100)
			}
		}
		return rewritten;
	}
	
	return rec;
}

function line(line:string):void {
	const trimmed = line.trim();
	if( trimmed == '' || trimmed.startsWith('#') ) {
		return;
	}
	
	console.log(JSON.stringify(processRecord(JSON.parse(line))));
}

for await (const l of readLines(Deno.stdin)) {
	line(l);
}
