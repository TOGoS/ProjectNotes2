https://smile.amazon.com/gp/product/B01IQO8LZC


I think for the money you can't go wrong with these cameras. I have 2 outside sheltered from the weather and they operate superbly. Video quality is good even at night and the app is pretty reliable.

I like these better thanSDETER 720P WiFi Wireless HD IP Network Camera, Pan/Tilt, Plug/Play, Day/Night Vision Home Surveillance, Two-Way Audio, SD Card Slot, Alarm, Baby Monitor for the same money, they advertise the url's and settings so you can use it with 3rd party tools, although the documentation could be better. The SDeter only lets you use it with an app and the app is constantly making calls back to China..... Even moving the camera caused this.

If you want a camera that will record on motion, installs easy and will alert your phone when there is activity, this will work great right out of the box. If you want more advanced integration, see below:

For those interested these are the URL's that are known to work:
BlueIris:
When setting up, make sure the ONVIF port is set to 10080 and the IP address you point it to is to the camera IP port 81 http : // yourip : 81 (No spaces) It will self discover. The trick to getting it to work is under the path section it will correctly fill in /tcp/av0_0. What I had to guess was you have to append the username and password again to the end so the path should be /tcp/av0_0?user=USERNAME&pwd=PASSWORD. Sound and Video worked like a charm after that.

ISpy: has the same issue, append the username and password to the end of the url after initial setup and it just works.

Vera (MIOS): This system is a great zwave automation hub, but its camera support is horrific. The only way to get this camera (or most) to work is by installing the Blue Iris plugin and setting the camera but in Blue Iris then having the plugin connect to it. The only issues is authentication, it will not work if the Blue Iris web server requires authentication, even a blank password setup. You have to setup the Web Server Advanced options for Authentication to Non-LAN only, then you can control and view the camera within Vera.

Hope this helps.
