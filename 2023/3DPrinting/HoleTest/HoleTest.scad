inch = 25.4;
hole_fn = 40;
thickness = 1/2*inch;

difference() {
	cube([1*inch, 1*inch, thickness], center=true);
	translate([-1/4*inch, -1/2*inch, 0]) rotate([0,0,0]) cube([1/8*inch, 1/8*inch, thickness*2], center=true);
	translate([-1/4*inch, -1/4*inch, 0]) cylinder(h=thickness*2, d=2.0, center=true, $fn=hole_fn);
	translate([ 1/4*inch, -1/4*inch, 0]) cylinder(h=thickness*2, d=2.5, center=true, $fn=hole_fn);
	translate([-1/4*inch,  1/4*inch, 0]) cylinder(h=thickness*2, d=3.0, center=true, $fn=hole_fn);
	translate([ 1/4*inch,  1/4*inch, 0]) cylinder(h=thickness*2, d=3.5, center=true, $fn=hole_fn);
}
