// PhoneHolderBack-v2.2
// 
// Changes:
// v2.0:
// - Put backside at z=0
// - Add VESA 100mm holes to allow attaching to a monitor mount
// v2.1:
// - Allow customization (straight, counterbored, none) VESA 100 holes.
// v2.2:
// - Refactor countersunk hole code, changing their shape slightly

// Unit used for *_bsunit
bsunit = "1/8*inch"; // ["mm", "inch", "1/8*inch", "1/16*inch", "1/32*inch", "1/64*inch", "inval"]
// Size of one small block
sblock_size_bsunits = 4;
// Size of one large block
lblock_size_bsunits = 12;

width_lblocks = 3;
height_lblocks = 4;

thickness = 3.175;
corner_rad = 3.175;

vesa_mounting_hole_style = "countersunk"; // ["straight", "countersunk", "none"]
// Size of VESA 100x0100 mounting holes
vesa_mounting_hole_diameter = 5; // 0.1

// Stand-in for infinity; used for distance calculations
infinity = 1000;

include <PhoneHolderLib.scad>;

bsunit_size = unit_value(bsunit);
sblock_size = bsunit_size * sblock_size_bsunits;
lblock_size = bsunit_size * lblock_size_bsunits;
width  = width_lblocks*lblock_size;
height = height_lblocks*lblock_size;

corner_fn = 20;
hole_fn   = 20;

module topside(thickness) {
	translate([0,0,thickness]) children();
}
module bottomside(thickness) {
	translate([0,0,0]) rotate([180,0,0]) children();
}
module middleside(thickness) {
       translate([0,0,thickness/2]) children();
}

vesa_mounting_hole_surface_diameter =
	vesa_mounting_hole_style == "countersunk" ? vesa_mounting_hole_diameter*2 : // Approximately, assuming 90-degree screw heads
	vesa_mounting_hole_diameter;
min_shole_distance = max(vesa_mounting_hole_surface_diameter + 5/32*inch, vesa_mounting_hole_diameter + 1/4*inch)/2 + 1;

module vesa_mounting_hole() {
	if( vesa_mounting_hole_style == "straight" ) {
		middleside(thickness) cylinder(d=vesa_mounting_hole_diameter, h=infinity, center=true);
	} else if( vesa_mounting_hole_style == "countersunk" ) {
		bottomside() countersunk_hole(vesa_mounting_hole_surface_diameter, vesa_mounting_hole_diameter, vesa_mounting_hole_diameter/2, infinity);
	} else if( vesa_mounting_hole_style == "none" ) {
		// None!
	}
}

module a_vesa_mounting_hole() { render() vesa_mounting_hole($fn=hole_fn); }
module a_large_countersunk_hole() { render() large_countersunk_hole(infinity, $fn=hole_fn); }
module a_small_countersunk_hole() { render() small_countersunk_hole(infinity, $fn=hole_fn); }

module phone_holder_back_panel(size, corner_rad=corner_rad) {
	thickness = size[2];

	difference() {
		linear_extrude(thickness) {
			rounded_rectangle(tovec2(size), corner_rad, $fn=corner_fn);
		}
		for( pos=grid_cell_center_positions(size, [sblock_size,sblock_size]) ) {
			if( distance_to_nearest_2d(pos, vesa_mounting_hole_style == "none" ? [] : vesa100_hole_positions) >= min_shole_distance ) {
				translate(pos) topside(thickness) a_small_countersunk_hole();
			}
		}
		for( pos=grid_cell_center_positions(size, [lblock_size,lblock_size]) ) {
			translate(pos) bottomside(thickness) a_large_countersunk_hole();
		}
		for( pos=vesa100_hole_positions ) {
			translate(pos) a_vesa_mounting_hole();
		}
	}
}

phone_holder_back_panel([width,height,thickness]);
