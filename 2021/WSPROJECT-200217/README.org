#+TITLE: WSPROJECT-200217: Arduino (or ESP8266) MIDI controller

Can I make my own MIDI controller?

Hypothesis: Yes, using [[https://awesomeopensource.com/project/tttapa/Control-Surface][this library or similar]].

Or perhaps 'MIDIUSB', by Arduino.


** Tasks

*** TODO Test feasibility with an entirely fake one

Okay so looks like MIDIUSB is incompatible with ESP8266 boards.
So I need to figure out [[https://tttapa.github.io/Control-Surface-doc/Doxygen/d8/da8/md_pages_Installation.html][how to install Control-Surface]]

They seem to really want me to use Git, so...

#+BEGIN_SRC bat
ccd C:\Users\togos\stuff\ext-proj
git clone https://github.com/tttapa/Control-Surface.git
#+END_SRC

Man, it's taking a while.  There's lots of stuff in there, I guess.
But, huh, it downloaded almost instantaneously onto Framey.
Maybe TOGThoms1's network connectino is bad (not unlikely!)  72 KiB/s instead of Framey's 22 MiB/s, jeez.

But then the documentation says

#+BEGIN_QUOTE
If you are using an Arduino board with on-board USB capabilities, you
need the MIDIUSB library. Install it as explained before, or by using
the Library Manager.
#+END_QUOTE

So maybe it's using MIDIUSB after all?  Idk.

[2021-12-31T18:39] Tried to run one of the examples.
It said it uploaded Okay, but no MIDI device showed up in Windows or
in Live so I guess I need to do more research.

**** DONE Try USBMIDI_write on the Uno R3

[[https://forum.arduino.cc/t/error-midiusb-can-only-be-used-with-an-usb-mcu/437801][Well that doesn't work]].

**** DONE Try USBMIDI_write on the WeMos D1 Mini

Also doesn't work.
Also says "#error MIDIUSB can only be used with an USB MCU."

**** DONE More research / successful demo of MIDI over USB

tl;dr: USBMIDI_write works and talks to Live on a Pro Micro I found lying around.
It does not work on Uno or some other boards because they have a hardwired
serial-to-USB chip.

#+BEGIN_QUOTE
The ESP8266 and ESP32 microcontrollers don't have native USB support,
and all development boards I've come across use a single-purpose
USB-to-TTL chip, which means that they fall into the same category as
the Arduino Nano when it comes to MIDI over USB.

That being said, both the ESP8266 and the ESP32 have built-in WiFi, so
you can use rtpMIDI, using the AppleMIDI library, for example.

An alternative is to use Open Sound Control (OSC). This is not MIDI,
it's an entirely different protocol, built on top of UDP. It can be
used for bidirectional communication between audio software and
control surfaces. I've successfully used this to build a control
surface for Reaper.
#+END_QUOTE

#+BEGIN_QUOTE
I strongly recommend getting a Teensy 3.x or 4.x for building a
Control Surface. MIDI over USB is supported right out of the box, and
it's the only platform that currently supports USB audio output. On
top of that, it has plenty of memory to create large MIDI Controllers,
and drive lots of displays.
#+END_QUOTE

Hokay, perhaps that is the Christmas gift that I need.
But a Teensy is 20$.  Could a Raspberry Pi (Zero or Pico) do the job?
I kind of want to play with them anyway.

#+BEGIN_QUOTE
If you just want a small and cheap MIDI Controller, the Arduino
Leonardo/Micro are good choices as well, just keep in mind that they
don't have a lot of RAM, and are pretty slow compared to Teensy
boards, both in terms of CPU power and IO or analog inputs.
#+END_QUOTE

....I might have a Leonardo.  Time to scout around in the basement.

Didn't find a Leonardo.  Found a Pro Micro.  Does it work with that?

Well, MIDIUSB_write compiled, so maybe.

...uploaded.  And Windows said a lot of "Micro is ready for use" or similar.

Live lets me select 'Arduino Micro' as a MIDI in, so that's promising.
Sure enough, it gets notes on channel 0.  Bwahahahaha this is amazeballs.

Why do [[https://www.sparkfun.com/products/12640][they cost 18$]] tho.  Pi Picos are like 4$.
Thing is I don't want to have to deal with multiple platforms.
Anything the Arduino IDE can program I consider a single platform.
[[https://rootsaid.com/program-raspberry-pi-pico-using-arduino-ide/][I guess Pi Pico fits the bill]].

Teensys cost a little bit more, and have all but the physical part of the Ethernet port.
I've wanted that for other projects but for this one I don't care.
But I guess it is a point for Teensy.

Generally it seems the Pico is [[https://tutorial.cytron.io/2021/01/21/raspberry-pi-pico-vs-teensy-lc/][way more awesome]] than a Teensy, and also cheaper,
so it would be great if I could use them.

**** TODO What I/O pins does my Pro Micro have?

**** TODO Can a Raspberry Pi Pico be a USB controller?

Looks like so, though the examples I've found (e.g. [[https://blog.4dcu.be/diy/2021/05/20/MIDIpad.html][this 'MacroPad']]) all use Python.
I'm not sure if the Raspberry Pi board loaded into the Arduino IDE would be seamless
with the Arduino MIDI libraries!

https://forum.arduino.cc/t/midi-and-nano-rp2040/868310 suggests that it can be done,
but maybe nobody else has done it, yet.

[[https://www.pschatzmann.ch/home/2021/02/15/usb-midi-on-the-arduino-pico/][this guy may have done it]] with a different library

*** DONE find potentiometer

I sort of lucked out and found a RadioShack 500k-ohm 'Volume Control with Push Switch'
at the top of the box of Digi-Key stuff, which I was not expecting.

It says "For use with 16mm dimmer potentiometer with push switch", but
I think there's a lack of punctuation, there, and that
it *is* the potentiometer, and can be used with a 16mm knob.

Worth 12$ [[https://www.ebay.com/p/1800125731][on eBay]].

So what's the pinout on this thing?  /I'm guessing/ that the 3 pins on
the bottom are +, out, -, and the two long ones are connected when the
switch is pressed.  Where's my multimeter.

Alright I got it backwards.  The push-button toggles
whether that center small pin is connected to the right or left small pin.
Turning the knob varies the resistance across the two big pins
from 440k-ohms (when turned left) to 0 (when turned right).

It has clicks between, so wouldn't be good for a frequency cutoff knob,
but will do for demonstration of viability of this project, I suppose.
Seems roughly linear, with each clicking making a difference of about 16k-ohms.

*** TODO Hook potentiometer up to the pro micro and try to read it

*** DONE Can the Pro Micro talk Serial and MIDI over USB at the same time?  A: yes

Yes, it is doing so right now ("Sending note on" appears in serial monitor,
meanwhile Live is receiving note on/off events).

*** TODO Potentiometer research

**** Shaft type

What potentiometer knob types are there?
D-shaft seems pretty common?

Okay google, 'what is the most common shaft for potentiometer'

Google responded: "D Shaft"

Is 'WH148' a knob type?  Based on https://www.rcscomponents.kiev.ua/datasheets/20130416150848kls4-wh148.pdf I think it is used to mean a 6mm shaft.


https://lovemyswitches.com/news/what-knob-will-fit-on-my-gear/

#+BEGIN_QUOTE
Within the audio industry, you'll find that most smooth shaft pots measure 1/4" (6.35mm). You'll also see 6.0mm, but it's less popular.

Most 1/4" knobs fit will securely on a 6.0mm knurled shaft pot.
Again, a Potentiometer Adapter can be your friend here.
Slide on the adapter and voila!—a perfect fit for your 1/4" knob.
#+END_QUOTE

Okay, what's on Amazon?  Search for 10k-Ohm pots:

| Product               | Shaft type            | cost    | knobs | wires | Note                       |
|-----------------------+-----------------------+---------+-------+-------+----------------------------|
| [[https://smile.amazon.com/HiLetgo-Single-Joint-Potentiometer-Variable-Resistors/dp/B00MCK7JMS][HiLetgo WH148]]         | 6mm knurled?          | 9 / 20  | no    | no    |                            |
| [[https://smile.amazon.com/dp/B07QT1BTLJ][Dafurui something]]     | 6mm knurled           | 16 / 20 | yes   | no    |                            |
| [[https://smile.amazon.com/WGCD-Knurled-Linear-Rotary-Potentiometer/dp/B07B64MWRF][WMYCONGCONG]]           | 6mm knurled?          | 13 / 20 | yes   | no    | Amazon's choice! |
| [[https://smile.amazon.com/dp/B09MQN6MVB][Konohan blah blah]]     | 6mm knurled           | 13 / 4  | yes   | no    | Prettier ones!             |
| [[https://smile.amazon.com/RV24YN20S-Potentiometer-Inverter-Regulation-Control/dp/B0795R7PXZ][Taiss blah blah]]       | 6mm?  smooth, slotted | 12 / 2  | yes   | no    |                            |
| [[https://smile.amazon.com/TWTADE-3PCS-Potentiometer-XH2-54-3P-Connector-148-5kBK/dp/B082FCRQS2][TWTADE potentiometers]] | 6mm knurled           | 3 / 11  | yes   | yes   | come with wires!           |

Okay, so as far as 10k-ohm potentiometers from Amazon go, 6mm knurled seems to be the standard.

**** Resistance

10k?  idfklol.  But that seems common for [[https://www.sparkfun.com/search/results?term=potentiometer][potentiometers that SparkFun sells]].

V=IR.  5V = 10kOhm * 0.0005A.  Not too much power drain, there.  Also they'll most likely be behind multiplexers.

**** Linear/logarithmic

You can get logarithmic potentiometers, which would be good for volume control.
I just figure I'll map in software?  But maybe that would be good to have some log pots around.

*** TODO Shopping list

- [ ] Picos
- [ ] Potentiometers


*** TODO Wire up a button and have that do something in Live (enable/disable a device, maybe, or affect gain)
*** TODO Get some potentiometers (I might have one)
*** TODO Wire up multiplexer
