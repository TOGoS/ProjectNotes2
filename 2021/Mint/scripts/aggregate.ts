#!/usr/bin/env -S deno run

import {readLines} from "https://deno.land/std/io/bufio.ts";

class Aggregator {
	timestampLength : number|undefined = undefined;
	aggregated : any = undefined;
	outputs : any[] = [];

	recordPart(item:any, aggregated:any) {
		if( typeof item == 'object' ) {
			for( let k in item ) {
				if( k == 'timestamp' && aggregated == this.aggregated ) continue;
				
				if( aggregated[k] == undefined ) aggregated[k] = {};
				this.recordPart(item[k], aggregated[k]);
			}
		} else if( typeof item == 'number' ) {
			aggregated.min = Math.min(aggregated.min ?? item, item);
			aggregated.max = Math.max(aggregated.max ?? item, item);
			aggregated.total = (aggregated.total ?? 0) + item;
			aggregated.count = (aggregated.count ?? 0) + 1;
		} else {
			if( aggregated.values == undefined ) aggregated.values = [];
			aggregated.values.push(item);
		}
	}

	record(rec:any) {
		this.timestamp(rec.timestamp);
		this.recordPart(rec, this.aggregated);
	}

	timestamp(ts:string) {
		if( this.timestampLength ) {
			ts = ts.substr(0,this.timestampLength);
		}
		
		if( this.aggregated == undefined || ts != this.aggregated.timestamp ) {
			this.flushValues();
			this.aggregated.timestamp = ts;
		}
	}
	
	end() {
		this.flushValues();
	}
	
	flushValues() : void {
		if( this.aggregated != undefined ) {
			this.outputs.push(this.aggregated);
		}
		
		this.aggregated = {};
	}
	
	takeOutputs() : any[] {
		let outputs = this.outputs;
		this.outputs = [];
		return outputs;
	}
}

const agg : Aggregator = new Aggregator();

const TS_LENGTHS = {
	YEAR: 4,
	MONTH: 7,
	DAY: 10,
	HOUR: 13,
	MINUTE: 16
};

for (const arg of Deno.args) {
	if( arg == "--daily" ) {
		agg.timestampLength = TS_LENGTHS.DAY;
	} else if( arg == "--hourly" ) {
		agg.timestampLength = TS_LENGTHS.HOUR;
	} else if( arg == "--minutely" ) {
		agg.timestampLength = TS_LENGTHS.MINUTE;
	} else {
		throw new Error("Unrecognized argument: '"+arg+"'");
	}
}

function processRecord(rec:any) {
	agg.record(rec);
}

function line(line:string|null):void {
	if( line != null ) {
		const trimmed = line.trim();
		if( trimmed == '' || trimmed.startsWith('#') ) {
			return;
		}
		agg.record(JSON.parse(line));
	} else {
		agg.end();
	}

	const aggregated = agg.takeOutputs();
	for( const o in aggregated ) {
		console.log(JSON.stringify(aggregated[o]));
	}
}

for await (const l of readLines(Deno.stdin)) {
	line(l);
}

line(null);
