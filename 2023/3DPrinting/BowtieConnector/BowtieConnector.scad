// Outer dimensions of panel, in mm. 3"=76.2, 6"=152.4mm, 12"=304.8mm
bowtie_length    = 19.05;
thickness = 3.175;
margin    = 0.25;  // 0.01
// Size of holes; doesn't take margin into account; 12.2 printed with my regular Slic3r settings on my Kobra Max was found to fit 7/16" router bushings
hole_diameter = 12.2;
hole_grid_size = [38.1, 38.1];
// How many units to skip at corners
bowtie_position_offset = 1.0; // 0.5

// Countersunk in-between holes
// Counterbored hole surface diameter; 1/4" = 6.35
hole2_surface_diameter = 7;
// Counterbored hole shaft diameter; 3/16" = 4.7625
hole2_shaft_diameter = 5;
// Counterbored hole shaft diameter; 1/8" = 3.175
hole2_countersink_depth = 2.54;

corner_radius = 3.175;

module __end_parameter_list() { }

include <BowtieLib.scad>

// Bowtie connectors

translate([ bowtie_length*-3.0, 0, 0]) linear_extrude(thickness, center=false) bowtie_connector_2d(bowtie_length, -margin);

translate([ bowtie_length*-1.5, 0, 0]) linear_extrude(thickness, center=false) minimal_bowtie(bowtie_length, -margin, $fn=40);

translate([ bowtie_length* 0.0, 0, 0]) linear_extrude(thickness, center=false) semi_maximal_bowtie(bowtie_length, -margin, $fn=40);

translate([ bowtie_length* 1.5, 0, 0]) linear_extrude(thickness, center=false) maximal_bowtie(bowtie_length, -margin, $fn=40);

translate([ bowtie_length* 3.0, 0, 0]) linear_extrude(thickness, center=false) quarter_bit_cutout_bowtie(bowtie_length, -margin, $fn=40);

// TODO: Make a panel with 'maximal bowtie' cuts to see how that works out with 'minimal bowtie' connectors.
