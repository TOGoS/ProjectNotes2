** Hot and Savory

Which kind should I buy?

| Name            | Rating | Notes                           |
|-----------------+--------+---------------------------------|
| Mac and cheese  | *      | Bland                           |
| Tomato whatever |        | Okay as I recall                |
| Cajun past      | ****   | Surprisingly good and spicy     |
| Red curry       | ****   | Pretty good and spicy           |
| Green curry     | *      | Bland aside from pine-sol taste |
| Yellow curry    |        |                                 |
