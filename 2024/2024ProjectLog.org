#+TITLE: Misc. Project Notes, 2024

** Rehydration of Sharpie Oil-Based Paint Markers
:PROPERTIES:
:CUSTOM_ID: paint-marker-reydration
:END:

*** Notes from [2024-12-08] and earlier

As of [2024-05-25], I have tried rubbing alcohol, mineral spirits, and Xylene.

Mineral spirits seems to make things worse, causing what ink is left to clump.

Xylene seems to work the best.  Remove the wick and soak it for a while,
slosh it around, and maybe repeat a time or two to get it cleaned out.

Xylene even seems to help with the acrylic paint markers,
which I otherwise had given up on.

Acetone may also be worth a shot.  As of [2024-10-28] I have not tried it.

The tips of the wicks seem to dry out or clog up first
(they can get gummed up if you're dragging them across
surfaces with other substances on them).
Make sure to get the tip cleaned and rehydrated,
otherwise you might just thin the ink so that it runs out the sides
but the tip will still not wick very well.

I think the best way to keep these markers working is to
1. use them regularly,
2. avoid using them on oily surfaces
  - Sand, write label, then apply another coat
3. store them horizontally?

*** [2024-12-08] Trying Acetone

**** [2024-12-08T16:00] - Dropped the tips into acetone

And swirled it a bit.
Lots of ink came out into the acetone!

Gave them a few minutes.

Re-inserted the tips, held them down in the acetone
in attempt to get it up into the marker a bit,
since I did this with the mineral spirits and now
"need the acetone to fight the mineral spirits".

This definitely did something, at least in the short term.
I am able to paint with the paint markers, though it comes
out very runny, as if painting with water colors,
and the tip tends to drag the paint along with it
so that it builds up at the end of the stroke.

**** [2024-12-08T16:30] - More testing

The yellow marker is working okay, though thin.

The white one 'works' but is a bit 'clotty'.
I suspect due to mineral spirits up in there.
Maybe I'll give it another press in the acetone.

...

Now it's acting more like an eraser,
at least on this one smooth bit of plastic.
Maybe because that area got some mineral spirits on it.
Otherwise it's working about like the yellow one, now.

**** [2024-12-08T16:49] - More testing

I need more black non-porus surfaces to test on!

Yellow one seems to be working well, now.

White working well on some black PLA,
but still 'clotty and self-erasing' when used
on the bottom of one of my HDX organizers.
The yellow does not seem to have this problem.

The white flakes off of duct tape when I rub it with a finger.
The yellow does not.  Or at least not quite as quickly.

Maybe I just thinned the white too much,
or there was less ink in there to start with.

It works better on glass.

**** [2024-12-10] - Used yellow marker

to color the top of WSITEM-201178

**** [2024-12-11T11] - Yellow marker is dry again

*** TODO Re-test these markers in a few days
