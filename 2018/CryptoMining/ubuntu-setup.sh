#!/bin/bash

set -euo pipefail

node_name=""
while [ -z "$node_name" ] ; do
    read -p "Enter node name for nanopool, e.g. 'foo-miner-1': " node_name
done

eth_wallet_address=0x58a64c13cd00d2709b644f6771bac60b1fd463d8
sia_wallet_address=942d792e51b2457a33834ee76bf0652ac58f01b521bcd7ea341458285f6e175be38c4f08bbfc
email_address=togos00@gmail.com

claymore_tar_url=https://github.com/nanopool/Claymore-Dual-Miner/releases/download/v10.0/Claymore.s.Dual.Ethereum.Decred_Siacoin_Lbry_Pascal.AMD.NVIDIA.GPU.Miner.v10.0.-.LINUX.tar.gz
claymore_version=11.8
claymore_tar_skip_directory_count=1 # The 11.8 tar has an extra directory (with lots of spaces in the name) in it, so strip that out.

claymore_tar_basename=$(basename $claymore_tar_url)
downloads_dir=$HOME/downloads
claymore_tar_path=$downloads_dir/${claymore_tar_basename}
claymore_install_dir=$HOME/ext-apps/Claymore-${claymore_version}-test

echo "Installing Nvidia driver..."
echo "If you had different drivers installed already,"
echo "you may have to first $ sudo apt purge -y 'nvidia*'"
sudo add-apt-repository -y ppa:graphics-drivers
sudo apt update -y

sudo apt-get install -y \
     nvidia-384 \
     wget \
     libcurl4-openssl-dev # Claymore uses it, apparently

if [ -f "$claymore_tar_path" ]
then
    echo "$claymore_tar_path already exists; skipping download"
else
    echo "Downloading Claymore miner..."
    mkdir -p $downloads_dir && cd $_ &&
	wget -nc ${claymore_tar_url} -O ${claymore_tar_path}
fi
echo "Extracting claymore miner..."
mkdir -p ${claymore_install_dir} && cd $_ &&
    tar --strip-components=${claymore_tar_skip_directory_count} -xf ${claymore_tar_path}


echo "Downloading marlin..."
mkdir -p ~/downloads && cd $_ &&
    wget -nc https://fs.marvin.nuke24.net/uri-res/raw/urn:bitprint:C5PMCZEM726VDN6MMU6U2CTZDE6VILB2.FADNOG2C5UZYDAXFUJAD4JOGZCR3CXG5WSGUP7Y/marlin-1.0.0-linux-amd64.tar.gz
echo "Extracting marlin..."
mkdir -p ~/ext-apps && cd $_ &&
    rm -rf marlin &&
    tar -xf ~/downloads/marlin-1.0.0-linux-amd64.tar.gz &&
    mkdir marlin-1.0.0 &&
    mv marlin marlin-1.0.0/



cd ${claymore_install_dir}

claymore_runner_script=${claymore_install_dir}/run
echo "Writing Claymore runner script, $claymore_runner_script"
cat <<EOF > "$claymore_runner_script"
#!/bin/bash

node_name=${node_name}
eth_wallet_address=${eth_wallet_address}
sia_wallet_address=${sia_wallet_address}
# sia_pool="stratum+tcp://sia-us-east1.nanopool.org:7777"
# sia_username="${sia_wallet_address}/${node_name}/togos00@gmail.com"
sia_pool=stratum+tcp://us-east.siamining.com:3333
sia_username="${sia_wallet_address}.${node_name}"

cd "\$(dirname "\$0")"

./ethdcrminer64 \\
    -epool eth-us-east1.nanopool.org:9999 \\
    -ewal \${eth_wallet_address}.\${node_name}/togos00@gmail.com \\
    -epsw x \\
    -dcoin sia \\
    -dpool "\${sia_pool}" \\
    -dwal 942d792e51b2457a33834ee76bf0652ac58f01b521bcd7ea341458285f6e175be38c4f08bbfc/\${node_name}/togos00@gmail.com \\
    -dpsw x -ftime 10 \\
    -li 5 \\
    "$@"
EOF
chmod +x "$claymore_runner_script"


cat <<EOF >epools.txt
POOL: eth-us-west1.nanopool.org:9999, WALLET: 0x58a64c13cd00d2709b644f6771bac60b1fd463d8.${node_name}/togos00@gmail.com, PSW: x, WORKER: , ESM: 0, ALLPOOLS: 0
POOL: eth-eu1.nanopool.org:9999, WALLET: 0x58a64c13cd00d2709b644f6771bac60b1fd463d8.${node_name}/togos00@gmail.com, PSW: x, WORKER: , ESM: 0, ALLPOOLS: 0
POOL: eth-eu2.nanopool.org:9999, WALLET: 0x58a64c13cd00d2709b644f6771bac60b1fd463d8.${node_name}/togos00@gmail.com, PSW: x, WORKER: , ESM: 0, ALLPOOLS: 0
POOL: eth-asia1.nanopool.org:9999, WALLET: 0x58a64c13cd00d2709b644f6771bac60b1fd463d8.${node_name}/togos00@gmail.com, PSW: x, WORKER: , ESM: 0, ALLPOOLS: 0
EOF

cat <<EOF >dpools.txt
POOL: stratum+tcp://sia-us-west1.nanopool.org:7777, WALLET: 942d792e51b2457a33834ee76bf0652ac58f01b521bcd7ea341458285f6e175be38c4f08bbfc/${node_name}/togos00@gmail.com, PSW: x, WORKER: , ESM: 0, ALLPOOLS: 0
POOL: stratum+tcp://sia-eu1.nanopool.org:7777, WALLET: 942d792e51b2457a33834ee76bf0652ac58f01b521bcd7ea341458285f6e175be38c4f08bbfc/${node_name}/togos00@gmail.com, PSW: x, WORKER: , ESM: 0, ALLPOOLS: 0
POOL: stratum+tcp://sia-eu2.nanopool.org:7777, WALLET: 942d792e51b2457a33834ee76bf0652ac58f01b521bcd7ea341458285f6e175be38c4f08bbfc/${node_name}/togos00@gmail.com, PSW: x, WORKER: , ESM: 0, ALLPOOLS: 0
POOL: stratum+tcp://sia-asia1.nanopool.org:7777, WALLET: 942d792e51b2457a33834ee76bf0652ac58f01b521bcd7ea341458285f6e175be38c4f08bbfc/${node_name}/togos00@gmail.com, PSW: x, WORKER: , ESM: 0, ALLPOOLS: 0
EOF


marlin_dir=~/ext-apps/marlin-1.0.0
marlin_runner_script="${marlin_dir}/run"
echo "Writing Marlin runner script, ${marlin_runner_script}"
cat <<EOF >"${marlin_runner_script}"
#!/bin/bash

cd "$(dirname "$0")"

# cards=0,1,2 -- pass to -d if you want to only mine on some cards
sia_wallet_address=${sia_wallet_address}
node_name=${node_name}

# siaminer.com
exec ./marlin \\
     -u "\${sia_wallet_address}.\${node_name}"
EOF
chmod +x "${marlin_runner_script}"


read -p "I'm a reboot.  Press [Enter] to continue. "
sudo reboot # Reboot!
