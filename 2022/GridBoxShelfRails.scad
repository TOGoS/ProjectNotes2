// Purpose: provide rails that can be mounted inside 18" cubes
// (1/2" thick walls) upon which shelves (16.5" square) can sit.

inch = 25.4;
grid = 25.4*1.5; // i.e. 38.1

beam_thickness = 0.75*inch;
beam_width     = 1*grid;
wall_thickness = 1/4*inch;
beam_length_grids = 11;
counterbore_depth = 3/15*inch;
counterbore_diameter = 7/8*inch;

difference() {
	cube([beam_thickness, beam_width, 16.5*inch], center=true);
	for( z=[-beam_length_grids/2 + 0.5 : 1 : beam_length_grids/2 - 0.5] ) {
		translate([beam_thickness/2,0,z*grid]) {
			rotate([0,90,0]) {
				cylinder(h = beam_thickness*3, d=5/16*inch, center=true);
				cylinder(h = counterbore_depth*2, d=counterbore_diameter, center=true);
			}
		}
	}
	for( z=[-beam_length_grids/2 + 1.5 : 2 : beam_length_grids/2 - 1.5] ) {
		cube_width = 1.5 * inch;
		translate([beam_thickness/2,0,z*grid]) {
			cube([(beam_thickness-wall_thickness)*2, beam_width*2,1*grid], center=true);
		}
	}
}
