## Watch out for galvanic corrosion!

http://www.zygology.com/cms/upload_area/pdf/Zyg-Anodic-Index.pdf

	"All dissimilar materials have the potential to react with
	each other when they are brought together in the presence of a
	catalyst. In most circumstances this reaction is so mild as to
	be of no importance, but when the wrong materials are used in
	combination and then exposed to an electrolyte such as water
	the effects can become much more noticeable.  For applications
	where humidity is low then you can generally ignore galvanic
	corrosion. If on the other hand you are working on a project
	for use in damp conditions or outside then it is best to pay
	some regard to the possible effects of galvanic corrosion.

	 As a rule it is best to use metals which are as close
	 together as possible in the table below. Doing this will help
	 to eliminate any possibility of galvanic corrosion.

	 If you do have to mix materials then take a look at the
	 galvanic compatibility section"


## Mah wires an stuff

- Solid wiring: copper
- 2" #6-32 screws: zinc
- Hex nuts: zinc-plated [steel?] : https://smile.amazon.com/gp/product/B0106O7V06
- Aluminium bars: aluminium
- Solder: tin?
- Terminals - tin plated copper?


## Mini-PV and compatible-ish

The usual 1/10" (i.e. 2.54mm) pitch connectors that
I use a lot of are called 'Mini-PV' or (historically) 'DuPont' connectors.

This guy describes different variations.
'Mini-PV' is apparently the name brand, and good quality.
The 'dupont' connectors you get from China are garbage according to him.
http://www.mattmillman.com/info/crimpconnectors/dupont-and-dupont-connectors/

See also:
- https://en.wikipedia.org/wiki/Pin_header
- https://en.wikipedia.org/wiki/Berg_connector
- [Molex KK 254 connectors](https://web.archive.org/web/20220121112340/https://www.molex.com/molex/products/family/kk_254_rpc_connector_system)
  - https://www.content.molex.com/dxdam/literature/987651-0403.pdf


### KK-254

Possibly pin-compatible-with-dupont polarized wire-to-board connector with 1-walled shroud.

e.g. https://www.digikey.com/en/products/detail/molex/0022232021/26667

'KF2510' seems to be an easier-to-find name for...'KK-254-style' connectors?

'WM4202' is what my dad has several of in a box.
Google for that brings me to....https://www.digikey.com/en/products/detail/molex/0022232041/26671,
which says 'Series: KK254, 6373'


https://www.mattmillman.com/info/crimpconnectors/#kk100 doesn't like them so much,
recommending instead Molex SLs.

Q: Pin-compatible with 'dupont'?
Dad has some; I could try making sure they can mate.


### Molex SL

"If I were to start over, I would probably use these connectors in
place of several others I commonly use, this is because it is truly a
“do-it-all” connector family."
-- https://www.mattmillman.com/info/crimpconnectors/#sl




## MC4

For solar panel wiring, use 'MC4' connectors.
Ones that split are readily available.
Electrical male (looks like the socket from the outside) goes on the negative side of each panel
and electrical female (looks like a plug on the outside) goes to the positive side,
so you can think of current flowing out of the pointier
(electrically female, which could be confusing) connectors.

## Mini-Fit Jr

The ATX power supply things are 'Mini-Fit Jr'.
While I was building the mining rig I thought these were cool
but they're kind of expensive and I don't think there's any reason
to use them except to interface with PC power supplies and components,
and it's easier to just buy a cable and splice things onto it than to go
buy a crimp tool, etc.


## JST

There are many different 'JST' connector types.
For small little things that need only 2 small-guage wires, I sometimes use JST RCY.
For 12V applications I'll usually use a 2.1mm barrel connector instead.

JST SM is funny-looking due to plastic locks, and is commonly used for EL wire.

JST-RCY has 2.50mm pin spacing (pitch), but that seems to be
[close enough](https://piccouch.appspot.com/3af94973-ea17-46de-b7da-052bd956ff62)
for compatibility with Dupont/Mini-PV's 0.1" connectors.

Pix on [the article on nuke24.net](http://www.nuke24.net/docs/2019/JSTRCYMiniPVCompatibility.html).


## Deans

Cost: 0.21$/each (so 0.42$ for a male-female pair)
[on eBay](https://www.ebay.com/itm/20-x-Pairs-Ultra-T-Plug-Male-and-Female-Connectors-fit-Deans-Style-Lipo-USA-SHIP/322602172588?ssPageName=STRK%3AMEBIDX%3AIT&_trksid=p2060353.m2749.l2649)

Conventionally positive is on the 'wide' (top part of the "T") connector, negative on the 'tall' part.
Battery is female, load is male, which makes sense because you don't want the battery shorting out.

You can order them in [kits that come with tubing](https://smile.amazon.com/UEETEK-T-Plug-Connectors-Female-Battery/dp/B072Q9SFNR)
but sometimes the included tubing is all black
and it's probably chepaer to order the connectors and tubing separately.


## SAE

https://en.wikipedia.org/wiki/DC_connector#SAE_connector

'typically used for applying a maintenance charge to a vehicle battery'

They're hermaphroditic, so technically you only need to get one kind of connector.
In practice they tend to come wired with red and black wires,
and you probably want the colors to match your positive and negative
wires, respectively.

Cost: 2$ each (4$ per pair) [on eBay](https://www.ebay.com/itm/5pcs-Battery-Tender-SAE-DC-Power-Automotive-DIY-Connector-Cable-2x0-75mm-300mm/183205653090?hash=item2aa7e86a62:g:sYMAAOSw-YZa6YWR).
So it might be useful to have a few SAE-Deans adapters to wire things up to battery chargers,
but otherwise I plan to stick with Deans as my standard power connector for now. -- TOGoS, 2018-06-13


## Anderson Powerpole Connector

Similar use case as Deans but seems popular for use on fancy DC power splitters.

https://smile.amazon.com/dp/B074QMRBPB/ref=sspa_dk_detail_6?psc=1

Commenters on https://hackaday.com/2016/12/21/so-wheres-my-low-voltage-dc-wall-socket/ love it.

```
They click together like LEGO blocks. You want a 100A circuit but you
only have the 30A powerpole connectors? Use 6 of them in a 3×2
formation to create your plug. Best thing is if someone then comes
along with a device that only needs 10A they can still plug it in.
```

(Related HN discussion: https://news.ycombinator.com/item?id=21803996)

This page has some nice pictures showing how they mate:
https://smile.amazon.com/Powerpole-Connectors-Assortment-Disconnect-Unassembled/dp/B07CB121LL

"The ultimate connection."

There is a standard +/- configuration that, if followed,
physically blocks incorrect polarity, and also is color-coded.

Powerpole-adjacent stuff on Amazon includes battery charging modules.
e.g. https://smile.amazon.com/dp/B0794V58BK


### How to install Anderson Powerpole connectors

https://www.youtube.com/watch?v=QzLvdR6X81k

"Some of the ameteur radio groups and emergency response groups have adopted this orientation
with the hoods going over the top and the red on the left and the black on the right."

He strongly recommends using a proper crimper.
He uses a 'tri-crimp' by West Mountain Radio.


## Small screw terminals

I use #6-32 studs and ring or spade terminals for a lot of things.


## Battery screw terminals

```
5/16" is the standard battery post but; I always buy 3/8" eye
holes on the hi-press lugs to make it easy to use buss bars or other
termination points that commonly use 3/8" - 18 thread as a
standard. Just use 5/16" flat washers with the 5/16" studs and it will
be the right procedure .
```

-- https://www.amazon.com/ask/questions/Tx1IIQY05XQIL14/ref=ask_dp_dpmw_al_hza

As evidence that this is true, one of my batteries has 5/16"-18 studs.


## Spade / 'quick-disconnect' terminals

As in the connectors where male is just a flat plate, and female is one that bends around to slide on tightly.

They come in red, blue, yellow, which are different sizes.

The inside of my furnace control box uses these,
so I used them for inline connectors to the fan, also.

![Blue female spade connector next to matching male stud inside furnace control box](http://picture-files.nuke24.net/uri-res/raw/urn:bitprint:D7QHF4MCXH2XVTCRGZENN4YI54ROW5AU.HILX3VMPIQEU44WKUKTUKDXSBZGWDBWE2OLQKDA/133334-IMG_3630-cropped.JPG)


### Busbars

These might make for the simplest and cheapest busbars that I've yet imagined.
A busbar with male spade connectors could be just a flat piece of metal with notches
for the female connectors to fit around.
If I had 1mm aluminium sheet I could mill one.
Could use something like [this 5mm-wide ribbon](https://smile.amazon.com/dp/B07KRWB885),
though I'd probably want something wider.

You can buy busbars, also, though they tend to be more complicated than what I hav in mind:

- https://www.carbuildersolutions.com/uk/14-male-spade-terminal-triple-busbar


### Sizing

- I think blue is "16-14 gauge" - is that the wire or the spade?
- Also the gauge used by the little batteries, according to
  [my Amazon review](https://smile.amazon.com/gp/customer-reviews/R2P2PAIH87GNOB),
  which says they're 5mm wide (about 1/5") and 1mm thick (about 1/25").
- "16 gauge" is 1/16", so maybe that's the spade thickness?


## Other connectors

This page has good info for some common connectors:
http://tech.mattmillman.com/info/crimpconnectors/


## IEC 60320

References:
- https://www.plugsocketmuseum.nl/PowerCord1.html (includes polarity!)
- https://en.wikipedia.org/wiki/IEC_60320
- https://upload.wikimedia.org/wikipedia/commons/0/0c/IEC_60320_plugs.jpg

Plugs are odd, ilnet jack is that +1.
Reversed for outlet ports, which are less common.

Commonest for me are C5/C6 ('mickey mouse'), C7/C8 ('lamp connector'), and C13/C14 ('computer').

> IEC 60320 Appliance couplers for household and similar general
> purposes is a set of standards from the International
> Electrotechnical Commission (IEC) specifying non-locking connectors
> for connecting power supply cords to electrical appliances of
> voltage not exceeding 250 V (a.c.) and rated current not exceeding
> 16 A. Different types of connector (distinguished by shape and size)
> are specified for different combinations of current, temperature and
> earthing requirements. Unlike IEC 60309 connectors, they are not
> coded for voltage; users must ensure that the voltage rating of the
> equipment is compatible with the mains supply. The standard uses the
> term coupler to encompass connectors on power cords and power inlets
> and outlets built into appliances.

- P = pin count

| Plug | Inlet | Amps | P | Description / usage notes                                         |
|------+-------+------+---+-------------------------------------------------------------------|
|   C1 |    C2 |  0.2 | 2 | Oval; electric shavers                                            |
|   C3 |    C4 |  2.5 | 2 | Keyed                                                             |
|   C5 |    C6 |  2.5 | 3 | Mickey mouse; monitors/laptops                                    |
|   C7 |    C8 |  2.5 | 2 | Fans, 'lamp connector'                                            |
|   C9 |   C10 |  6.0 | 2 | Rectangular                                                       |
|  C11 |   C12 | 10.0 | 2 | Rectangle, keyed                                                  |
|  C13 |   C14 | 10.0 | 3 | The common computer/monitor inlet                                 |
|  C15 |   C16 | 10.0 | 4 | High-temp version of C13/C14 w/ additional pin; keyed 'A' variant |
| C15A |  C16A | 10.0 | 4 | Higher-temp version of C15/C16; additional keying                 |
|  C17 |   C18 | 10.0 | 2 | Ungrounded variant of of C13/C14; Xbox One                        |
|  C19 |   C20 | 16.0 | 3 | Rectangular, sideways pins, higher-current                        |
|  C21 |   C22 | 16.0 | 3 | C13/C14 shaped, sideways pins, high temperature                   |
|  C23 |   C24 | 16.0 | 2 | Rectangular, sideways pins; ungrounded variant of C19/C20         |

### C7/C8 - unpolarized 'lamp cord'

Max current: 2.5A

### Variant of C7/C8 with square side

"the most common wiring appears to connect the squared side to the neutral, and the rounded to the hot line"
-- https://www.wikiwand.com/en/IEC_60320

### C5/C6 - 'Mickey Mouse Connector'

Max current: 2.5A

I have found these on some monitors and laptop power supplies.

Polarity: Looking at the inlet (C6), with the 'ears' on the bottom, hot, ground, neutral.

Sometimes 'hot' is referred to as 'live'.

### C13/C14 - Standard PSU inlet

Max current: 10A

Polarity: Looking at inlet (C14), with the flat side on bottom (this seems to be the most common way to draw it),
pinout from left to right is is hot, ground, neutral.  This is similar to C6.

That is, according to https://cdn.instructables.com/FFJ/JLT1/IYB2EI0X/FFJJLT1IYB2EI0X.LARGE.jpg,
referenced by https://www.instructables.com/id/Wire-Up-a-Fused-AC-Male-Power-Socket/.
