** Measurements

*** Desk

- Current desk dimensions are: 63" wide, 28+3/4" high, 31+1/2" deep
- Desk is 31.5" deep, so you can ahead and make a 24"-long macro-TOGRack-mountable thing
  (but make sure it can be rotated into place under the desk!)
- Gap between rails and desk is a little less than 1/2" tall
- About 14+1/4" between rail centers, rails are about 1+1/4" wide

*** Cabinet

Shelf peg holes in the sides of the IKEA cabinet are almost 1/8" in diameter -- they're probably 3mm

*** Monitors

Dell monitors: apparently have 4" hole spacing?
Oh maybe it's 100mm.
The space the mounting panel has to fit into is about 4+3/4" wide and maybe 5/16" deep.

*** Brick column between windows

is about 16.5" wide.

*** Power strip

- 4.5" between mounting holes on back of power strip
- 9/32" and 5/32" are hole and slot diameter

*** Ethernet switch

- The ethernet switch takes 12V, 1.5A.  So I could replace the power supply
  with a single large 12V supply (probably want at least 5A=60W capacity) that
  could also power any lamps or whatever that I have.
  Hey check it:
  - https://www.amazon.com/dp/B073QTNF9F - 16$, 60W, includes connectors, good reviews - ordered 1
  - https://www.amazon.com/dp/B011TDKRFO - 22$, 60W, bare wire but looks more 'industrial'
  - https://www.amazon.com/dp/B01EWG6YT8 - 18$, 360W, screw terminals, noisy fan - ordered 2!

** TODO Make 24" Macro-TOGRack board for mounting power strips to

- Needs to be <1/2" thick to fit into gap between desk and rails

- Can have Macro-TOGRack mounting holes just in case I ever want to mount it,
  but really it'll just sit under the desk

Attachment points for:
- Power strip
- Zip ties to attach rando wires, etc
- GridBeam stuff!

My #6 screws have 1/4" (0.25") heads and 1/8" shafts, so may fit the power strip mounting holes.

** TODO bring to work

- [ ] Some more bolts (cube is several 3" bolts short)

