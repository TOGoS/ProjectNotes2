inch = 25.4;

beam_color = "#888060";
french_cleat_color = "#807060";
panel_color = "#208070";

module french_cleat_symmetrical(length, nominal_height) {
	rotate([0,90,0]) rotate([0,0,90]) {
		linear_extrude(length, center=true) {
			polygon([
				[-3/8*inch, +nominal_height/2+3/8*inch],
				[+3/8*inch, +nominal_height/2-3/8*inch],
				[+3/8*inch, -nominal_height/2-3/8*inch],
				[-3/8*inch, -nominal_height/2+3/8*inch],
			]);
		}
	}
}

// z=0 being the top center hole line
module french_cleat(length, nominal_height) {
	rotate([0,90,0]) rotate([0,0,90]) {
		linear_extrude(length, center=true) {
			polygon([
				[-3/8*inch, +9/8*inch],
				[+3/8*inch, +3/8*inch],
				[+3/8*inch, -nominal_height + 3/4*inch],
				[-3/8*inch, -nominal_height + 3/4*inch],
			]);
		}
	}
}

module gridbeam_hole(depth) {
	translate([0,0,-1]) cylinder(h=depth+1, d=5/16*inch);
}

module counterbored_gridbeam_hole(depth, counterbore_depth=1/8*inch) {
	cylinder(h=depth, d=5/16*inch);
	cylinder(h=counterbore_depth*2, d=7/8*inch, center=true);
}

module rail() {
	color(beam_color) difference() {
		cube([1.5*inch, 1.5*inch, 16.5*inch], center=true);
		for( z=[1.5-16.5/2 : 1.5 : 16.5/2-1.5] ) {
			translate([0,0,z*inch]) rotate([0,90,0])
				cylinder(h=2*inch, d=5/16*inch, center=true);
		}
		for( z=[3/4-16.5/2 : 1.5 : 16.5/2-3/4] ) {
			translate([0,3/4*inch,z*inch]) rotate([90,0,0])
				counterbored_gridbeam_hole(2*inch, 1/2*inch);
		}
	}
	
}

module panel() {
	color(panel_color) {
		rotate([90,0,0]) difference() {
			minkowski() {
				cylinder(h=3/8*inch, r=3/8*inch);
				cube([(12-3/4)*inch, (3-3/4)*inch, 0.1], center=true);
			}
			for( xy=[[-5.25,-3/4], [+5.25,-3/4], [-5.25,+3/4], [+5.25,+3/4]] ) {
				translate([xy[0]*inch, xy[1]*inch, 0]) cylinder(h=1*inch, d=5/16*inch, center=true);
			}
		}
	}
}

module rack() {
	for( x=[-5.25*inch, 5.25*inch] ) {
		translate([x, 0, 0]) {
			rail();
		}
	}
	
}

module back_board() {
	difference() {
		cube([6*inch, 3/4*inch, 16.5*inch], center=true);
		for( y=[1:1:10] ) {
			translate([2.25*inch, 3/8*inch, y*1.5*inch - 16.5/2*inch]) {
				rotate([90, 0, 0]) counterbored_gridbeam_hole(1*inch);
			}
		}
		// TODO more holes lol
	}
}

module back_french_cleat(nominal_height=3*inch) {
	color(french_cleat_color) difference() {
		french_cleat_symmetrical(length=42*inch, nominal_height=nominal_height);
		for( z=[-3/4*inch, +3/4*inch] ) {
			// Don't counterbore the end holes,
			// since their fronts attach to the racks!
			// And their backs may yet have a slim
			// board behind them, too.
			for( i=[0,27] ) {
				translate([(i+0.5)*1.5*inch - 21*inch, 3/8*inch, z]) {
					rotate([90,0,0]) {
						//counterbored_gridbeam_hole(1*inch);
						gridbeam_hole(1*inch);
					}
				}
			}
			for( i=[0,3,6,9,12,15,18,21,24,27] ) {
				translate([(i+0.5)*1.5*inch - 21*inch, -3/8*inch, z]) {
					rotate([-90,0,0]) {
						counterbored_gridbeam_hole(1*inch);
						//gridbeam_hole(1*inch);
					}
				}
			}
		}
	}
}

translate([(-21+0.75)*inch, 0, 0]) rotate([0,0,90]) {
	rack();
	translate([0, -3/4*inch, -6.75*inch]) panel();
	translate([0, -3/4*inch, +6.75*inch]) panel();
}
//translate([(-21+3)*inch, (6+3/8)*inch, 0]) back_board();
//translate([(+21-3)*inch, (6+3/8)*inch, 0]) scale([-1,1,1]) back_board();
translate([(+21-0.75)*inch, 0, 0]) rotate([0,0,-90]) {
	rack();
	translate([0, -3/4*inch, -6.75*inch]) panel();
	translate([0, -3/4*inch, +6.75*inch]) panel();
}

translate([0, (6+3/8)*inch, +(2+1/4)*inch]) back_french_cleat(3*inch);
translate([0, (6+3/8)*inch, -(3+3/4)*inch]) back_french_cleat(3*inch);