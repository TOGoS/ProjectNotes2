// MiscMidblocks-v2.2
//
// Changes:
// v2.0:
// - Refactor to use goofy hole pattern spec list
// - Add a chonker block, intended as a gridbeam-mountable back for something
// v2.1:
// - Add 3/8" spacer for Renee
// v2.2:
// - Intersect Renee's spacers with the gridfinity block bottom for the lulz

// Unit used for *_bsunit
bsunit = "1/8*inch"; // ["mm", "inch", "1/8*inch", "1/16*inch", "1/32*inch", "1/64*inch", "inval"]
// Size of one small block
sblock_size_bsunits = 4;
lblock_size_bsunits = 12;
small_hole_size = 5;
number8_hole_size = 5.5;

length_sblocks = 3;
width_sblocks = 1;
thickness = 12.7;
cap_thickness = 3.175;

$fn = 20;

// Depth of all-the-way-through holes
infinity = 1000;

include <PhoneHolderLib.scad>;

bsunit_size = unit_value(bsunit);
sblock_size = bsunit_size * sblock_size_bsunits;
lblock_size = bsunit_size * lblock_size_bsunits;

// hole pattern =
//   [spacing, "straight", diameter]
//   [spacing, "countersunk", surface_d, neck_d, head_h, bore_d]

function min_corner_radius_for_hole_patterns(hole_patterns, i=0) =
	i >= len(hole_patterns) ?
		infinity :
		min(hole_patterns[i][0]/2, min_corner_radius_for_hole_patterns(hole_patterns, i+1));

module block(size, hole_patterns, max_corner_radius=999) {
	corner_radius = min(max_corner_radius, size[0]/2, size[1]/2, min_corner_radius_for_hole_patterns(hole_patterns));
	difference() {
		linear_extrude(size[2]) {
			difference() {
				rounded_rectangle(size, corner_radius);
				for( pattern=hole_patterns ) if( pattern[1] == "straight" ) {
					hole_spacing  = pattern[0];
					hole_diameter = pattern[2];
					size_sblocks = [round(size[0]/hole_spacing), round(size[1]/hole_spacing)];
					hole_positions = grid_cell_center_positions([size_sblocks[0]*hole_spacing, size_sblocks[1]*hole_spacing], tovec2(hole_spacing));
					if( hole_diameter > 0 ) for( pos=hole_positions ) {
						translate(pos) circle(d=hole_diameter);
					}
				}
			}
		}
		for( pattern=hole_patterns ) if( pattern[1] == "countersunk" ) {
			hole_spacing   = pattern[0];
			hole_surface_d = pattern[2];
			hole_neck_d    = pattern[3];
			hole_head_h    = pattern[4];
			hole_bore_d    = pattern[5];
			size_sblocks = [round(size[0]/hole_spacing), round(size[1]/hole_spacing)];
			hole_positions = grid_cell_center_positions([size_sblocks[0]*hole_spacing, size_sblocks[1]*hole_spacing], tovec2(hole_spacing));
			if( hole_surface_d > 0 ) for( pos=hole_positions ) {
				translate([pos[0], pos[1], size[2]]) countersunk_hole(hole_surface_d, hole_neck_d, hole_head_h, infinity, hole_bore_d);
			}
		}
	}
}

small_hole_straight_pattern = [sblock_size, "straight", small_hole_size];
small_hole_countersunk_pattern = [sblock_size, "countersunk", 8, 4, 2, 5];
large_hole_countersunk_pattern = [lblock_size, "countersunk", 1/2*inch, 1/4*inch, 1/8*inch, 5/16*inch];
number8_on_lblock_grid_pattern = [lblock_size, "straight", number8_hole_size];

// #6 = (* 25.4 0.138) = 3.5mm  ; and 5mm holes work well
// #8 = (* 25.4 0.164) = 3.96mm ; so 5.5mm?

pat1 = [small_hole_straight_pattern];
pat2 = [small_hole_countersunk_pattern];
pat3 = [small_hole_straight_pattern, large_hole_countersunk_pattern];
pat4 = [small_hole_straight_pattern, number8_on_lblock_grid_pattern];

translate([sblock_size*-8,0,0]) block([sblock_size*2, 3/4*inch, thickness], pat1);

translate([sblock_size*-4,0,0]) block([sblock_size*2, 5/8*inch, thickness], pat1);

translate([sblock_size* 0,0,0]) block([sblock_size*2, 1/2*inch, thickness], pat1);

translate([sblock_size* 4,0,0]) block([sblock_size*2, 3/8*inch, thickness], pat1);


translate([sblock_size*-8, sblock_size*4, 0]) block([sblock_size*2, sblock_size*2, cap_thickness], pat1);

translate([sblock_size*-4, sblock_size*4, 0]) block([sblock_size*3, sblock_size*2, cap_thickness], pat1);

translate([0, sblock_size*4, 0]) block([sblock_size*3, sblock_size*3, cap_thickness], pat1);

translate([sblock_size*4, sblock_size*4, 0]) block([sblock_size*2, sblock_size*2, cap_thickness], pat2);

// Spacer for Renee
translate([ sblock_size*3, -sblock_size*6, 0]) {
	intersection() {
		block([sblock_size*3, sblock_size*3, 1/4*inch], pat4, max_corner_radius=2);
		import("../Gridfinity/GridfinityBlockBottom-50mm-v1.0.stl");
	}
}

// Generic 4.5"x4.5"x1/4" back panel for tool holders
translate([-sblock_size*6, -sblock_size*6, 0]) block([sblock_size*9, sblock_size*9, 3/8*inch], pat3);
