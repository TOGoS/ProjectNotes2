#+TITLE: ESP8266 communication notes

[2020-05-05]

Thoughts on implementing simple (cheap and easy, slow speeds are acceptable)
communication between microcontrollers.

Original use case: control LED strips with built-in controllers,
allowing daisy-chaining.

Wanted to use Ethernet, but that might require additional chips.
Would prefer as few external components as possible!

** Options

|          | Pro                        | Con                                               |
|----------+----------------------------+---------------------------------------------------|
| WiFi     | Built-in to ESP8266 boards | Requires configuration (passwords, servers)       |
|          |                            | Requires higher-level protocol                    |
|----------+----------------------------+---------------------------------------------------|
| Ethernet | Ubiquitous cables          | Expensive to implement                            |
|          |                            | Requires higher-level protocol                    |
|----------+----------------------------+---------------------------------------------------|
| Serial   | Ubiquitous                 | Requires additional networking layer - maybe PPP? |
|          | Built-in to ESP8266 boards |                                                   |
|----------+----------------------------+---------------------------------------------------|
| I2C      | Built-in to ESP8266 boards | Requires configuration due to small address space |
|----------+----------------------------+---------------------------------------------------|
| 1-Wire   | Large address space        |                                                   |

See also: [[../../2014/1Wire.org][thoughts on 1-wire wiring from 2014]]

** Serial

Many chips have built-in UARTs

http://weigu.lu/microcontroller/tips_tricks/esp8266_tips_tricks/

https://diyi0t.com/what-is-the-esp8266-pinout-for-different-boards/

*** TODO What baud rates are widely supported?  (PIC, ESP, RPi, etc)
*** TODO What physical protocols (voltage levels, etc) are widely supported?
