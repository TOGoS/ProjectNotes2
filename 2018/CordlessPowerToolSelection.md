# Which Cordless Tool Brand Should I Buy?

## TL;DR

Best options seemed to be Milwaukee or Ryobi.
I decided to go with Ryobi because:

- They tend to have nice ergonomic features
  (magnetic bit holders, LED headlights on drills)
- They have affordable chainsaws
- Their tools are readily available at Home Depot
- Their tool selection is huge
- They seem pretty dedicated to making one of everything that works with the ONE+ 18V batteries

The downsides of Ryobi tools seem to be

- They're flimsy and won't hold up if you start abusing them a lot.

But I'm pretty used to keeping cheap tools working, so in my case they're probably fine.
I really like the idea of having a pile of spare batteries that can go with anything.

In cases where power and durability are important I'll buy wall-powered tools.

## Convo with Joh

Dan:
Which brand of cordless tools do you like?  I've never bought any but I figure if I do it'd be good to stick with one so the batteries are interchangeable.

Joh:
Milwaukee is probably the best. Ryobi is the cheapest and I like them too. They are kind of opposites. You should probably go with Milwaukee.

Dan:
:o thx.

Joh:
I don't like big bulky tools that don't fit in your hand.
Milwaukee has light weight tools that don't lose much power, but they cost a ton.
Ryobi is light, not as powerful, maybe not as durable, but extremely inexpensive.
So you could buy more Ryobi tools,. Like you could have 3 tools for $100 instead of one.

Dan:
Ryobi appears to win on inexpensive cordless chainsaws.

Joh:
But they would probably have less amps.

Dan:
Both Ryobi and Milwaukee seem to focus on 18V tools.
Do you own any?
I know Renee has a couple of drills but I forget which brand they are

Joh:
Only Ryobi, and it's fine.  I think Tim has a Milwaukee drill.
Milwaukee seems to be higher quality.
They both are lighter feeling than everyone else.
And that's a good thing I think.
Like they are not bulky.

Dan:
yeah if I want bulk I'll lug around a car battery and an inverter XD

Joh:
Now Milwaukee tries to achieve this lightness without making any compromises...other than price.

Dan:
http://www.thesawguy.com/ryobi-vs-milwaukee/  oh wow the price diff

Joh:
And Ryobi makes every compromise for it.  So a lot of times Ryobi's form factor will actually be the best.
Yep.
How many tools are you buying?
It makes you wonder what corners Ryobi is cutting lol.

Dan:
Not buying anything right now.  But I was tempted in the tool aisle at Walmart.
I only stopped myself because I wanted to make 'the brand decision' and hadn't yet.

Joh:
But I like Ryobi stuff too. Yeah.

Dan:
Yeah, I'm leaning towards Ryobi, just because we could buy one of those kits and just carry it around.
Or buy 3 of them.

Joh:
Don't unless you need it for something I guess.
150$ and you have your "tools to keep in car".
Well right.
But we're pretty gentle on our tools so I think it'd probably be okay.
Some guy on the internet would do a comparison where he drives 2 drills backwards and the one that catches fire loses.
Ryobi would lose that one, but we wouldn't do that to it.
I might be thinking of this guy https://www.youtube.com/watch?v=lJz00lYpbeo

Joh:
I would be just as happy with Ryobi.
But we would have to just be aware that they have weaknesses.
We could still buy one main Milwaukee drill too if we wanted.

Dan:
Looks like Milwaukee tools are up to about 50% more expensive,
but definitely higher quality and the warranty is like 2x as long.
Ryobi ones have some neat ergonomic features like having better headlights and bit holders on the drills
But since I try to plan for the apocalypse maybe I should get the better stuff
Still waiting for Renee's 2c though

Joh:
Yeah I like them both.
I can't really decide between the two.
Milwaukee is the best and Ryobi is the cheapest, sometimes it has random features too.
They both try to load themselves up with random features.

Dan:
We could split it by voltage.
Ryobi for 40V stuff like chainsaws, Milwaukee for 18V stuff like impact drivers.  :P
Not that particular split necessarily

Joh:
Yeah sure.
I do like 18v Milwaukee drills and impact drivers.


## Convo with Timmy at Home Depot

Timmy:
Ryobi stuff is great!
All the 18V stuff is interchangeable!
They make like so much different stuff!
There's even an 18V chainsaw!

Dan:
Yeah this is great.

## Convo with Renee

Renee:
It seems like milwaukee is the best.

Dan:
Yeah from what I've seen (youtube and otherwise)
Milwaukee tools are way sturdier.

But I'm still leaning towards the Ryobi ONE+ 18V system because it's
pretty much guaranteed that if you need a tool they have it in there,
and relatively inexpensively.

## Internal Monologue

DeWalt, Ryobi, Milwaukee, and probably others each have
batteries that are interchangeable between tools.

- DeWalt 20V MAX
- Milwaukee M18 FUEL
- Ryobi 18V ONE+

https://toolguyd.com/best-cordless-power-tool-brand/

On the one hand if I'm shelling out a bunch of money for a tool I would
prefer to know that it's not a flimsy piece of crap.

But if that's what I want I'd get a thing with a cord.

"Buy a tool with a cord, and like my dad with his Makita, you might still be using it in four decades from now."
-- https://hackaday.com/2017/08/07/the-trouble-with-cordless-power-tools/

"Ryobi owners I know (myself included) like that we can have complete compatibility between tools, batteries, and chargers."
-- https://toolguyd.com/why-wont-power-tool-brands-standardize-their-battery-packs/

Yeah I think that's me.

"I’ve thought a fair bit about this, and I believe that when the next
generation of battery technology emerges, there is a good chance that
a standardized battery format will emerge."
-- Guy on that same page

Makes me think Ryobi trying to fill the market with their interchangeable batteries
might be trying to set that standard.

OTOH people keep referring to 'Milwaukee/Makita/Dewalt' as the big standard players.
Have they seen all this Ryobi stuff?

"They are marketing to the people that will respond to marketing. Most
tradesmen or serious DIYer will buy what they think is the best tool
in their price range, regardless of the store displays. A novice or
first time home buyer that stumbles in the store having never owned a
power tool, will be caught by the displays. Those are the people that
usually will buy the absolute cheapest price point because "a drill is
a drill" and they can't tell the difference. That being said, my
current cordless setup is all Ryobi. I know they are at the low end,
but they are good enough for 90 percent of what I do and I have high
end(Milwaukee and Makita)corded tools, so worst case scenario I'm
running an extension cord. My priority with cordless is convenience,
not performance."
-- https://www.reddit.com/r/Tools/comments/6dzoad/ridgid_vs_ryobi/di6oywh/

I think I agree with that guy.
