Now known as [TOGRack](http://www/nuke24.net/docs/2018/TOGRack.html).

## Use cases

- Mounting components on a board
- Mounting components on a grid of 2x2s
- Mounting stuff on pegboard?

## v0.1 - Holes on 1" grid

- which means components mounted on 1" grid
- Cut edges of panels 3/8" from holes to make sure panels have some room
- mount with #6-32 screws, because I have lots
 - they can be electrical conductors to the back of the panel if needed. XD
- A 7" high panel would fit in a 4U rack,
  but anything smaller is odd.

Advantages:

- Components (without rear projections) could theoretically be mounted on pegboard!
  (if I come up with a way to attach things)

Disadvantages:

- Hard to make good use of 1.5" crossbeams, since hole centers would have to be only 1/4" from edges
  - I've made one, and with 5/31" screw holes this might not be that problematic
