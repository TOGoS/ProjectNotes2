$fn = 30;

box_width = 14;
box_depth = 20; // idk; however wide that board I cut was
box_inner_height = 12;

// Card-holding board that I cut is actually only about 3/8.
// Better to under than over-estimate it, so that cards
// are firmly against it.
// Can always put some washers under the brackets if they're a little loose.
card_board_thickness = 3/8;

panel_thickness = 1/2; // Either 1/2 or 5/8
box_height = box_inner_height + panel_thickness * 2;

standard_screw_hole_diameter = 5/32; // What I use for #6 screws

echo("Outer dimensions:", box_width, box_depth, box_height);

card_pcb_thickness = 1/16; // At le

motherboard_thickness = card_pcb_thickness; // probably the same
motherboard_width = 9.6;
motherboard_depth = 12;

//// Materials

tog_rail_color = [0.8,0.8,0.2];
wood_base_color = [0.6,0.4,0.3];
body_paint_color = [0.1,0.3,0.2]; // 'Hunter green'

function alter_color(c, a) = [c[0]+a, c[1]+a, c[2]+a];

// Make different boards look different
function jounce_color(color, a=0.2) = alter_color(color, rands(0,a,1)[0]);

module wooden() {
	//color(alter_color(wood_base_color, rands(0,0.2,1)[0]))
	color(jounce_color(body_paint_color, 0.1))
		children();
}

module metallic() {
	color([0.8,1,1]) children();
}

pcb_color = [0.2, 0.7, 0.2];
module pcbish() {
	color(pcb_color) children();
}


module black_plasticish() {
	color([0.2, 0.2, 0.2]) children();
}

////

module atx_motherboard() {
	pcbish()
		cube([motherboard_width, motherboard_depth, motherboard_thickness], true);
}

motherboard_standoff_length = 3/8;

/*
          - From bottom of bracket to bottom edge of lip = 4.75"
          - Total length of card = just over 10+1/8" (not including bracket lip)
          - Top of card sticks up another ~3/16" over bottom edge of bracket lip
          - 4+5/16" from bottom of connectors to top of card
*/

card_bracket_thickness = 1/24; // idk whatever
card_bracket_height = 4.75; // to just under lip; lip adds additional +thickness
card_bracket_width = 1+1/2; // measured; pcb is on the +y edge
card_bracket_lip_length = 1/2; // idk whatever
card_bracket_to_connector_dist = 1+13/16; // approx. based on measurement
card_connector_length = 3+3/8; // approx. based on measurement
// Vertical distance from bottom of connector to inside of bracket lip;
// should be the same for all PCI-e cards; measured from Nica's old graphics card.
card_connector_to_lip_height = 4+1/8;
card_connector_height = 1/2; // based on some internet pix; might be wrong
card_body_height = 3+7/8;
card_body_length = 10+1/8;
card_plastic_thickness = 1;
card_pcb_to_plstic_gap = 1/4;

// Distance from bottom of riser (including padding) to bottom of contained card connector
riser_to_connector_height = 5/16; // or maybe a little more

// Origin of cards is...bracket inner corner
// Don't forget that the power wires will stick up another inch.5 or so!
module gtx_1070() {
	metallic() {
		translate([-card_bracket_lip_length/2 + card_bracket_thickness/2,0,card_bracket_thickness/2]) {
			cube([card_bracket_lip_length, card_bracket_width, card_bracket_thickness], true);
		}
		translate([+card_bracket_thickness/2, 0, -card_bracket_height/2]) {
			cube([card_bracket_thickness, card_bracket_width, card_bracket_height], true);
		}
	}
	
	translate([
		card_bracket_thickness,
		card_bracket_width/2 - card_pcb_thickness/2,
		-card_connector_to_lip_height
	]) {
		translate([card_bracket_to_connector_dist + card_connector_length/2, 0, card_connector_height/2])
			pcbish() cube([card_connector_length, card_pcb_thickness, card_connector_height], true);
		// Body of card
		translate([
			card_body_length/2, 0,
			card_connector_height + card_body_height/2
		]) {
			pcbish() cube([card_body_length, card_pcb_thickness, card_body_height], true);
			translate([ 0, -card_pcb_thickness/2 - card_pcb_to_plstic_gap - card_plastic_thickness/2, 0] ) {
				black_plasticish() cube([card_body_length, card_plastic_thickness, card_body_height], true);
			}
		}
	}
}

module panel(dims, center=true) {
	echo("Flat board: ", dims[0], dims[1], dims[2]);
	cube(dims, center);
}

module base_board() {
	wooden() panel([box_width, box_depth, panel_thickness], true);
}

leg_width = 1.5;
leg_depth = 3/4; // legs are 1x2s!
leg_height = box_inner_height;
leg_bottom_z = 0; // ha ha jk
lower_leg_mounting_hole_pitch = 0.5;
lower_leg_mounting_holes_bottom_z = leg_bottom_z+0.5;
lower_leg_mounting_holes_top_z = leg_bottom_z + 5;
upper_leg_mounting_holes_bottom_z = lower_leg_mounting_holes_top_z + 0.5;
upper_leg_mounting_hole_pitch = 0.5;
lower_leg_mounting_hole_zs = [lower_leg_mounting_holes_bottom_z : 0.5 : lower_leg_mounting_holes_top_z];
upper_leg_mounting_hole_zs = [upper_leg_mounting_holes_bottom_z+0.5 : 0.5 : leg_height-0.5];
function lower_leg_mounting_hole_z(hole_number) = lower_leg_mounting_holes_bottom_z + lower_leg_mounting_hole_pitch * hole_number;
function upper_leg_mounting_hole_z(hole_number) = upper_leg_mounting_holes_bottom_z + upper_leg_mounting_hole_pitch * hole_number;

module board(dims, center=true) {
	echo("Board: ", dims[0], dims[1], dims[2]);
	cube(dims, center);
}

// Origin is bottom center of leg
module leg(upper_holes_x=0) {
	wooden() difference() {
		translate([0,0,leg_height/2]) cube([leg_width, leg_depth, leg_height], true);
		for( z=lower_leg_mounting_hole_zs ) {
			for( x=[-leg_width/2+0.5:0.5:leg_width/2-0.5] ) {
				translate([x,0,z]) rotate([90,0,0]) cylinder(d=standard_screw_hole_diameter, h=leg_depth*2, center=true);
			}
		}
		for( z=upper_leg_mounting_hole_zs ) {
			translate([upper_holes_x,0,z]) rotate([90,0,0]) cylinder(d=standard_screw_hole_diameter, h=leg_depth*2, center=true);
		}
	}
}

beam_depth = 3/4;
beam_height = 1.5; // Also 2x2s!

card_stiffener_beam_width = 3/4;
card_stiffener_beam_height = 3/4; // It's a 2x2!

card_stiffener_beam_z = upper_leg_mounting_hole_z(7);

fan_rail_width = 3/4;
fan_rail_height = 3/4;

// measured height of cpu fan off board is about 2+5/8.
// add 1/4 for margin
min_motherboard_clearance = 2+5/8 + 1/4;

// PS 3.39" x 5.91" x 6.69"
ps_depth = 6.69;
ps_width = 5.91;
ps_height = 3.39;
module power_supply() {
	// power input/output ports are on the small ends
	black_plasticish() cube([ps_width, ps_depth, ps_height], true);
}

// Maxumum height of any power supply we want to accomodate
max_ps_height = 4;

// 120mm Fans are 4+3/4" inches on a side, 1" thick;
// corner mounting holes are 4+1/8" apart, so 5/16" from edge

fan120_width = 4+3/4;
fan120_height = 4+3/4;
fan120_mounting_hole_spacing = 4+1/8;
fan120_thickness = 1;
fan120_mounting_hole_diameter = 3/16; // let's say

module fan120_mounting_holes() {
	for( x=[-1,2,+1] ) {
		for( z=[-1,2,+1] ) {
			translate([x*(4+1/8)/2, 0, z*(4+1/8)/2])
				rotate([90,0,0]) cylinder(h=2, d=fan120_mounting_hole_diameter, center=true);
			//translate([x*(3+5/8)/2, 0, z*(3+5/8)/2])
			//	rotate([90,0,0]) cylinder(h=2, d=fan120_mounting_hole_diameter, center=true);
		}
	}
}

module fan120() {
	black_plasticish() {
		difference() {
			cube([fan120_width, fan120_thickness, fan120_width], true);
			rotate([90,0,0])
				cylinder(h=2, d=fan120_width - 1/2, center=true);
			fan120_mounting_holes();
		}
	}
}

// Origin is the front, center of the rail
module tog_rail(length, depth=3/4, z0=-1/4, z1=1/4) {
	translate([0,depth/2,0]) {
		color(tog_rail_color) difference() {
			translate([0, 0, (z0 + z1)/2]) cube([length, depth, z1 - z0], true);
			for( x=[-length/2+0.5:0.5:+length/2-0.5] ) {
				translate([x,0,0]) rotate([90,0,0]) cylinder(h=depth*2, d=standard_screw_hole_diameter, center=true);
			}
		}
		echo("TOG Rail: ",length,"x",z1-z0);
	}
}

// Origin is the center of the rack at the front of the rails
module tog_rack(length, z0=-1/4, z1=1/4) {
	translate([0,0,-3.5/2+1/4]) tog_rail(length, z0=z0+3/2);
	translate([0,0,+3.5/2-1/4]) tog_rail(length, z1=z1-3/2);
}

fan_margin = 1;
module fan_spacing() {
	for( y=[-1:1:1] )
		translate([0,y*(fan120_width + fan_margin),0])
			children();
}

module fan_rail(fan_mounting_hole_offset_z=0) {
	fan_rail_dims = [fan_rail_width, box_depth - leg_depth*2, fan_rail_height];
	wooden() difference() {
		board(fan_rail_dims, true);
		// Random mounting holes between fans for whatever:
		for( y=[fan120_width/2+fan_margin/2:fan120_width+fan_margin:fan_rail_dims[1]-0.5] )
			for( s=[-1,1] )
				translate([0,s*y,0]) rotate([0,90,0]) cylinder(d=standard_screw_hole_diameter, h=2, center=true);
		fan_spacing()
			for( h=[-0.5:1:0.5] )
				translate([0, h*fan120_mounting_hole_spacing, fan_mounting_hole_offset_z])
					rotate([0,90,0]) cylinder(d=fan120_mounting_hole_diameter, h=2, center=true);
	}
}

module front_rail_screw() {
	translate([0,-box_depth/2,0])
		rotate([-90,0,0])
			metallic() cylinder(d=standard_screw_hole_diameter, h=1);
}
module back_rail_screw() {
	mirror([0,1,0]) front_rail_screw();
}
// Origin is [x,center of box,z]
module fb_rail_screws() {
	front_rail_screw();
	back_rail_screw();
}

// z = 0 is bottom of the box
module rig_and_components() {
	//cube([box_width,box_height,box_depth], true);
	translate([0, 0, -panel_thickness/2])
		base_board();
	translate([0, box_depth/2 - motherboard_depth/2 - 1/8, motherboard_thickness/2 + motherboard_standoff_length])
		atx_motherboard();
	leg_off_x = (box_width - leg_width) / 2;
	leg_off_y = (box_depth - leg_depth) / 2;
	leg_off_z = 0; // Since origin's at the base
	translate([-leg_off_x, -leg_off_y, leg_off_z]) leg(upper_holes_x=-3/8);
	translate([-leg_off_x, +leg_off_y, leg_off_z]) leg(upper_holes_x=-3/8);
	translate([+leg_off_x, -leg_off_y, leg_off_z]) leg(upper_holes_x=+3/8);
	translate([+leg_off_x, +leg_off_y, leg_off_z]) leg(upper_holes_x=+3/8);
	
	card_board_bottom_z = max(
		max_ps_height,
		3.5 + 1/4 + 3/4, // TOG rack!
		motherboard_standoff_length + min_motherboard_clearance
	);
	
	// TOG rail
	rack_space_height = card_board_bottom_z - beam_height;
	tog_rack_z = 3.5/2 + 4/16;
	translate([0, -box_depth/2+leg_depth, tog_rack_z]) {
		tog_rack(box_width,
			z0 = -3.5/2 - 2/8,
			z1 = card_board_bottom_z - tog_rack_z);
	}
	
	translate([0, 0, card_board_bottom_z]) {
		beam_off_y = leg_off_y - leg_depth/2 - beam_depth/2;
		// beamacrosses
		//translate([0, -beam_off_y, -beam_height/2])
		//	wooden() board([box_width, beam_depth, beam_height], true);
		translate([0, +beam_off_y, -beam_height/2])
			wooden() board([box_width, beam_depth, beam_height], true);
		// card-holding board
		translate([-3,0,card_board_thickness/2])
			wooden() panel([5, box_depth, card_board_thickness], true);
	}
	
	ideal_card_bracket_lip_bottom_z = card_board_bottom_z + card_board_thickness + card_connector_to_lip_height + riser_to_connector_height;
	card_bracket_lip_bottom_z = card_stiffener_beam_z + card_stiffener_beam_height/2;
	echo("Card bracket Z:", card_bracket_lip_bottom_z, " (", leg_height - card_bracket_lip_bottom_z, "from top");
	translate([-box_width/2 + card_stiffener_beam_width/2, 0, card_stiffener_beam_z ]) {
		wooden() board([card_stiffener_beam_width, box_depth - leg_depth * 2, card_stiffener_beam_height], true);
		fb_rail_screws();
	}
	
	// Cards
	translate([-box_width/2 + card_stiffener_beam_width, 3, card_bracket_lip_bottom_z]) {
		for( i=[0:5] ) {
			translate([0, i*3 - 10.5, 0]) gtx_1070();
		}
	}
	
	// PS
	translate([
		0,
		-box_depth/2 + ps_width/2 + leg_depth * 2 + 1/8,
		ps_height/2
	]) rotate([0,0,90]) power_supply();
		
	// Fans
	fan_rail1_z = upper_leg_mounting_hole_z(8);
	fan_rail0_z = fan_rail1_z - 4;
	fan_rail_distance = 4;
	translate([box_width/2 - fan_rail_width/2, 0, fan_rail0_z]) {
		fan_rail(-1/16);
		fb_rail_screws();
	}
	translate([box_width/2 - fan_rail_width/2, 0, fan_rail1_z]) {
		fan_rail(+1/16);
		fb_rail_screws();
	}
	translate([box_width/2 - fan_rail_width - fan120_thickness/2, 0, fan_rail0_z + fan_rail_distance/2])
		fan_spacing()
			rotate([0,0,90])
				fan120();
	
	
	// TODO: Handles on short ends for moving around!
	// TODO: Place to stick an SSD!
	// TODO: 3.5" rail!  maybe share rails with fans?
}

scale(25.4)
	rig_and_components();
	//fan120();