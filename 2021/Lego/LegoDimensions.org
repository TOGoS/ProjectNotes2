#+TITLE: Lego Dimensions

** Q: I want to combine Lego bricks with gridbeam.  Can it be done?

- 1 Gridbeam   = 1.5in = 38.1mm
- 1/3 gridbeam = 0.5in = 12.7mm
- 1 lego width = 8mm

I don't think there's any nice common multiple for legos and half-inch units.
Lowest is 1016mm, which is over a meter!  127 lego block widths, and 80 TOGRack units (40 inches).
So if I want to attach legos to gridbeam stuff, will need special adapters.

That said, 2.5mm is 63.5mm, only 0.5mm off from 64mm.
So small Lego assemblies could be attached to a TOGRack with two holes 8 lego bricks / 2.5in apart.
Of course the error will grow as you multiply, so trying to match up beyond 5in / 16 lego bricks
would probably result in mismatches too large to accommodate #6 screws.

Vertical holes are 3LU, or 4.8mm in diameter, so plenty big for a #6 screw, which is 9/64in or 3.3mm.
3/16in = 4.76mm, so a 3/16 drill bit will do the job if you don't care about
fitting Lego axles through them, since those are 4.78mm, according to
https://bricks.stackexchange.com/questions/9406/what-are-the-dimensions-of-a-technic-axle

This diagram would def be handy if I ever decide to try to 3D print any LEGO-compatible things: https://i.stack.imgur.com/npW0j.gif
Just remember that THE NUMBERS ARE IN Lego Units (1.6mm each), not millimeters!

There's also LDUs, which are 1/20 of a brick width, or 1/4 of a LU, or 0.4mm:
https://bricks.stackexchange.com/questions/691/what-are-ldus-and-how-do-they-relate-to-lego-bricks?noredirect=1&lq=1
