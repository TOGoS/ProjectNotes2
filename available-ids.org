#+TITLE: Available OIDs

Take from the first applicable category.
Commit and push changes to this file before IDs are official
to ensure no conflicts.

OID namespace is urn:oid:1.3.6.1.4.1.44848.260.

** Bins

- 3537-3539 - 6 quart bins for electronics

** Standards

*** French cleat standards

- 4115-4116 - French cleat standards?

** Gridbeam + gridbeam-compatible or otherwise adjacent stuff

- 100045-100049 - 4' gridbeams
- 100448-100449 - WSTYPE-4114 (edge centers on gridbeam boundaries) French cleats
- 100484-100489 - Shelves for gridboxes (9x12 or whatever)
- 100497-100499 - Deck stained gridbeam!
- 100823-100829 - 1/2" plywood gridbeam shelves

*** Imported gridbeam (WSBATCH-100320)

100334
100335
100336
# Maybe these are 'extra', given that I only bought 16:
100337
100338
100339

*** Gridbeam / panels / FCs

**** TOGThoms1

101301
101302
101303
101304
101305
101306
101307
101308
101309
101310
101311
101312
101313
101314
101315
101316
101317
101318
101319

**** Framey

101323
101324
101325
101326
101327
101328
101329
101330
101331
101332
101333
101334
101335
101336
101337
101338
101339

*** Gridblocks, etc

Or panels; whatever, just reserving this range for 'not quite gridbeam' stuff.

** Panels

- 100392-100399 - designs for gridbeam panels (including monitor mounts)
- 100408-100409 - Small monitor mounts and other gridbeam panels.
  - 100400, 100401 are panels I want to CNC route, 2021-05-16
- 200109-200109 - green panels for 12" racks; 200104 was a power supply switch.

*** Hardboard panels

100940
100943
100944
100945
100946
100947
100948
100949

** Disposable

- 300003-300009 - Boxes of stuff going to the thrift store

** Everything else

- 100132-100139 - Mini-PV sleeve plugs
- 200096-200099 - ~34" shelf boards for WSITEM-200083, and some extra.
- 200119-200119 - French cleats made from that 3/4" x 2+1/2" plywood
- 200125-200129 - Straightish edges
- 200161-200169 - Capitalist-manufactured shelf units, especially but not necessarily curb-found ones.

** Joh's table parts

x 100060
x 100061
x 100062
x 100063
x 100064
x 100065
x 100066
x 100067
x 100068
x 100069
x 100070 - flat pine board
x 100071 - flat osb
- 100072
- 100073
- 100074
- 100075
- 100076
- 100077
- 100078
- 100079

** Orders

*** TOGThoms1

300153
300156
300157
300158
300159

*** Framey

300166
300167
300168
300169

** Misc items

3D prints, resin castings, whatever.

*** TOGThoms1

201560

201565
201566

201568
201569

*** Framey

201480

201486
201487
201488
201489
201490
201491
201492
201493
201494
201495
201496
201497
201498
201499

** Projects/tasks

*** TOGThoms1

201570
201571
201572
201573
201574
201575
201576
201577
201578
201579
201580
201581
201582
201583
201584
201585
201586
201587
201588
201589
201590

201593
201594
201595
201596
201597
201598
201599

*** Framey

201297
201298
201299

** Coat applications

*** TOGThoms1

300096
300097
300098
300099

*** Framey

300128
300129
300130
300131
300132
300133
300134
300135
300136
300137
300138
300139
300140
300141
300142
300143
300144
300145
300146
300147
300148
300149

** Garden/Plants

*** TOGThoms1

400029
400042
400043
400044
400045
400046
400047
400048
400049

*** Framey

400050
400051
400052
400053
400054
400055
400056
400057
400058
400059
400060
400061
400062
400063
400064
400065
400066
400067
400068
400069
