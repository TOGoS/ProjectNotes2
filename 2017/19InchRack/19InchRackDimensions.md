## Rack Dimensions

1U = 1.75"
Within 1U, screw holes are in the center and 0.25" from the top/bottom of space (5/8" (0.625") between holes)
(+ 0.25 0.625 0.625 0.25) = 1.75

Standard rail is 5/8" wide with centered screw holes,
making screw holes 5/16" from inner and outer edges of rail,
and 18+3/8" from each other.

Minimum width between rails: 17.75" (450mm) (actually it's just under 17.72", but 17.75" may be close enough)
Minimum width for panels: 19" (483mm)
(let ((rail-width (/ 5.0 8.0)))
  (+ 17.75 rail-width rail-width)) = 19

## Screw Sizes

Most common: 10-32 (#10 or ~3/16" with 32 threads per inch)
Sometimes (according to Wikipedia): 12-24

Stuff I found on Amazon was all 10-32.
