#!/usr/bin/env -S deno run

import {readLines} from "https://deno.land/std/io/bufio.ts";

function line(line:string):void {
	const trimmed = line.trim();
	if( trimmed == '' || trimmed.startsWith('#') ) {
		return;
	}
	
	const record = JSON.parse(line);
	const result:any = {};
	for( const k in record ) {
		const parts = k.split(/\//g);
		let subRec:any = result;
		for( let i=0; i<parts.length; ++i ) {
			let part = parts[i];
			if( i == parts.length - 1 ) {
				subRec[part] = record[k];
			} else {
				if( subRec[part] == undefined ) {
					subRec[part] = {};
				}
				subRec = subRec[part];
			}
		}
	}
	console.log(JSON.stringify(result));
}

for await (const l of readLines(Deno.stdin)) {
	line(l);
}
