# Mining rig (hardware)

I want to build this one out of wood, just because that's what we do here.

Requirements:

- Portable (so you can move it to somewhere you want heat)
- Fits on 19" rack shelf (i.e. should fit on wooden shelves near routers, etc)
- Fit on shelf above doorways and blow warm air downward?
- Lots of fans!
- Lots of LEDs!
- Screen or mesh around to keep cats out
- Probably some 3.5" racks inside for voltage meters etc
- Frame color: Maybe red, to go with red lights, and to make use of leftover red paint

Q: How to control LEDs?  Directly from computer?
3-way switch to provide alternate power to lights when PS is powered off?
(like for doing maintenance more than to look cool)

## Actual rig

Outer size is 14" deep x 20" long x 12" or 13" tall, according to [RigDesign.scad](./RigDesign.scad)
(also mentioned in [8020/beams.md](../8020/beams.md)),
which looks close enough to the actual rig to be convincingly authoritative.
beams.md says 12" tall, and I suspect that's true, given that the OpenSCAD design
clearly has an inch of extra space at the top which I may have decided to
omit in order to make the outer dimensions nicer.
