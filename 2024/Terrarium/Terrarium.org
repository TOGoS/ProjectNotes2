#+TITLE: Terrariums

By 'terrariums' I really mean indoor plant pots that are mostly
enclosed to keep humidity in, because I have some plants that I need
to bring inside during the winter, and they really dislike dry air.
They sometimes get very tall.

There are 'humdifity tents' you can buy, and I have done so,
but they are unwieldy in my cluttered basement.

I also want to be able to transport plants in a way that
protects them during the drive.

I'd like to be able to hang these on the wall, at least
smaller ones containing smaller plants.

Sometimes I will leave for several weeks at a time, and I want the
plants to not die of dehydration.  Growing them in a netpot
above a tank of water is one way to do this.  Another is to
have a timer controlling a valve or pump to water them regularly.
An aeroponic system that mists the roots might also work.
All of thes solutions require a container that holds water.

Problems
- Containing humidity
- Lighting
- Automation of lighting
- Stackability
- Cable management
- Management of water tubing, if pumping water
- Catching excess water
- Hang-on-wall-ability

** [2024-10-02] some thoughts

*** 3D-printed terrarium segments

These are intended to solve the humidity containment problem
for small, wall-mounted terrariums.

*** Wooden boxes

18" wide and deep by 36" tall, and the top/bottom could be mostly open
to allow these boxes to be stacked.  Then if your plant gets taller,
you can just stack another on.  16.5" panels could be bolted on
either inside or outside when a solid top/bottom is needed.

The front should be plexiglass or something, with a hinge.

5 gallon buckets and the plastic cat litter bins are both just a
little over 12" wide (the long way, in the case of the rectangular cat
litter buckets), so even 13.5" wide/deep boxes would work, to hold one
each, assuming 1/2" walls.  13.5" has the advantage that my Kobra Max
can print things that size (I'm thinking of TPU gaskets).

*** Store-bought grow tents

There are smaller ones. e.g. https://www.amazon.com/VIVOSUN-Hydroponic-Observation-Window-Growing/dp/B01DXYMQ9M,
which comes in a variety of sizes.

** [2024-10-03T18] - First large section printed, thoughts on size

WSITEM-201220 (13.5" x 9" x 4.5" segment) turned out very nice.

It occurs to me that two netpots would fit easily enough into
a 12" x 6" area, which might be a better (quicker to print,
less filament) size for prototyping the first one, and easier
to hang on the wall.

** [2024-10-03T21] - Proposed adjustments

I've been thinking about what I would need to change in order to bolt
these to a board on the wall, and have the bottom sectio nbe
strong enough that I'd trust it to hold water without tearing apart.

Due to the orientation of the layers, I think it would be best if the
whole thing sat on a shelf made of wood, rather than having it hanging
by some screws through, at best, the edges of the backside.

That simplifies the design a bit.

In case of a tall terrarium, I might still want to attach upper layers
to a back panel, just to keep the whole thing tipping.  To this end it
might be good if some of the segments' walls extended all the way to
the back (and other sides), rahter than stopping 1/8" short, as the
current design (TerrariumSegment0.7) does.

To secure them to the back panel, I might just hold them back with a strap.

For the water-holding section, I might want thicker walls.

And for putposes of holding a panel to hold netpots, I'm thinking
it would be much more convenient if this just sat inside
rather than being an additional layer that's bolted in.
This way it can be removed for cleaning rather than
the whole assembly having to be disassembled.

Some parameters to add:
- Inset of wall
- Thickness of walls
- Thickness of floor (0 for current 'no floor' shape)
- Top overhang

Of course, if I'm having to make these wooden almost-boxes,
then maybe I should just focus on making wooden boxes,
and put 6 quart sterilite bins (or other plant bots) in the bottom.

** Tasks

*** DONE Experiment to see if a watertight thing can be 3D-printed

Some folks at https://forum.prusa3d.com/forum/english-forum-general-discussion-announcements-and-releases/best-filament-for-long-term-submerged-prints/
recommend spray acrylic or spray primer to seal things.

OP also mentions that TPU is great underwater.

In which case, PLA might be fine, too.

...

Ended up printing in PETG and coating the inside of [[../../2018/StainTest/StainedItems.tef::WSITEM-201224][WSITEM-201224]] with
Rustoleum Spray Leak-Seal, which might have been not the best choice,
being-okay-for-plants-wise.  ChatGPT suggested that polyurethane might
be safer.

But I also slathered some stain+poly around the inside of [[../../2018/StainTest/StainedItems.tef::WSITEM-201145][WSITEM-201145]]
so that I could, someday (as of [2025-03-13] I haven't yet tried),
fill it with water and see how well it holds up.


*** DONE Design lid for terrarium

- clear top for lighting from outside while keeping in the humidity
- 1+1/4" or maybe small mason jar lid-sized holes for ventilation?
  (small mason jar lid would be big enough for a 40mm fan, whereas 1+1/4" is only 31.75).
  - Note that 1+1/4" would be enough for a standard E-26 light bulb's threads (~1.03"),
    and possibly a thin-walled socket, to fit through.
  - For threads, I could try [[../OneAndAQuarterSevenUNC.org][1 1/4" - 7 UNC]]

p1638 almost works, but:
- it's a little tight
- it's not very transparent
  (I think it blocks a significant fraction of the light that hits it)

[2025-03-13]: WSITEM-201430 / p1797 are working great!

*** Could I design the segments to stack?

Similar to how TOGridPile does?
- Maybe actually using TOGridPile shape
- Or maybe a scaled-up one

*** Blah blah

I forgot about this file.
More notes are on [[../../2022/Projects.tef::WSPROJECT-201255][WSPROJECT-201255]].
