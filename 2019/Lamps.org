Find out what the threads on the bottom of a 'regular' light socket are
- e.g. https://www.amazon.com/GE-3-Way-Lamp-Socket-54372/dp/B002DN4LX4


This seems a good page: https://grandbrass.zendesk.com/hc/en-us/articles/203607534-Thread-Size-Chart
- Sizes are named based on internal diameter, but are weird for historical reasons
  (actual interior diameter is wider, because when they converted from gas
  to electric they first used gas pipes, but then widened the inside
  to fit wires, but keeping the same size designation.
  So '1/8' actually means 'size corresponding to a gas pipe with 1/8" ID')
- Apparently 'Vase Caps' are a pretty standard part, too.

I guess the main rod thing is just called "lamp rod"

#+BEGIN_QUOTE
We generally leave off the pitch on 1/8 thread, it is 1/8-27.

The (finial) threads on the top of a harp are 1/4-27.

1/4-20 is the most popular furniture thread, it is more course.

Unlike plumbing, lamp parts are not tapered (not NPT). Lamp thread IPS is straight or parallel threads, equals the same as NPS National Pipe Straight threads.

Slip is with no threads at all.

Taps make female threads, dies make male threads.

A reducer allows you to reduce the size of a female thread. A nozzle allows you to reduce the size of a male thread.

Females are innies (the threads are on the inside), Males are outies (the threads are on the outside).

Righty tighty, lefty loosey.

"" = inch, ' = foot.

ID = internal diameter , OD = outside diameter
#+END_QUOTE

** Threaded lamp rod

- https://smile.amazon.com/Thread-Assortment-Hardware-Nipples-Washers/dp/B07WCS6RD3
  #+BEGIN_QUOTE
  The most standard threaded rod - 1/8-27 threaded nipples kit
  
  Outside Diameter: 3/8 inch
  
  Inside Diameter: 1/4inch
  #+END_QUOTE
- https://www.homedepot.com/p/Commercial-Electric-12-in-All-Thread-Lamp-Pipe-Kit-81815/306197039

*** Removing wires from socket

I had trouble getting some of the wires out from a plastic socket
where they were not screwed in, but pushed in and held by some internal mechanism.

I was able to get them out by pushing a paperclip into the hole next to the wire.
This released it.

https://diy.stackexchange.com/questions/63063/rewiring-screw-less-lamp-socket-how-do-i-remove-wires

** TODO

- [ ] Find out what the threads are on the finials
- [ ] Find out what threads are on the 'lamp rod' of the lamp I disassembled
  - I suspect it's "1/8 I.P.", with 27 threads per inch
    - "The most common thread in the Lamp & Lighting Industry"
- [ ] Find out if I have a tap for the finial thread
- [ ] Find out if I have a tap for the lamp rod thread
- [ ] Can I buy fully threaded lamp rod from somewhere?
