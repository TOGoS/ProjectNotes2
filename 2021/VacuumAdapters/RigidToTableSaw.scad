// Adapter to hopefully attach Rigid shop vac to my Harbor Freight table saw dust port, v1.
// Measurements:
// - Port tapers from 57.8mm to 56.8mm along 9mm (then there's obstacles farther in)
// - Rigid vacuumm hose end tapers from 44.1mm to 45.4mm along 60mm length.

$fn = 200;

mm = 1;
inch = 25.4;

length = 18 * mm;
epsilon = 0.5;
inner_length = length + 2 * epsilon; // For subtraction purposes, subtracted volume is a wee bit longer

outer_small_diameter = 57;
outer_taper = (57.8 - 56.8) / 9;

inner_small_diameter = 44;
inner_taper = (45.4 - 44.1) / 60;
inner_large_diameter = inner_small_diameter + inner_taper * length;

outer_large_diameter = outer_small_diameter + outer_taper * inner_length;

echo(str(
	"outer small: ", outer_small_diameter, "mm, ",
	"outer large: ", outer_large_diameter, "mm, ",
	"inner small: ", inner_small_diameter, "mm, ",
	"inner large: ", inner_large_diameter, "mm, "));

difference() {
	cylinder(h = length, d1 = outer_large_diameter, d2 = outer_small_diameter);
	translate([0,0,-epsilon]) cylinder(h = inner_length, d1 = inner_large_diameter, d2 = inner_small_diameter);
}