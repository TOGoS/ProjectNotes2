$fn = 30; // Make curves look better and stuff

panel_xpitch = 3.5;
panel_ypitch = 1;
panel_margin = 1/16;

panel_thickness = 3/16;
panel_width  = panel_xpitch - panel_margin*2;
panel_height = panel_ypitch - panel_margin*2;

meter_x = -7/16;
meter_width = 0.9;
meter_height = 0.55 + 1/32; // 0.55 was a little tight, +1/16 was a bit loose.
// meter_depth is 
// 7.5mm, slightly over 1/4"
// Documentation says 10mm, but that must include the board.
meter_depth = 0.295;
meter_mounting_hole_distance = 1.1; // 28mm
meter_mounting_hole_diameter = 0.06; // For 1/8 on the circuit board
meter_mounting_hole_undepth = 1/16; // Distance from surface at which it stops
meter_extra_width = meter_mounting_hole_distance + (meter_mounting_hole_distance - meter_width);
meter_wire_diameter = 1/16;

/* thickess of narrow part of panel between meter hole and outer edge */
module_frame_width = (panel_height-meter_height)/2;
meter_wire_trough_radius = (panel_ypitch - meter_height)/2;

power_button_x = 5/8;
power_button_diameter = 0.51; // From the product page; I measured 1/2"

big = 100;

use_screw_mounts = true;

module chamfered_rect(width, height, chamfer) {
	xn = width/2 - chamfer;
	xf = width/2;
	yn = height/2 - chamfer;
	yf = height/2;
	polygon([
		[+xf,+yn],[+xn,+yf],
		[-xn,+yf],[-xf,+yn],
		[-xf,-yn],[-xn,-yf],
		[+xn,-yf],[+xf,-yn]
	]);
}

module washer (innerdiam,thickness,outerdiam) {
	difference () {
		cylinder(h=thickness,d=outerdiam,center=true);
		cylinder(h=big,d=innerdiam,center=true);
	}
}

module screwrot() {
	rotate([0,0,rands(0,360,1)[0]]) children();
}

module screw (diam,height,headdiam) {
	headheight=headdiam/3;
	/*intersection() {
		translate([-big/2,-big/2,0]) cube([big,big,big]);
		sphere(headdiam);
	}*/
	difference() {
		translate([0,0,headheight/2])
			scale([1,1,headheight/headdiam])
			sphere(headdiam/2);
		translate([0,0,headheight]) {
			cube([headdiam*1/8, headdiam*2/3, headheight], true);
			cube([headdiam*2/3, headdiam*1/8, headheight], true);
		}
	}
	translate([0,0,-height]) cylinder(h=height+headheight/2,d=diam);
}

module meter_wire_trough() {
	translate([0,0,meter_wire_diameter - meter_wire_trough_radius])
		rotate([0,90,0])
			cylinder(h=1/4,r=meter_wire_trough_radius,center=true);
}

module meter_holes () {
	cube([meter_width,meter_height,big], true);
	screw_hole_bottom_z = panel_thickness/2 - meter_depth - 1;
	screw_hole_depth = meter_depth - meter_mounting_hole_undepth + 1;
	
	translate([-meter_mounting_hole_distance/2,0,screw_hole_bottom_z])
		cylinder(h=screw_hole_depth, d=meter_mounting_hole_diameter, center=false);
	translate([+meter_mounting_hole_distance/2,0,screw_hole_bottom_z])
		cylinder(h=screw_hole_depth, d=meter_mounting_hole_diameter, center=false);
	if(!use_screw_mounts) {
		translate([0,-meter_height/2,-panel_thickness/2]) meter_wire_trough();
		translate([0,+meter_height/2,-panel_thickness/2]) meter_wire_trough();
	}
}

module screw_mount_hull() {
	holder_width = meter_mounting_hole_distance - meter_width;
	translate([0, 0, panel_thickness - meter_depth]) {
		linear_extrude(height=meter_depth, center=true) {
			chamfered_rect(holder_width, panel_height, 1/16);
		}
	}
}

module meter_mount_hull() {
	holder_width = meter_width + (meter_mounting_hole_distance - meter_width)*2;
	translate([0, 0, panel_thickness/2 - meter_depth/2]) {
		difference() {
			linear_extrude(height=meter_depth, center=true) {
				chamfered_rect(holder_width, panel_height, 1/8);
			}
			translate([0, 0, -meter_depth/2])
				cube([1/4, big, meter_wire_diameter*2], true);
		}		
	}
}

/**
 * Basic shape of panel without any holes
 */
module panel_hull() {
	// cube([width-margin*2,height-margin*2,thickness], true);
	linear_extrude(height=panel_thickness,center=true) {
		chamfered_rect(panel_width, panel_height, 1/8);
	}
	if(use_screw_mounts) {
		//translate([meter_x-meter_mounting_hole_distance/2, 0, 0]) screw_mount_hull();
		//translate([meter_x+meter_mounting_hole_distance/2, 0, 0]) screw_mount_hull();
		translate([meter_x, 0, 0]) meter_mount_hull();
	}
}

module wire_hole() {
	cube([1/8,1/8,big],true);
}

module fancy_wire_notch() {
	linear_extrude(height=big,center=true) {
		//polygon([[-1/16,0],[0,1/16],[1/16,0],[0,-1/16]]);
				polygon([
			[-1/32,	0],
			[-1/32,-2/32],
			[-2/32,-2/32],
			[-2/32,-4/32],
			
			[+2/32,-4/32],
			[+2/32,-2/32],
			[+1/32,-2/32],
			[+1/32,	0],
			
			[+1/32,	0],
			[+1/32,+2/32],
			[+2/32,+2/32],
			[+2/32,+4/32],
			
			[-2/32,+4/32],
			[-2/32,+2/32],
			[-1/32,+2/32],
			[-1/32,	0],
		]);
	}
}

module wire_notch() {
	wire_hole();
}

module panel() {
	difference() {
		panel_hull();
		
		// Mounting holes
		translate([-3/2,0,0]) cylinder(h=big, d=6/32, center=true);
		translate([+3/2,0,0]) cylinder(h=big, d=6/32, center=true);
		
		// Hole for power button
		translate([power_button_x,0,0])
		cylinder(h=panel_thickness*2, d=power_button_diameter, center=true);
		
		// Hole for meter itself
		translate([meter_x,0,0]) meter_holes();
		
		notch_y = panel_height/2;
		hole_y = panel_height/2 - 1/8;
		translate([+18.75/16,+hole_y,0]) wire_notch();
		translate([+18.75/16,-hole_y,0]) wire_hole();
		translate([ +4.75/16,-hole_y,0]) wire_notch();
		translate([ +4.75/16,+hole_y,0]) wire_hole();
		translate([-18.75/16,+hole_y,0]) wire_notch();
		translate([-18.75/16,-hole_y,0]) wire_hole();
	}
}


module rail (length,thickness) {
	difference() {
		cube([1/2,length,thickness], true);
		
		for(i=[0:length-1]) {
			translate([0,i+0.5-length/2,0]) {
				cylinder(h=thickness*2, d=5/32, center=true);
			}
		}
	}
}

washer_thickness = 1/32;

module normal_washer() {
	washer(10/64, washer_thickness, 24/64);
}

module washerandscrew() {
	screw(9/64, 1, 9/32);
	translate([0,0,1/64]) normal_washer();
}

module panel_on_rails() {
	color([0.1,0.4,0.2]) {
		translate([-3/2,-1/2,-3/8]) rail(6, 3/4);
		translate([+3/2,-1/2,-3/8]) rail(6, 3/4);
	}
	color([1/2,1/2,1/2]) {
		translate([-3/2,0,panel_thickness+washer_thickness]) screwrot() washerandscrew();
		translate([+3/2,0,panel_thickness+washer_thickness]) screwrot() washerandscrew();
		translate([-3/2,0,washer_thickness/2]) normal_washer();
		translate([+3/2,0,washer_thickness/2]) normal_washer();
	}
	translate([0,0,washer_thickness+panel_thickness/2]) color([0.3,0.4,0.8]) panel();
}

scale(25.4) { // All my units are inches
	//panel_on_rails();
	rotate([180,0,0]) panel(); // Print upside-down so flat side is flat
}
