// Phone holder
// Or for USB power bricks, or whatever
// In progress.  Cool rounded cube module.

inch = 25.4;
mm = 1;

back_thickness   =  6;
front_thickness  =  6;
side_thickness   = 10;
bottom_thickness =  8; // Thicker and it interferes with counterbores
side_height = 76.2;

gridbeam_hole_diameter = 7.95;
gridbeam_counterbore_diameter = 21;
gridbeam_counterbore_depth = 2;

back_gridbeam_hole_positions = [
	[   0*inch, 0, 2.25*inch],
	[   0*inch, 0, 0.75*inch],
	[-1.5*inch, 0, 3.75*inch],
	[   0*inch, 0, 3.75*inch],
	[+1.5*inch, 0, 3.75*inch],
	[-1.5*inch, 0, 5.25*inch],
	[   0*inch, 0, 5.25*inch],
	[+1.5*inch, 0, 5.25*inch],
];

// Origin is the back, center, bottom;
// panel extends +y and +z
module back_panel(size) {
	difference() {
		translate([0,size[1]/2, size[2]/2]) {
			cube(size, center=true);
		}
		translate([0,size[1],0]) {
			// Start at 'front' to make counterbores easy
			for( pos=back_gridbeam_hole_positions ) {
				translate(pos) rotate([90,0,0]) {
					cylinder(h=back_thickness*3, d=gridbeam_hole_diameter, center=true);
					cylinder(h=gridbeam_counterbore_depth*2, d=gridbeam_counterbore_diameter, center=true); 
				}
			}
		}
	}
}

// Always centered!
module rounded_xy_cube(size, xy_rad, z_rad) {
	xy_rad_adjusted = min(min(size[0],size[1])/2-0.1, xy_rad);
	z_rad_adjusted = min(size[2]/2-0.1, z_rad);
	minkowski() {
		sphere(r=z_rad_adjusted);
		linear_extrude(size[2]-z_rad_adjusted*2, center=true) {
			minkowski() {
				circle(r=xy_rad_adjusted-z_rad_adjusted);
				square_margin = xy_rad_adjusted;
				square([size[0]-square_margin*2,size[1]-square_margin*2], center=true);
			}
		}
	}
}


module power_brick_holder(size) {
	back_panel([size[0], back_thickness, size[2]]);
	
	for( side=[-1,1] ) {
		translate([
			side * (size[0] - side_thickness)/2,
			size[1]/2, side_height/2
		]) cube([side_thickness, size[1], side_height], center=true);
	}
	
	translate([0, size[1]/2, bottom_thickness/2]) {
		cube([size[0], size[1], bottom_thickness], center=true);
	}
	
	translate([0, size[1]-front_thickness/2, side_height/2]) {
		cube([size[0], front_thickness, side_height], center=true);
	}
}

power_brick_holder([4.5*inch, 2.25*inch, 6*inch]);

//srounded_xy_cube([40,40,6], 10, 3, $fn=30);