margin = 0.6;
thickness = 3;

inch = 25.4;

width = (3+1/2)*inch - margin*2;
depth = (1+1/4)*inch - margin*2;
bumper_height = 1/2*inch - thickness;
left_bumper_width = 0.3*inch;
right_bumper_width = 1/2*inch;

bevel_r = 1.5;

module bevel_diamond(r) {
	polygon([
		[-r,  0],
		[ 0, +r],
		[+r,  0],
		[ 0, -r]
	]);
}

module beveled_square(size, r) {
	x0 = -size[0]/2;
	x1 = +size[0]/2;
	y0 = -size[1]/2;
	y1 = +size[1]/2;
	polygon([
		[x0+r, y0  ],
		[x0  , y0+r],
		[x0  , y1-r],
		[x0+r, y1  ],
		[x1-r, y1  ],
		[x1  , y1-r],
		[x1  , y0+r],
		[x1-r, y0  ]
	]);
}
module xy_beveled_cube(size, r) {
	linear_extrude(size[2], center=true) {
		beveled_square(size, r);
	}
}

difference() {
	union() {
		translate([0,0,thickness/2]) xy_beveled_cube([width, depth, thickness], bevel_r);
		translate([-width/2 +  left_bumper_width/2, 0, thickness + bumper_height/2]) xy_beveled_cube([ left_bumper_width, depth, bumper_height], bevel_r);
		translate([ width/2 - right_bumper_width/2, 0, thickness + bumper_height/2]) xy_beveled_cube([right_bumper_width, depth, bumper_height], bevel_r);
	}
	translate([0, -depth/2, thickness/2]) {
		xy_beveled_cube([1.5*inch, depth*2/3*2, thickness*2], bevel_r);
	}
	linear_extrude(thickness*2, center=true) {
		translate([-0.75*inch, -depth/2, thickness/2]) bevel_diamond(bevel_r);
		translate([ 0.75*inch, -depth/2, thickness/2]) bevel_diamond(bevel_r);
	}
}
