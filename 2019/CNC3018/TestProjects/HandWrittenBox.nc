(Using https://www.cnccookbook.com/g-code-cheat-sheet-mdi/ as sort of a tutorial)
(https://en.wikipedia.org/wiki/G-code is an okay reference)
(Basic online simulator: https://nraynaud.github.io/webgcode/)
(Fancier one that doesn't always work: http://www.cncwebtools.com/)

G20 (We'll be using inches)
F3 (Set up feedrate to 2 in/min, which is about 1 mm/s)
G00 Z0.25 (up!)
S1000 (set the spindle speed)
M03 (start it spinning; M05 to stop)

(Outline the panel:)
G00 Z0.25
G00 X0.25 Y0

G01 Z-0.01 (we'll just carve an outline)
G01 X3.75
G03 I0 J0.25 K0 X4 Y0.25 (counterclockwise arc centered at y=-0.25 from the start point)
G01 Y3.25
G03 I-0.25 J0 K0 X3.75 Y3.5
G01 X0.25
G03 I0 J-0.25 K0 X0 Y3.25
G01 Y0.25
G03 I0.25 J0 K0 X0.25 Y0

(another pass)
G01 Z-0.02
G01 X0.25 Y0
G01 Z-0.01 (we'll just carve an outline)
G01 X3.75
G03 I0 J0.25 K0 X4 Y0.25 (counterclockwise arc centered at y=-0.25 from the start point)
G01 Y3.25
G03 I-0.25 J0 K0 X3.75 Y3.5
G01 X0.25
G03 I0 J-0.25 K0 X0 Y3.25
G01 Y0.25
G03 I0.25 J0 K0 X0.25 Y0

G00 Z0.25 (up!)

(time to drill holes!)
G00 Y0.25 (bottom row)

G00 X0.25
G01 Z-0.125
G00 Z0.25

G00 X0.75
G01 Z-0.125
G00 Z0.25 (up!)

G00 X1.25
G01 Z-0.125
G00 Z0.25 (up!)

G00 X1.75
G01 Z-0.125
G00 Z0.25 (up!)

G00 X2.25
G01 Z-0.125
G00 Z0.25 (up!)

G00 X2.75
G01 Z-0.125
G00 Z0.25 (up!)

G00 X3.25
G01 Z-0.125
G00 Z0.25 (up!)

G00 X3.75
G01 Z-0.125
G00 Z0.25 (up!)

G00 Y3.25 X0.25 (top row)

(GRBL doesn't seem to know about G83 for peck drilling, so I'll just repeat)

G00 X0.25
G01 Z-0.125
G00 Z0.25

G00 X0.75
G01 Z-0.125
G00 Z0.25 (up!)

G00 X1.25
G01 Z-0.125
G00 Z0.25 (up!)

G00 X1.75
G01 Z-0.125
G00 Z0.25 (up!)

G00 X2.25
G01 Z-0.125
G00 Z0.25 (up!)

G00 X2.75
G01 Z-0.125
G00 Z0.25 (up!)

G00 X3.25
G01 Z-0.125
G00 Z0.25 (up!)

G00 X3.75
G01 Z-0.125
G00 Z0.25 (up!)

G01
M05
