Originally based on https://www.meebey.net/posts/ethereum_gpu_mining_on_linux_howto/

### Install Nvidia Drivers

Required to do mining using Claymore, and probably by other miners, too.

Annoying way:

```sh
sudo apt-get install linux-headers-amd64 build-essential unzip gcc make
mkdir -p ~/downloads && cd $_ &&
    wget https://us.download.nvidia.com/XFree86/Linux-x86_64/384.90/NVIDIA-Linux-x86_64-384.90.run &&
    chmod +x NVIDIA-Linux-x86_64-384.90.run &&
    sudo ./NVIDIA-Linux-x86_64-384.90.run
```

Different, easier way:

```sh
sudo apt purge -y nvidia*
sudo add-apt-repository ppa:graphics-drivers
sudo apt update -y
sudo apt-get install -y nvidia-384
sudo reboot # Reboot!
```

### Install Etheredum Wallet and Ethminer

If you need to set up a new Ethereum wallet:

```sh
sudo apt-get install -y software-properties-common
sudo add-apt-repository ppa:ethereum/ethereum
sudo sed 's/jessie/vivid/' -i /etc/apt/sources.list.d/ethereum-ethereum-*.list
sudo apt-get update
sudo apt-get install -y ethereum
geth account new
# copy long character sequence within {}, that is your <YOUR_WALLET_ADDRESS>
# if you lose the passphrase, you lose your coins!
```

To run ethminer (but don't, we'll use Claymore, instead):
```
sudo apt install -y ethminer
ethminer -G -F http://yolo.ethclassic.faith:9999/0x<YOUR_WALLET_ADDRESS> --farm-recheck 200
```

### Install Sia-UI

If you need to set up a new Sia wallet:

```sh
mkdir -p ~/downloads && cd $_ &&
      wget https://github.com/NebulousLabs/Sia-UI/releases/download/v1.3.1/Sia-UI-v1.3.1-linux-x64.zip
mkdir -p ~/ext-apps/sia-ui-1.3.1 && cd $_ &&
      unzip ~/downloads/Sia-UI-v1.3.1-linux-x64.zip
```

Sia-UI is a GUI app so nothing to show for running it.
I created a wallet and left the password the same as the seed,
which is the default.

### Install Claymore Miner

```sh
sudo apt-get install libcurl4-openssl-dev # Claymore uses it, apparently
mkdir -p ~/downloads && cd $_ &&
      wget https://github.com/nanopool/Claymore-Dual-Miner/releases/download/v10.0/Claymore.s.Dual.Ethereum.Decred_Siacoin_Lbry_Pascal.AMD.NVIDIA.GPU.Miner.v10.0.-.LINUX.tar.gz
mkdir -p ~/ext-apps/Claymore-10.0 && cd $_ &&
      tar -xvf ~/downloads/Claymore.s.Dual.Ethereum.Decred_Siacoin_Lbry_Pascal.AMD.NVIDIA.GPU.Miner.v10.0.-.LINUX.tar.gz
```

Copy to a new directory (just because I like to keep the original untouched) and overwrite some config files with our own:

```sh
rm -rf ~/ext-apps/Claymore-runner && mkdir -p $_ && cd $_ &&
   cp --reflink=auto ~/ext-apps/Claymore-10.0/* ./ &&
   wget http://wherever-files.nuke24.net/uri-res/raw/urn:bitprint:K3ZVSDYCMGPPB35FJFMNTZBDWFPC5AR4.AGVYUYW6PIECWWBWQIB3L3UR3UHZFPJHE5GESOQ/claymore_config_nanopool.zip &&
   unzip claymore_config_nanopool.zip &&
   chmod +x ./start.bash
```

Change the name of the node in ```start.bash```.  Then you can run it.

```sh
./start.bash
```

### Get stats on cards

```
nvidia-smi -q -a
```

will dump out a crapload of information about your cards.
