#+TITLE: WSTYPE-4110: A method for making French cleats.

I put two of these in on the west northwest wall of the office on 2020-03-29,
27" and 51" from the ceiling.


3/4" by 3+1/2" common board, with a corner cut off.

# Original convention:
# Recommended screw placement is 1" from top and 1" from bottom of the 3.5"-wide board.

Recommended screw placement is 1" and 2" from the top of the cleat
(the cleats in the kitchen as of 2020-06-30 are this way).

Measure section of wall, mark where screws should go, drill small holes into wall.
- This is a good opportunity to make sure there are studs there!
- If insufficient studs, put in drywall anchors.

Note positions of studs on wall.

Mark where the screws will go on back of board corresponding to wall holes that you intend to use.

Cut corner off of the board.
Try to keep the cut as close as possible to going exactly through the other top corner,
since your measurements are based on it!

Drill holes in board large enough for screws to pass through.

Countersink if needed (not needed if wood is soft).

Attach to wall.

[[http://picture-files.nuke24.net/uri-res/raw/urn:bitprint:L6F2R4CJ7VGPQHYJRNMHGPXNL3JDJU7N.3AISTSR2XDATP4WBWSIJWGL24ZCW3XHZF2SM5CY/cleats.jpg][Here they are on the wall]]


#+CAPTION: French cleat shape including recommended mounting holes and countersinks.  1 char = 1/4".  Wall's on the left.
#+BEGIN_SRC ascii-art
  ╱│
 ╱ │
╱  │
│_┌┤
│¯└┤
│  │
│  │
│_┌┤
│¯└┤
│  ╱
│ ╱
│╱
#+END_SRC
