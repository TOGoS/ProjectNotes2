// Size of one block
block_size_bsunit = 6;
// Unit used for block_size_xunit
bsunit = "1/8*inch"; // ["mm", "inch", "1/8*inch", "1/16*inch", "1/32*inch", "1/64*inch", "inval"]
// Length of the beam, in blocks
length_blocks = 8;

// Hole diameter, in mm; 3.97mm = 5/32in, reasonable for #6 screws
hole_diam = 3.97;
// Corner radius, in mm; 3.175 = 1/8in
corner_radius = 3.175;

// Diameter of lengthwise hole, if desired (0 if not)
tunnel_diam = 3.97;

// Width of slot cut every few units to prevent peeling during printing
slot_width = 1;
// Thickness of top and bottom layers outside of the slot
slot_wall_thickness = 0.6;
// number of blocks between slots
slot_interval = 2;

// For end hole diameter,
// 0.185" to 0.205" is recommended for certain threaded inserts
// (4.7mm to 5.2mm) over 1/4"
// by some Amazon commenter: https://www.amazon.com/gp/customer-reviews/RZQ2DG0MHYO3E/ref=cm_cr_dp_d_rvw_ttl?ie=UTF8&ASIN=B076R7F3DJ

// End hole diameter; for threaded inserts, if you want them.
end_hole_outer_diam = 5.2;
end_hole_inner_diam = 4.7;
end_hole_depth = 6.35;

end_brim_diameter = 40;
end_brim_thickness = 0.3;

corner_fn = 20; // [5:1:40]
hole_fn = 20; // [5:1:40]

module _() { }

inch = 25.4;

function unit_value(unit_name) =
	unit_name == "mm"        ? 1 :
	unit_name == "inch"      ? inch :
	unit_name == "1/8*inch"  ? inch/8 :
	unit_name == "1/16*inch" ? inch/16 :
	unit_name == "1/32*inch" ? inch/32 :
	unit_name == "1/64*inch" ? inch/64 :
	("invalid unit size"+unit_name); // Invalid!

block_size = block_size_bsunit * unit_value(bsunit);

// TODO: Customizable girth, length
// TODO: Beveled or rounded edges

length = length_blocks*block_size;


$fs = 1;

module rounded_cube(size, r, $fn=corner_fn) {
	if( r <= 0 ) {
		cube(size, center=true);
	} else {
		hull() {
			for(xf=[-1:2:1]) {
				for(yf=[-1:2:1]) {
					for(zf=[-1:2:1]) {
						translate([xf*(size[0]/2-r), yf*(size[1]/2-r), zf*(size[2]/2-r)]) sphere(r=r);
					}
				}
			}
		}
	}
}

difference() {
	union() {
		rounded_cube([block_size, length, block_size], r=corner_radius);
		if( end_brim_thickness > 0 && end_brim_diameter > 0 ) {
			for( y=[-length/2, +length/2] ) {
				translate([0, y, -block_size/2]) cylinder(h=end_brim_thickness, d=end_brim_diameter, center=false);
			}
		}
	}
	union() {
		$fn = hole_fn;
		for( i=[-length_blocks/2+0.5:1:+length_blocks/2-0.5] ) {
			translate([0, i*block_size, 0]) {
				cylinder(d=hole_diam, h=block_size*2, center=true);
				rotate([0,90,0]) cylinder(d=hole_diam, h=block_size*2, center=true);
			}
		}
		if( tunnel_diam >= 0 ) {
			rotate([90,0,0]) {
				cylinder(h=length+2, d=tunnel_diam, center=true);
			}
		}
		if( (end_hole_outer_diam >= 0 || end_hole_inner_diam >= 0) && end_hole_depth >= 0 ) {
			for( ff=[-1,1] ) scale([1, ff, 1]) translate([0,length/2,0]) {
				rotate([-90,0,0]) {
					cylinder(h=end_hole_depth*2,
						d1=end_hole_inner_diam,
						d2=end_hole_outer_diam+(end_hole_outer_diam-end_hole_inner_diam),
						center=true);
				}
			}
		}
		if( slot_width > 0 ) {
			for( p=[-length_blocks/2+slot_interval:slot_interval:length_blocks/2-slot_interval] ) {
				translate([0,p*block_size,0]) {
					hull() {
						translate([0,0,-block_size/2+slot_width/2+slot_wall_thickness]) {
							rotate([0,90,0]) cylinder(h=block_size*2, d=slot_width, center=true);
						}
						translate([0,0,+block_size/2-slot_width/2-slot_wall_thickness]) {
							rotate([0,90,0]) cylinder(h=block_size*2, d=slot_width, center=true);
						}
					}
				}
			}
		}
	}
}
