Explanation:

Last night I decided to buy the parts for a very basic mining rig. It was about
$1100 altogether.  I don't care if it's even cost effective or if it ever pays 
me back, I just want to build it to build it. Maybe the experience will help me 
do something similar later. It could also make me more helpful in 
this particular project.  I think you guys should also build one, a better one.  
Some of the parts I chose(like the motherboard) were simply chosen because 
they were going to ship more quickly.  A lot of parts I saw were not going to 
arrive for over a month, so i ordered others that will be coming 
in the next 5-10 days. I'm building this with little regard for anything other 
than getting it done as soon as possible and doing it on my own.  It might not 
turn out well, but it will be better than nothing.
There are a few parts i didn't order, but I have what i need to get it running.
There's some parts like the case, that might be a complete waste of money
but that will save time and I think help with organization and appearance.
The graphics card is a very questionable choice too.  i don't think I should
buy mining only cards, but this is going to be a single card rig to help me
learn before the new GTX 1100's etc. come out in a few weeks or months.  And I'm 
sure no one will like my choice of windows 10. But the point is that if I 
am actively mining, even insignificant amounts, it will also force me to learn 
about more exchanges, have a wallet, trade currencies, etc.  Once I get it up 
and running I will worry about adding the other 5 graphics cards and i hope they
will be from the next generation of graphics cards that i think is going to come
out soon.  I know I am so far behind on mining.  It might be too late to start 
mining at all, but I'm doing it anyway. Maybe just me doing this will make all 
cryptocurrencies crash.

Part list:

Motherboard = ASRock H81 PRO BTC R2.0
$193.95
https://www.amazon.com/gp/product/B01M5FQZYE/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1
 
Power Supply = 600 watt Rosewill Glacier Series
$54.99 
https://www.amazon.com/gp/product/B00S8NOAJ8/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1

Graphics Card = Sapphire Radeon RX 470 8Gb GDDR5 MINING QUAD UEFI 
$419.95 
https://www.amazon.com/gp/product/B073TBZS5F/ref=od_aui_detailpages01?ie=UTF8&psc=1

risers = SHINESTAR PCIe Riser Card ( 8 Pack )
$50.99
https://www.amazon.com/gp/product/B077P6GSGT/ref=od_aui_detailpages01?ie=UTF8&psc=1

ram = Kingston HyperX Fury 4GB
$34.99
https://www.amazon.com/gp/product/B00J8E8ZK6/ref=od_aui_detailpages00?ie=UTF8&psc=1

processor = Intel G3260 3MB Haswell Dual-Core 3.3 GHz LGA 1150
$80.00 
https://www.amazon.com/gp/product/B00W4TREZO/ref=oh_aui_detailpage_o02_s00?ie=UTF8&psc=1

Windows 10 Pro
$156.99
https://www.amazon.com/gp/product/B00ZSHDJ4O/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1

Veddha Professional 8 GPU Miner Case
$189.66
https://www.amazon.com/gp/product/B073Q2CN14/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1


