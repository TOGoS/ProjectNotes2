/* [Size adjustments (all in mm)] */

// Radius of the outer corners at the base
footprint_curve_rad   = 8  ; // [0  :1  :25]
// Radius of the curve around the gridbeam socket
beam_cavity_curve_rad = 2.0; // [0  :0.2: 3]
// Extra space around the gridbeam socket
beam_cavity_margin    = 0.5; // [0  :0.1: 2]
// Radius of rounding on most convex corners; nonzero value will result in very slow rendering esp. with high $fn values
3d_curve_rad          = 0.0; // [0.5:0.5: 3]

/* [Curve precision] */

footprint_curve_fn   = 32;
slot_curve_fn        = 16;
beam_cavity_curve_fn = 16;
// Set this to a high value and 3d_curve_rad to a nonzero one if you want to use your computer as a space heater for the next month
3d_curve_fn          =  8;

/* [Preview] */
// Do you want to see a piece of gridbeam?
include_gridbeam_in_preview = true;

module rounded_rectangle(size, r, center=true) {
	if( r == 0 ) {
		square(size, center=center);
	} else {
		d = r*2;
		minkowski() {
			square([size[0]-d, size[1]-d], center=center);
			circle(r=r);
		}
	}
}
module rounded_cube(size, r, center=true, fn=$fn) {
	if( r == 0 ) {
		cube(size, center=center);
	} else {
		d = r*2;
		minkowski() {
			cube([size[0]-d, size[1]-d, size[2]-d], center=center);
			sphere(r, $fn=fn);
		}
	}
}


mm   = 1;
inch = 25.4;

beam_width = 1.5*inch;

// Derived constants
3d_curve_diam = 3d_curve_rad*2;

module padded(r, sphere_fn=$fn) {
	if( r <= 0 ) {
		children();
	} else {
		minkowski() {
			children();
			sphere(r=r, $fn=sphere_fn);
		}
	}
}

module gridbeam_socket_subtraction(extra_margin) {
	translate([0, 0, 3*inch]) {
		rounded_cube([
			beam_width  +beam_cavity_margin*2+extra_margin,
			beam_width  +beam_cavity_margin*2+extra_margin,
			beam_width*2                     +extra_margin
		], beam_cavity_curve_rad+extra_margin, fn=beam_cavity_curve_fn);
	}
}

module bolt_slot_subtraction() {
	for( zrot=[0,90,180,270] ) {
		rotate([0,0,zrot]) {
			translate([1.5*inch, 0, 3*inch]) {
				rounded_cube([1.25*2*inch, 5/16*inch, (1.5*inch+5/32*inch)*2], r=5/32*inch-0.1, fn=slot_curve_fn);
			}
		}
	}
}

module bolt_counterbore_subtraction(extra_margin) {
	for( zrot=[0,90,180,270] ) {
		rotate([0,0,zrot]) {
			translate([1.5*inch, 0, 3*inch]) {
				translate([0,0,-1.5*inch]) rotate([0,-90,0]) {
					linear_extrude(3/4*inch+extra_margin, center=true) {
						hull() {
							circle(d=7/8*inch+extra_margin);
							translate([4*inch,0,0]) circle(d=7/8*inch+extra_margin);
						}
					}
				}
			}
		}
	}
}

difference() {
	padded(r=3d_curve_rad, sphere_fn=3d_curve_fn) {
		difference() {
			translate([0,0,3d_curve_rad]) {
				linear_extrude(3*inch-3d_curve_diam, scale=3/4) {
					rounded_rectangle([3*inch-3d_curve_diam, 3*inch-3d_curve_diam], r=footprint_curve_rad, $fn=footprint_curve_fn);
				}
			}
			// Cut space for gridbeam itself
			gridbeam_socket_subtraction(3d_curve_diam);
			// Drainage and bolt holes
			bolt_counterbore_subtraction(3d_curve_diam);
		}
	}
	bolt_slot_subtraction();
}

module gridbeam(block_size, hole_diameter, length_blocks) {
	difference() {
		rounded_cube([block_size, block_size, block_size * length_blocks], r=block_size/16, center=true);
		for( i=[-(length_blocks-1)/2 : 1 : +(length_blocks-1)/2] ) {
			translate([0,0, block_size*i]) {
				rotate([90,0,0]) cylinder(h=block_size+1, d=hole_diameter, center=true, $fn=20);
			}
		}
	}	
}

if( $preview && include_gridbeam_in_preview ) {
	translate([0,0,6*inch]) color("brown") gridbeam(1.5*inch, 5/16*inch, 6);
}
